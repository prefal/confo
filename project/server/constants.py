#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **A module for global constants and objects.**

This module has the purpose to define global constants such that every other
module can import and use them.
"""

DEFAULT_MAX_NUMBER_OF_WORKERS = 4
DEFAULT_ALGO_WINDOW_SIZE = 10
DEFAULT_NET_PATH = 'petri.pnml'
DEFAULT_HTTP_PORT = 5000
DEFAULT_LOG_FILE = 'server.ini'

MAX_NUMBER_OF_WORKERS = 0
ALGO_WINDOW_SIZE = 0
NET_PATH = ''
DEBUG_MODE = False
LOGFILE = ''

NET = None
INI = None
FIN = None
ALIGNMENT_STORAGE = None
STATISTIC_STORAGE = None


def default():
	""" **Resets the constant's config parameters to the default values** """

	global MAX_NUMBER_OF_WORKERS
	MAX_NUMBER_OF_WORKERS = DEFAULT_MAX_NUMBER_OF_WORKERS
	global ALGO_WINDOW_SIZE
	ALGO_WINDOW_SIZE = DEFAULT_ALGO_WINDOW_SIZE
	global NET_PATH
	NET_PATH = DEFAULT_NET_PATH
	global HTTP_PORT
	HTTP_PORT = DEFAULT_HTTP_PORT
	global LOGFILE
	LOGFILE = DEFAULT_LOG_FILE


def pretty_print():
	""" **Pretty prints the constant's config values** """
	import logging
	message = ' * {:30}: {:10}'.format('MAX_NUMBER_OF_WORKERS', str(MAX_NUMBER_OF_WORKERS))
	logging.info(message)
	print(message)

	message = ' * {:30}: {:10}'.format('ALGO_WINDOW_SIZE', str(ALGO_WINDOW_SIZE))
	logging.info(message)
	print(message)

	message = ' * {:30}: {:10}'.format('NET_PATH', NET_PATH)
	logging.info(message)
	print(message)

	message = ' * {:30}: {:10}'.format('HTTP_PORT', str(HTTP_PORT))
	logging.info(message)
	print(message)

	message = ' * {:30}: {:10}'.format('DEBUG_MODE', 'True' if DEBUG_MODE else 'False')
	logging.info(message)
	print(message)

	message = ' * {:30}: {:10}'.format('LOGFILE', str(LOGFILE))
	logging.info(message)
	print(message)
