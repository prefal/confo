#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

from server.view_statistics.versions.generate_statistic_png import generate_statistic_with_alignment
from server.view_statistics.versions.generate_statistic_png import generate_statistic_only
from server.view_statistics.versions import generate_statistic_png
from server.view_statistics.versions.generate_statistic_png import store_picture as save_pic

STATISTIC_WITH_ALIGNMENT = 'statistic_with_alignment'
STATISTIC_ONLY = 'statistic_only'

# Constants
N_M_MODEL = generate_statistic_png.N_M_MODEL
N_M_LOG = generate_statistic_png.N_M_LOG
N_M_SYNC = generate_statistic_png.N_M_SYNC
ACTIVATED = generate_statistic_png.ACTIVATED

VERSIONS = {
	STATISTIC_WITH_ALIGNMENT: generate_statistic_with_alignment,
	STATISTIC_ONLY: generate_statistic_only
}


def apply(net, image_format='png', curr_event_name=None, initial_marking=None,
			final_marking=None, is_show_deviation=True, dic_deviation=None,
			is_show_place=True, variant=STATISTIC_WITH_ALIGNMENT,
			debug=False):
	return VERSIONS[variant](net, image_format='png', curr_event_name=curr_event_name,
							initial_marking=initial_marking, final_marking=final_marking,
							is_show_deviation=is_show_deviation, dic_deviation=dic_deviation,
							is_show_place=is_show_place,
							debug=debug)


def store_picture(gviz, path):
	""" **Stores generate process model on hard drive** """
	save_pic(gviz, path)
