#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

from copy import deepcopy
import server.pm4pyref  # noqa: F401
from pm4py.visualization.petrinet import factory as vis_factory
from pm4py.objects import petri

from server.view_statistics.versions.visualize import graphviz_visualization
from server.view_statistics.versions import visualize

# Constant
SIZE = visualize.SIZE
LABEL = visualize.LABEL
LABELLOC = visualize.LABELLOC
FILLCOLOR = visualize.FILLCOLOR
STYLE = visualize.STYLE
FONTSIZE = visualize.FONTSIZE
FONTCOLOR = visualize.FONTCOLOR
PENWIDTH = visualize.PENWIDTH
BORDER = visualize.BORDER

N_M_MODEL = 'n_m_model'
N_M_LOG = 'n_m_log'
N_M_SYNC = 'n_m_sync'
ACTIVATED = 'activated'
SIZE_SILENT = ('0.6', '0.4')
COLOR_BREWER = "/ylorrd9/"
COLOR_CURR_EVENT = 'purple'
SIZE_DEFAULT_TRANSITION = ('0.8', '1.3')
SIZE_DEFAULT_PLACE = ('0.8', '0.8')


def generate_statistic_only(
		petrinet, image_format='png', curr_event_name=None,
		initial_marking=None, final_marking=None,
		is_show_deviation=True, dic_deviation=None,
		is_show_place=False,
		debug=False):
	"""
		Prepare the decoration dictionary for the visualizer to generate the image
		of statistics without alignment
	"""

	net = deepcopy(petrinet)
	net.transitions.add(petri.petrinet.PetriNet.Transition('unaffiliated', label='unaffiliated'))
	decorations = prepare_render_list(
			net=net,
			curr_event_name=None,
			initial_marking=None,
			final_marking=final_marking,
			is_show_deviation=is_show_deviation,
			dic_deviation=dic_deviation,
			is_show_place=is_show_place,
			debug=debug)
	viz = graphviz_visualization(
			net=net,
			image_format=image_format,
			decorations=decorations,
			is_show_place=is_show_place,
			debug=False)
	return viz


def generate_statistic_with_alignment(
		net, image_format='png', curr_event_name=None,
		initial_marking=None, final_marking=None,
		is_show_deviation=True, dic_deviation=None,
		is_show_place=True,
		debug=False):
	"""
	Prepare the decoration dictionary for the visualizer to generate the image
	of statistics with alignment

	Parameters
	----------
	net : :class:`pm4py.objects.petri.petrinet.PetriNet`
		The Petri net to decorate
	image_format : :class:`string`
		Format that should be associated to the image
	curr_event_name : :class:`string`
		current name of event to run the alignment
	initial_marking : :class:`pm4py.objects.petri.petrinet.Marking`
		Current marking of the Petri net to decorate the place
	final_marking : :class:`pm4py.objects.petri.petrinet.Marking`
		Final marking of the Petri net
	is_show_deviation : :class:`boolean`
		Decide which percentage value should be shown on node.
		True for deviation rate.
		False for corrrectness rate.
	dic_deviation : :class:`{Object : dictionary}`
		The dictionary of deviation statistics.
		The allowed keys include:
			{string : dictionary}
				key: :class:`string`
					string 't', 'p' or 'a'
					't' is for Transition
					'p' is for Place
					'a' is for Arc
				value: :class:`dictionary`
					{'string' : dictionary}
						The dictionary of statistics.
						key: string label of Transition, Place or Arc
						(while the instance can be changed, label will not)
						values: dictionary of statistics
							{string : integer}
								key: string 'n_m_log'
									number of move in log
									for transition is the relevant transition
									for place is the relevant in place
								key: string 'n_m_model'
									number of move in model
									for transition is the relevant transition
									for place is the relevant in place
								key: string 'n_m_sync'
									number of sync move
									for transition is the relevant transition
									for place is the relevant in place
								key: string 'activated'
									number of times a arc is visited
									for arc only
	is_show_place : :class:`boolean`
		boolean to decide whether to show statistic on places and arcs
	debug : :class:`boolean`
		DEBUG MOD

	Returns
	-------
	viz :
		Returns a graph object
	"""
	decorations = prepare_render_list(
			net=net,
			curr_event_name=curr_event_name,
			initial_marking=initial_marking,
			final_marking=final_marking,
			is_show_deviation=is_show_deviation,
			dic_deviation=dic_deviation,
			is_show_place=True,
			debug=False)
	viz = graphviz_visualization(
			net=net,
			image_format=image_format,
			decorations=decorations,
			is_show_place=is_show_place,
			debug=False)
	return viz


def prepare_render_list(  # noqa: C901
		net, curr_event_name=None,
		initial_marking=None, final_marking=None,
		is_show_deviation=True, dic_deviation=None,
		is_show_place=True,
		debug=False):
	"""
	Prepare the decoration dictionary for the visualizer to generate the image
	of statistics

	Parameters
	----------
	read the function generate_statistic_image

	Returns
	-------
	decorations: :class:`{Object : dictionary}`
		Decorations of the Petri net (says how element must be presented)
		Prepare the decoration list is in another module.
		The allowed items include:
			t : dictionary
				t: :class:`pm4py.objects.petri.petrinet.Transition`
				key: a transition instance of the Petri net
				value: the dictionary of attributes to decorate this transition
			p : dictionary
				p: :class:`pm4py.objects.petri.petrinet.Place`
				key: a place instance of the Petri net
				value: dictionary of attributes to decorate this position
			a : dictionary
				a: :class:`pm4py.objects.petri.petrinet.Arc`
				key: an arc instance of the Petri net
				value: dictionary of attributes to decorate this arc
		Dictionary of attributes, for example:
			'label': label of this transition to been shown
			'fillcolor': string of fillcolor or RGB or HSV, default is defined
			'style': string of randering style, default is 'filled'
			'size': integer of node (height, width), default is automatic
			'fontsize': the font size of label
			'fontcolor': the color of label text
			'penwidth': string of float to draw a arc
			'border': the distance of text to border of figure form
	"""

	decorations = dict()
	# 1. Prepare Transition
	for t in net.transitions:  # ite_key : instance of place or transition
		if t.label is None:
			# When it is silent transition
			decorations[t] = {
				SIZE: SIZE_SILENT,
				STYLE: 'filled',
				FILLCOLOR: 'white',
				LABEL: t.name,
				LABELLOC: 'c',
				FONTCOLOR: 'black',
				FONTSIZE: '11',
				BORDER: '0.1'
			}
			continue
		if dic_deviation is not None and 't' in dic_deviation and t.label in dic_deviation['t']:
			dic_statistic_curr = dic_deviation['t'][t.label]
			n_m_log = dic_statistic_curr[N_M_LOG]
			n_m_model = dic_statistic_curr[N_M_MODEL]
			n_m_sync = dic_statistic_curr[N_M_SYNC]
		else:
			n_m_log = 0
			n_m_model = 0
			n_m_sync = 0

		total_deviation = n_m_log + n_m_model
		total_visited = total_deviation + n_m_sync
		if total_visited != 0:
			percent_of_deviation = float(total_deviation) / total_visited
			percent_of_sync = float(n_m_sync) / total_visited
		else:
			percent_of_deviation = 0.0
			percent_of_sync = 0.0
		# prepare the string decoration of a transition
		lst_label_deco = list()
		# first line
		lst_label_deco.append(t.label)
		lst_label_deco.append('\n')
		# second line
		if is_show_deviation:
			lst_label_deco.append('(deviation, total)\n')
			lst_label_deco.append(
					'({:d}, {:d})'.format(total_deviation, total_visited))
			percent = percent_of_deviation
		else:
			lst_label_deco.append('(sync_move, total)\n')
			lst_label_deco.append(
					'({:d}, {:d})'.format(n_m_sync, total_visited))
			percent = percent_of_sync
		lst_label_deco.append('\n')
		# third line
		lst_label_deco.append('{0:.2%}'.format(percent))
		# Concatenate the string together
		label_deco = ''.join(lst_label_deco)
		if t.label == 'unaffiliated':
			decorations[t] = {
				'bez': t.label,
				SIZE: SIZE_DEFAULT_TRANSITION,
				STYLE: 'filled',
				FILLCOLOR: 'white',
				LABEL: label_deco,
				LABELLOC: 't',
				FONTCOLOR: 'black',
				FONTSIZE: '11',
				BORDER: '0.5'
			}
			continue
		# Fillcolor
		if not is_show_deviation:
			percent_fill_color = 1 - percent
		else:
			percent_fill_color = percent
		if percent_fill_color < 0 or percent_fill_color > 1.0:
			raise Exception('Wrong percentage: {0}'.format(percent))
		# Fillcolor of transition is based on the percentage
		# Using Brewer Color /ylorrd9/ from gold(1) to dark red(9)
		if curr_event_name == t.label:
			fillcolor = COLOR_CURR_EVENT
		elif percent_fill_color >= 0.9:
			fillcolor = 'firebrick4'
		else:
			idx_color = str(int(percent_fill_color * 10 + 1))
			fillcolor = COLOR_BREWER + idx_color
		# Font Color
		if percent_fill_color >= 0.8:
			fontcolor = 'white'
		else:
			fontcolor = 'black'

		decorations[t] = {
			'bez': t.label,
			SIZE: SIZE_DEFAULT_TRANSITION,
			STYLE: 'filled',
			FILLCOLOR: fillcolor,
			LABEL: label_deco,
			LABELLOC: 't',
			FONTCOLOR: fontcolor,
			FONTSIZE: '11',
			BORDER: '0.5'
		}

	# 2. Prepare Place
	for p in net.places:  # ite_key : instance of place or transition
		if dic_deviation is not None and 'p' in dic_deviation and p.name in dic_deviation['p']:
			dic_statistic_curr = dic_deviation['p'][p.name]
			n_m_log = dic_statistic_curr[N_M_LOG]
			n_m_model = dic_statistic_curr[N_M_MODEL]
			n_m_sync = dic_statistic_curr[N_M_SYNC]
		else:
			n_m_log = 0
			n_m_model = 0
			n_m_sync = 0

		total_deviation = n_m_log + n_m_model
		total_visited = total_deviation + n_m_sync
		if total_visited != 0:
			percent_of_deviation = float(total_deviation) / total_visited
			percent_of_sync = float(n_m_sync) / total_visited
		else:
			percent_of_deviation = 0.0
			percent_of_sync = 0.0
		# prepare the string decoration of a transition
		lst_label_deco = list()
		# first line
		if initial_marking is not None and p in initial_marking:
			n_token = initial_marking[p]
		else:
			n_token = ''
		lst_label_deco.append(str(n_token))
		lst_label_deco.append('\n')
		# second line
		if is_show_deviation:
			lst_label_deco.append(
					'({:d}, {:d})'.format(total_deviation, total_visited))
			percent = percent_of_deviation
		else:
			lst_label_deco.append(
					'({:d}, {:d})'.format(n_m_sync, total_visited))
			percent = percent_of_sync
		lst_label_deco.append('\n')
		# third line
		lst_label_deco.append('{0:.2%}'.format(percent))
		# Concatenate the string together
		label_deco = ''.join(lst_label_deco)
		if percent < 0 or percent > 1.0:
			raise Exception('Wrong percentage: {0}'.format(percent))
		# Fillcolor
		# Fillcolor of place is based on the marking
		# show green when number of token > 0
		# show light gray when place is visited but token = 0
		# show white when place is not visited yet
		# show orange when place is final marking.
		if final_marking is not None and p in final_marking:
			fillcolor = 'orange'
		elif n_token == '':
			# place not visited
			fillcolor = 'white'
		else:
			# place visited
			if n_token == 0:
				# when token is 0
				fillcolor = 'grey69'
			else:
				# when token > 0
				fillcolor = 'green'
		# Font Color
		fontcolor = 'black'

		decorations[p] = {
			'bez': p.name,
			SIZE: SIZE_DEFAULT_PLACE,
			STYLE: 'filled',
			FILLCOLOR: fillcolor,
			LABEL: label_deco,
			FONTCOLOR: fontcolor,
			FONTSIZE: '11',
			BORDER: '0.5'
		}

	# 3. Prepare Arc
	for a in net.arcs:
		name = '{0} -> {1}'.format(a.source.name, a.target.name)
		if dic_deviation is not None and 'a' in dic_deviation and name in dic_deviation['a']:
			label_deco = str(dic_deviation['a'][name]['activated'])
		else:
			label_deco = ''
		decorations[a] = {
			'bez': name,
			LABEL: label_deco,
			FONTCOLOR: 'black',
			FONTSIZE: '11.0',
			PENWIDTH: '0.1'
		}

	return decorations


def store_picture(gviz, path):
	""" **Stores generate process model on hard drive** """
	vis_factory.save(gviz, path)
