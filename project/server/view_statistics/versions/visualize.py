#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

import tempfile

from graphviz import Digraph

import server.pm4pyref  # noqa: F401
# unused from pm4py.objects.petri.petrinet import Marking

""" Modified visualize of the pm4py
"""

# Constant
SIZE = 'size'
LABEL = 'label'
LABELLOC = 'labelloc'
FILLCOLOR = 'fillcolor'
STYLE = 'style'
FONTSIZE = 'fontsize'
FONTCOLOR = 'fontcolor'
PENWIDTH = 'penwidth'
BORDER = 'border'


def graphviz_visualization(net, image_format='png', decorations=None, is_show_place=True, debug=False):
	"""
	Modified based on pm4py
	Provides extended visualization for the petrinet with prefix alignment

	Parameters
	----------
	net: :class:`pm4py.entities.petri.petrinet.PetriNet`
		Petri net
	image_format: :class:`string`
		Format that should be associated to the image
	decorations: :class:`{Object : dictionary}`
		Decorations of the Petri net (says how element must be presented)
		Prepare the decoration list is in another module.
		The allowed items include:
			t : dictionary
				t: :class:`pm4py.objects.petri.petrinet.Transition`
				key: a transition instance of the Petri net
				value: the dictionary of attributes to decorate this transition
			p : dictionary
				p: :class:`pm4py.objects.petri.petrinet.Place`
				key: a place instance of the Petri net
				value: dictionary of attributes to decorate this position
			a : dictionary
				a: :class:`pm4py.objects.petri.petrinet.Arc`
				key: an arc instance of the Petri net
				value: dictionary of attributes to decorate this arc
		Dictionary of attributes, for example:
			'label': label of this transition to been shown
			'fillcolor': string of fillcolor or RGB or HSV, default is defined
			'style': string of randering style, default is 'filled'
			'size': integer of node (height, width), default is automatic
			'fontsize': the font size of label
			'fontcolor': the color of label text
			'penwidth': float to draw a arc
	is_show_place : :class:`boolean`
		boolean to decide whether to show statistics on place and arcs
	debug
		PLACEHOLDER: Enables debug mode

	Returns
	-------
	viz :
		Returns a graph object
	"""
	if decorations is None:
		decorations = {}

	filename = tempfile.NamedTemporaryFile(suffix='.gv')
	viz = Digraph(net.name, filename=filename.name, engine='dot')

	# transitions
	for t in net.transitions:
		if t in decorations:
			viz.attr(
					'node', shape='box', fixedsize='true',
					width=str(decorations[t][SIZE][1]),
					height=str(decorations[t][SIZE][0])
			)
			viz.node(
					str(t.name),
					label=decorations[t][LABEL],
					labelloc=decorations[t][LABELLOC],
					style=decorations[t][STYLE],
					# fillcolor="/ylorrd9/"+str(8),
					fillcolor=decorations[t][FILLCOLOR],
					fontsize=decorations[t][FONTSIZE],
					fontcolor=decorations[t][FONTCOLOR]
			)
		else:
			raise Exception(
					'Decoration preparation of transition is incorrect.\n' + 'error Trantion is %s\n' % str(t)
			)

	# places
	for p in net.places:
		if p in decorations:
			viz.attr(
					'node',
					shape='circle',
					width=str(decorations[p][SIZE][1]),
					height=str(decorations[p][SIZE][0])
			)
			if is_show_place:
				viz.node(
						str(p.name),
						label=decorations[p][LABEL],
						style=decorations[p][STYLE],
						fillcolor=decorations[p][FILLCOLOR],
						fontsize=decorations[p][FONTSIZE],
						fontcolor=decorations[p][FONTCOLOR]
				)
			else:
				viz.node(
						str(p.name),
						label='',
						style=decorations[p][STYLE],
						fillcolor=decorations[p][FILLCOLOR],
						fontsize=decorations[p][FONTSIZE],
						fontcolor=decorations[p][FONTCOLOR]
				)
		else:
			raise Exception('Decoration preparation of place is incorrect.')

	# arcs
	for a in net.arcs:
		if a in decorations:
			# print('# DEBUG #')
			# print(decorations[a]['bez'])
			# print(decorations[a][FONTSIZE])
			# print(type(decorations[a][FONTSIZE]))
			if is_show_place:
				viz.edge(
						str(a.source.name),
						str(a.target.name),
						label=decorations[a][LABEL],
						penwidth='1.0',
						fontcolor=decorations[a][FONTCOLOR],
						fontsize=decorations[a][FONTSIZE]
				)
			else:
				viz.edge(
						str(a.source.name),
						str(a.target.name),
						label='',
						penwidth='1.0',
						fontcolor=decorations[a][FONTCOLOR],
						fontsize=decorations[a][FONTSIZE]
				)
		else:
			raise Exception('Decoration preparation of arc is incorrect.')
	viz.attr(overlap='false')
	viz.attr(rankdir='LR')
	viz.format = image_format

	return viz
