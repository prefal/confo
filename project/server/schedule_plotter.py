#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

import matplotlib.pyplot as plt
import copy


def plot_schedule(schedule, offset):
	fig, ax = plt.subplots(figsize=(10, 5))

	if len(schedule) == 0:
		return

	sched_copy = copy.deepcopy(schedule)

	# remove incomplete events
	event_keys = list(sched_copy)
	for x in event_keys:
		if 'enqueued' not in sched_copy[x] or 'executed' not in sched_copy[x] or 'finished' not in sched_copy[x]:
			del sched_copy[x]
	if len(sched_copy) < len(event_keys):
		print(f'Removed {len(event_keys) - len(sched_copy)} incomplete events from schedule.')

	for x in sched_copy:
		sched_copy[x]['enqueued'] -= offset
		sched_copy[x]['executed'] -= offset
		sched_copy[x]['finished'] -= offset

	cases = {}
	for e_index, event in sched_copy.items():
		case_id = event['case_id']
		if case_id not in cases:
			cases[case_id] = []
		cases[case_id].append(event)

	counter = 1
	for case_id in sorted(cases.keys()):
		case = cases[case_id]
		data = [(event['executed'], event['finished'] - event['executed']) for event in case]
		y_coordinate = counter + 0.2
		ax.broken_barh(data, (y_coordinate, 0.6), alpha=0.4)
		for e_index, event in enumerate(case):
			whisker_y = y_coordinate + 0.6 - ((e_index % 20) / 40.0)
			ax.plot([event['enqueued'], event['executed']], [whisker_y, whisker_y], lw=2)
		counter += 1

	ax.set_ylabel('case')
	ax.set_xlabel('milliseconds')

	yticks = list(map(lambda x: x + 1.5, range(len(cases))))
	ax.set_yticks(yticks)
	ax.set_yticklabels(sorted(cases.keys()))
	ax.set_axisbelow(True)
	ax.grid(True)

	plt.savefig('schedule.svg')
