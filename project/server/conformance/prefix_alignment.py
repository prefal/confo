#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module holds the main methods to execute the prefix-alignment algorithm.**
"""

import server.pm4pyref  # noqa: F401
import heapq
import time
from pm4py.algo.conformance.alignments.versions import state_equation_a_star as a_star
from pm4py.algo.conformance.alignments.utils import construct_standard_cost_function, SKIP
from pm4py.objects import petri
from pm4py.objects.petri.utils import construct_trace_net

from server.conformance import auxiliary as aux
from server.objects.alignments import ExtendedSearchTuple


def apply(petri_net, initial_marking, final_marking, alignment_data, *events):
	""" **A version of the prefix alignment algorithm that starts from initial marking**

	Basically uses no optimization (no traceback or incremental approach) and
	computes alignments for the current trace (event sequence in
	old_alignment_data + new_event) from scratch on every call.

	Parameters
	----------
	petri_net : `pm4py.objects.petri.petrinet.PetriNet`
		The Petri net.
	initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the Petri net.
	final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the Petri net.
	alignment_data : `server.objects.alignments.AlignmentData`
		Data on previous prefix-alignment for the event's case.
	*events
		Variable length argument list with contents of type:
		`pm4py.objects.log.log.Event`.
		One or more current events to be checked. Must at least be one.

	Returns
	-------
	new_alignment_data : `server.objects.alignments.AlignmentData`
		The updated alignment data object.
	sync : `bool`
		True, if event caused a sync move. False, if not.
	move_on_model : `bool`
		True if deviation is move on model. False, if move on log. Only applies
		if sync == False.

	Raises
	------
	ValueError
		No event given.
	"""

	# Reference objects
	skip = SKIP

	# Pre-Computations
	if not events:
		raise ValueError('No event given to apply method.')
	for e in events:
		alignment_data.append_event(e)
	trace = alignment_data.get_current_trace()
	trace_net, t_im, t_fm, sync_net, s_im, s_fm, cost_func = _get_sync_trace_net_and_cost_function(
			trace,
			petri_net,
			initial_marking,
			final_marking,
			skip)

	new_search_tuple = _search(trace_net, t_im, t_fm, sync_net, s_im, s_fm, cost_func, skip)
	alignment_data.set_state(new_search_tuple)

	# Output
	sync, move_on_model = aux.provide_feedback(new_search_tuple, skip)
	return alignment_data, sync, move_on_model


def apply_incremental(petri_net, initial_marking, final_marking, alignment_data, event, revert_distance=1):
	""" **Performs the prefix-alignment search (includes: incremental and traceback)**

	Given old alignment data, net and new event of case, an update of the
	alignment data object is returned including the new current
	prefix-alignment. Is to be called for each event of the case.

	Parameters
	----------
	petri_net : `pm4py.objects.petri.petrinet.PetriNet`
		The Petri net.
	initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the Petri net.
	final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the Petri net.
	alignment_data : `server.objects.alignments.AlignmentData`
		Data on previous prefix-alignment for the event's case.
	event : `pm4py.objects.log.log.Event`
		The current event to be checked.
	revert_distance : `int`, optional
		The revert distance for tracebacks in the search algorithm.
		(default is 1)

	Returns
	-------
	new_alignment_data : `server.objects.alignments.AlignmentData`
		The updated alignment data object.
	sync : `bool`
		True, if event caused a sync move. False, if not.
	move_on_model : `bool`
		True if deviation is move on model. False, if move on log. Only applies
		if sync == False.
	statistics : `dict`
		A dictionary carrying information on sync-, log- or model moves for
		original Petri net transitions that occur in the model.
	start_time : `int`
		The unix time stamp when the computation started (server related).
	"""

	start_time = int(round(time.time() * 1000))

	# If you want to demonstrate which process this runs in, you can print the process id:
	# import os
	# print(f'Begin work PID: {os.getpid()}')

	# Reference objects
	skip = SKIP

	# Pre-Computations
	alignment_data.append_event(event)
	trace = alignment_data.get_current_trace()
	trace_net, trace_initial_marking, trace_final_marking, sync_net, sync_initial_marking, sync_final_marking, \
		cost_function = _get_sync_trace_net_and_cost_function(
			trace,
			petri_net,
			initial_marking,
			final_marking,
			skip)

	# Edge Case (This is the first event for this case)
	if len(trace) == 1:
		new_search_tuple = _search(trace_net, trace_initial_marking, trace_final_marking,
									sync_net, sync_initial_marking, sync_final_marking,
									cost_function, skip)
	else:
		old_search_tuple = alignment_data.get_state()
		new_search_tuple = _search_succeed_early(trace_net, trace_initial_marking, trace_final_marking,
										sync_net, sync_initial_marking, sync_final_marking,
										old_search_tuple, event, cost_function, revert_distance, skip)
	alignment_data.set_state(new_search_tuple)

	# Output
	sync, move_on_model = aux.provide_feedback(new_search_tuple, skip)
	statistics = aux.provide_statistic(new_search_tuple, event, petri_net, skip)
	return alignment_data, sync, move_on_model, statistics, start_time


def _get_sync_trace_net_and_cost_function(trace, petri_net, initial_marking, final_marking, skip):
	""" **Returns nets and cost function for prefix_alignment of given net and trace**

	Both trace net of given trace and synchronous product net of trace net and
	Petri net are necessary for the computation of any prefix alignment.
	Petri net and trace must of course be of the same PM context.

	Parameters
	----------
	trace : `pm4py.objects.log.log.Trace`
		The input trace, assumed to be a list of events (activity key assumed to
		be the xes default key).
	petri_net : `pm4py.objects.petri.petrinet.PetriNet`
		The Petri net.
	initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking in the Petri net.
	final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking in the Petri net.
	skip : `string`
		A skip symbol of choice.

	Returns
	-------
	trace_net : `pm4py.objects.petri.petrinet.PetriNet`
		The trace net.
	trace_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the trace net.
	trace_final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the trace net.
	sync_net : `pm4py.objects.petri.petrinet.PetriNet`
		The synchronous product net.
	sync_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the sync net.
	sync_final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the sync net.
	cost_function : `dict`
		Pm4py's standard cost function for the sync net.

	Notes
	-----
	The construction of a new final marking is only semantically correct, if labels in the net are unique.
	"""

	trace_net, trace_initial_marking, trace_final_marking = construct_trace_net(trace)
	sync_prod, sync_initial_marking, sync_final_marking = petri.synchronous_product.construct(
			trace_net,
			trace_initial_marking,
			trace_final_marking, petri_net,
			initial_marking,
			final_marking,
			skip)
	cost_function = construct_standard_cost_function(sync_prod, skip)

	return trace_net, trace_initial_marking, trace_final_marking,\
			sync_prod, sync_initial_marking, sync_final_marking, \
			cost_function


def _search_succeed_early(trace_net, trace_initial_marking, trace_final_marking,
							sync_net, sync_initial_marking, sync_final_marking,
							tpl, event, cost_function, revert_distance, skip):
	""" **Searches for a sync move before actually calling _search**

	A wrapper around the actual search method, to succeed early respecting the
	given pseudocode [1]_ :

	*Whenever there is at least one sync move available,
	the actual search method does not have to be called. The corresponding
	search tuples can be extended easily.*

	Parameters
	----------
	trace_net : `pm4py.objects.petri.petrinet.PetriNet`
		The trace net.
	trace_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the trace net.
	trace_final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the trace net.
	sync_net : `pm4py.objects.petri.petrinet.PetriNet`
		The synchronous product net.
	sync_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking in the sync net.
	sync_final_marking : `pm4py.objects.petrinet.Marking`
		The final marking of the sync net.
	tpl : `server.objects.alignments.ExtendedSearchTuple`
		The previous search tuple.
	event : `pm4py.objects.log.log.Event`
		The current event.
	cost_function : `dict`
		Pm4py's standard cost function for the sync net.
	revert_distance : `int`
		The revert distance for the traceback.
	skip : `string`
		The skip symbol used in the given nets.

	Returns
	-------
	result : `server.objects.alignments.ExtendedSearchTuple`
		The found search tuple.

	References
	----------
	.. [1] van Zelst, Sebastiaan J., "Online conformance checking: relating
		event streams to process models using prefix-alignments", International
		Journal of Data Science and Analytics, 2017.
	"""

	# Pre-Computations
	marking = aux.map_marking_into_sync_net(tpl, sync_net)
	tpl.m = marking
	tpl.np = list()
	incidence_matrix = petri.incidence_matrix.construct(sync_net)
	ini_vec, fin_vec, cost_vec = a_star.__vectorize_initial_final_cost(incidence_matrix, sync_initial_marking,
																		sync_final_marking, cost_function)

	sync_move_found, result = _search_sync_move(trace_net, sync_net, cost_function,
							incidence_matrix, cost_vec, fin_vec,
							event, skip, to_traverse=[tpl])

	if not sync_move_found:
		reverted_tuple = aux.do_trace_back(tpl, revert_distance)
		# cost_upper_bound = aux.cost_upper_bound(tpl, sync_net, event, cost_function, skip)
		result = _search(trace_net, trace_initial_marking, trace_final_marking,
						sync_net, sync_initial_marking, sync_final_marking,
						cost_function, skip, reverted_tuple)  # , cost_upper_bound)
	return result


def _search_sync_move(trace_net, sync_net, cost_function,  # noqa: C901
						incidence_matrix, cost_vec, fin_vec,
						event, skip, to_traverse=None, marked=None):
	""" **Searches for any enabled sync move in the sync net from the given marking**

	Such 'sync move'-transitions in the sync net may be separated by arbitrarily
	many 'tau move'-transitions. This function wraps the functionality to search
	for a sync move from a given marking in the net while neglecting tau moves.

	The method is build up to execute a breadth first search on the sync net's
	state space.

	Upon encountering only tau moves this function is called recursively on the
	marking after the 'tau-move'-transitions. Recursion depth is minimum path
	length to the nearest 'sync move'-transition.

	Parameters
	----------
	trace_net : `pm4py.objects.petri.petrinet.PetriNet`
		The trace net.
	sync_net : `pm4py.objects.petri.petrinet.PetriNet`
		The synchronous product net.
	cost_function : `dict`
		Pm4py's standard cost function for the sync net.
	incidence_matrix : `pm4py.objects.petri.incidence_matrix.IncidenceMatrix`
		The incidence matrix of the synchronous product net.
	cost_vec : `list`
		A cost vector internal to the LP of the A-Star algorithm.
	fin_vec : `list`
		A cost vector internal to the LP of the A-Star algorithm.
	event : `pm4py.objects.log.log.Event`
		The current event.
	skip : `string`
		The skip symbol used in the given nets.
	to_traverse : `list`, optional
		A list of search tuples from which a sync move is to be searched from.
		(default is None)
	marked : `set`, optional
		Search Tuples for which a search was already done to prevent a circular
		search. Is internal to the recursive nature of this function. (default
		is None)

	Returns
	-------
	bool
		True, if there exists a subsequent sync move. False, if not.
	result : `server.objects.alignments.ExtendedSearchTuple`
		The found search tuple. None if 1st return parameter is False.

	"""

	marked = set() if marked is None else marked
	tmp_traverse = set()
	tmp_transitions = dict()

	if to_traverse is None or not to_traverse:
		return False, None

	for tpl in to_traverse:
		marking = tpl.m
		if marking not in marked:
			marked.add(marking)
			for t in petri.semantics.enabled_transitions(sync_net, marking):
				if aux.is_sync_move(t, skip) and t.label[0] == t.label[1] == event['concept:name']:
					new_marking = petri.semantics.execute(t, sync_net, marking)
					mapped_trace_net_marking = aux.map_marking_into_new_model(trace_net, tpl.tm)
					trace_net_marking = aux.do_step_in_trace_net(t.label[0], trace_net, mapped_trace_net_marking)
					h, x = a_star.__compute_exact_heuristic(sync_net, incidence_matrix, new_marking, cost_vec, fin_vec)
					tr = tpl.tr + 1
					if not tpl.np or tpl.np is None:
						new_path = [t]
					else:
						new_path = tpl.np.copy()
						new_path.append(t)
					result = ExtendedSearchTuple(tpl.g + cost_function[t] + h,
													tpl.g + cost_function[t], h,
													new_marking, tpl, t,
													x, a_star.__trust_solution(x),
													trace_net_marking,
													tpl.sp, tr, new_path)
					return True, result

				elif aux.is_tau_move(t, skip):
					if tpl not in tmp_transitions.keys():
						tmp_transitions[tpl] = set()
					tmp_transitions[tpl].add(t)

	for tpl in tmp_transitions.keys():
		for t in tmp_transitions[tpl]:
			new_marking = petri.semantics.execute(t, sync_net, tpl.m)
			h, x = a_star.__compute_exact_heuristic(sync_net, incidence_matrix, new_marking, cost_vec, fin_vec)
			sp = tpl.sp + 1
			if not tpl.np or tpl.np is None:
				new_path = [t]
			else:
				new_path = tpl.np.copy()
				new_path.append(t)
			tmp_traverse.add(ExtendedSearchTuple(tpl.g + cost_function[t] + h,
												tpl.g + cost_function[t], h,
												new_marking, tpl, t,
												x, a_star.__trust_solution(x),
												tpl.tm,
												sp, tpl.tr, new_path))
	return _search_sync_move(trace_net, sync_net, cost_function,
										incidence_matrix, cost_vec, fin_vec,
										event, skip, tmp_traverse, marked)


def _search(trace_net, trace_initial_marking, trace_final_marking,  # noqa: C901
			sync_net, sync_initial_marking, sync_final_marking,
			cost_function, skip, old_search_tuple=None, cost_upper_bound=None):
	""" **Executes a modified version of the A* algorithm**

	The original A* was modified to work on prefix-alignments instead of classic
	alignments. Unlike the original, this version does not terminate when the
	`sync_final_marking` is found. Instead, it keeps track of the progress in
	the trace net and terminates when the current search tuple has reached the
	`trace_final_marking`.

	Further, it allows to continue the search starting with an `old_search_tuple`,
	making incremental computations possible.

	The function works with `ExtendedSearchTuple` instead of `SearchTuple` and updates
	the properties `sp` (skip penalty), `tm` (trace net marking), `tr` (trace net move
	reward) and `np` (new path).

	Respects cost upper bound as an option.

	Parameters
	----------
	trace_net : `pm4py.objects.petri.petrinet.PetriNet`
		The trace net. It is necessary to realize the early termination of the
		alignment search.
	trace_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the trace net.
	trace_final_marking : `pm4py.objects.petri.petrinet.Marking`
		The final marking of the trace net.
	sync_net : `pm4py.objects.petri.petrinet.PetriNet`
		The synchronous product net.
	sync_initial_marking : `pm4py.objects.petri.petrinet.Marking`
		The initial marking of the sync net.
	sync_final_marking : `pm4py.objects.petrinet.Marking`
		The final marking of the sync net.
	cost_function : `dict`
		Pm4py's standard cost function for the sync net.
	skip : `string`
		The skip symbol used in the given nets.
	old_search_tuple : `server.objects.alignments.ExtendedSearchTuple`, optional
		A search tuple of a previous iteration. (default is None)
	cost_upper_bound : `int`, optional
		The value for the cost upper bound set, used whenever there is a
		traceback done. (default is None)

	Returns
	-------
	result : `server.objects.alignments.ExtendedSearchTuple`
		The found search tuple.

	Raises
	------
	TypeError
		If old_search_tuple is given, but is not if type ExtendedSearchTuple.
	"""

	# Trace net parameters
	trace_net_marking = trace_initial_marking

	# Modified __search from PM4Py state_equation_a_star.py
	incidence_matrix = petri.incidence_matrix.construct(sync_net)
	ini_vec, fin_vec, cost_vec = a_star.__vectorize_initial_final_cost(incidence_matrix, sync_initial_marking,
																		sync_final_marking, cost_function)

	closed = set()
	h, x = a_star.__compute_exact_heuristic(sync_net, incidence_matrix, sync_initial_marking, cost_vec, fin_vec)
	ini_state = ExtendedSearchTuple(0 + h, 0, h, sync_initial_marking, None, None, x, True, trace_net_marking, 0, 0,
									list())
	if old_search_tuple is not None:
		if not isinstance(old_search_tuple, ExtendedSearchTuple):
			raise TypeError('Parameter old_search_tuple should be of type ExtendedSearchTuple, but is of type: ',
							str(type(old_search_tuple)))
		else:
			# set trust to false such that heuristic computation for this
			# initial tuple is re-done at the beginning of the while loop
			new_tm = aux.map_marking_into_new_model(trace_net, old_search_tuple.tm)
			new_m = aux.map_marking_into_sync_net(old_search_tuple, sync_net)
			ini_state = ExtendedSearchTuple(0, old_search_tuple.g, 0, new_m,
											old_search_tuple.p, old_search_tuple.t, x, False, new_tm,
											old_search_tuple.sp, old_search_tuple.tr, list())
	open_set = [ini_state]
	while not len(open_set) == 0:
		curr = heapq.heappop(open_set)
		if not curr.trust:
			h, x = a_star.__compute_exact_heuristic(sync_net, incidence_matrix, curr.m, cost_vec, fin_vec)
			tp = ExtendedSearchTuple(curr.g + h, curr.g, h, curr.m, curr.p, curr.t, x,
									a_star.__trust_solution(x), curr.tm, curr.sp, curr.tr, curr.np)
			heapq.heappush(open_set, tp)
			heapq.heapify(open_set)
			continue

		current_marking = curr.m
		closed.add(current_marking)
		if aux.net_done(curr.tm, trace_final_marking):
			return curr
		for t in a_star.petri.semantics.enabled_transitions(sync_net, current_marking):
			if curr.t is not None and a_star.__is_log_move(curr.t, skip) and a_star.__is_model_move(t, skip):
				continue
			new_marking = petri.semantics.execute(t, sync_net, current_marking)
			trace_net_marking = aux.do_step_in_trace_net(t.label[0], trace_net, curr.tm)
			if not curr.tm == trace_net_marking:
				tr = curr.tr + 1
			else:
				tr = curr.tr
			if new_marking in closed:
				continue
			g = curr.g + cost_function[t]

			alt = next((enum[1] for enum in enumerate(open_set) if enum[1].m == new_marking), None)
			if alt is not None:
				if g >= alt.g:
					continue
				open_set.remove(alt)
				heapq.heapify(open_set)

			# skip, if cost upper bound given and reached
			if cost_upper_bound is not None:
				if g > (cost_upper_bound + curr.sp):
					continue

			h, x = a_star.__derive_heuristic(incidence_matrix, cost_vec, curr.x, t, curr.h)
			if aux.is_sync_move(t, skip):
				sp = curr.sp
			else:
				sp = curr.sp + 1
			np = curr.np.copy()
			np.append(t)
			tp = ExtendedSearchTuple(g + h, g, h, new_marking, curr, t, x, a_star.__trust_solution(x),
										trace_net_marking, sp, tr, np)
			heapq.heappush(open_set, tp)
			heapq.heapify(open_set)
