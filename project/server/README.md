# Server

The web service application that receives an event stream and computes corresponding prefix-alignments.

View [our gitlab page](https://prefal.gitlab.io/confo) for details on how to run this server application.
