#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

import sys
import os
import inspect


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)