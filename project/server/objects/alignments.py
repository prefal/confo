#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **Alignments**
"""

import server.pm4pyref  # noqa: F401
from dataclasses import dataclass
from pm4py.algo.conformance.alignments.versions.state_equation_a_star import SearchTuple
from pm4py.objects.log.log import Trace, Event
from pm4py.objects import petri


class AlignmentData:
	""" **Represents all storage needed to compute the a prefix-alignment**

	Holds all past storage necessary to compute prefix-alignment for a new
	event. This includes all past events and the latest (optimal) prefix-
	alignment

	Parameters
	----------
	case_id : `string`
		The case id of the case that this object refers to.
	"""

	def __init__(self, case_id):
		self.__case_id = case_id
		self.__events = Trace(list(), attributes={'concept:name': str(case_id), 'creator': 'ConfOSystem'})
		self.__state = ExtendedSearchTuple(0, 0, 0, None, None, None, None, False, None, 0, 0, list())

	def __repr__(self):
		return str('CaseID: ' + str(self.__case_id) + ',\n Past events: ' + str([x['concept:name'] for x in
							self.__events]) + ',\n Past alignments: ' + str(self.__state))

	def get_case_id(self):
		""" **Returns the case_id**

		Returns
		-------
		case_id : `string`
			The case id of the case that this object refers to.
		"""

		return self.__case_id

	def append_event(self, event):
		""" **Appends an event to the partial trace object**

		Parameters
		----------
		event : `pm4py.objects.log.log.Event`
			The event to append.

		Raises
		------
		TypeError
			If given event is not of the correct type.
		"""

		if isinstance(event, Event):
			self.__events.append(event)
		else:
			raise TypeError('Parameter event should be of type Event, but is of type: ', str(type(event)))

	def get_current_trace(self):
		""" **Return the current partial trace of this case**

		Returns
		-------
		events : `pm4py.objects.log.log.Trace`
			The current partial trace.
		"""

		return self.__events

	def set_state(self, new_state):
		""" **Sets the field for the optimal search tuple of current
		partial trace**

		Parameters
		----------
		new_state : `server.objects.alignments.ExtendedSearchTuple`
			The new state to set.

		Raises
		------
		TypeError
			If given new_state is not of the correct type.
		"""

		if isinstance(new_state, ExtendedSearchTuple):
			self.__state = new_state
		else:
			raise TypeError('Parameter new_state should be of type ExtendedSearchTuple, but is of type: ',
							str(type(new_state)))

	def get_state(self):
		""" **Return the best alignment for the current partial trace**

		Returns
		-------
		events : `pm4py.objects.log.log.Trace`
			The ExtendedSearchTuple for the best alignment of the current
			partial trace.
		"""

		return self.__state


@dataclass
class ExtendedSearchTuple(SearchTuple):
	""" **A recursive data structure to represent a state in the alignment search**

	Parameters
	----------
	f : `float`
		The total costs: g + h.
	g : `float`
		The actual costs for executed transitions.
	h : `float`
		The heuristic value for the residual potential of the current alignment.
	m : `pm4py.objects.petri.petrinet.Marking`
		The marking in the synchronous product net after the current alignment.
		This is the main crucial state information that allows to extend the
		search in the incremental version of the prefix alignment algorithm.
	p : `Any`
		Parent pointer to a search tuple that expresses the state of the
		alignment before the transition given by **t** was executed.
	t : `pm4py.objects.petri.petrinet.PetriNet.Transition`
		The last transition of the alignment in the synchronous product net
		which lead to the marking given by **m**.
	x : `Any`
		A Boolean parameter related to the LP problem internal to the A-Star
		algorithm.
	trust : `bool`
		A Boolean trust parameter that is internal to the A_Star algorithm.
	tm : `pm4py.objects.petri.petrinet.Marking`
		The marking in the current trace net. To abort the alignment search for
		partial traces, the trace net of the partial trace is run in parallel,
		and the search is stopped as soon as the trace net reached its final
		marking. The trace net is run in parallel in the meaning that whenever
		the A-Star decides to execute transition t=[l0, l1], transition with
		label l0 in the trace net is executed too, given that it exists and is
		enabled by the old trace net marking.
	sp : `int`
		Skip penalty. To avoid a tail of model moves at the end of the alignment
		(because this lowers the heuristic potential of the alignment and ranks
		it better), a penalty is given for any kind of non 'sync move'-
		transition executed in the current alignment.
	tr : `int`
		Trace net move reward. Similar to the skip penalty, one wants to reward
		states that accomplish more moves in the trace net, as this corresponds
		to actually finding an alignment that focuses on having 'sync move'-
		or 'log move'-transitions.
	np : `list`
		New path. As the actual alignment is embedded in this recursive data
		structure, this field holds a list of transitions found in one alignment
		increment. It is not relevant to the actual search state info, but
		exists to see what transitions of the alignment were newly added and
		then to derive feedback about what types of moves were made (sync-,
		log- or model-move). Is reset on each search increment.
	"""

	tm: petri.petrinet.Marking  # trace net marking
	sp: int						# skip penalties
	tr: int						# trace net move reward
	np: list					# new path

	def __lt__(self, other):
		if self.f < other.f:
			return True
		elif other.f < self.f:
			return False
		else:
			if self.sp < other.sp:
				return True
			elif other.sp < self.sp:
				return False
			else:
				if self.tr < other.tr:
					return False
				elif other.tr < self.tr:
					return True
				else:
					if self.trust == other.trust:
						if self.h < other.h:
							return True
						else:
							return False
					else:
						return self.trust

	def __get_firing_sequence(self):
		ret = []
		if self.p is not None:
			ret = ret + self.p.__get_firing_sequence()
		if self.t is not None:
			ret.append(self.t)
		return ret

	def __repr__(self):
		string_build = ['\nm=' + str(self.m),
						' tm=' + str(self.tm),
						' sp=' + str(self.sp),
						' tr=' + str(self.tr),
						' f=' + str(self.f),
						' g=' + str(self.g),
						' h=' + str(self.h),
						' path=' + str(self.__get_firing_sequence()),
						' np=' + str([t.label for t in self.np]) + '\n']
		return " ".join(string_build)

	def __hash__(self):
		h = hash(self.f) + hash(self.g) + hash(self.h) + hash(self.m) + hash(self.t) + hash(self.tm) + \
			hash(self.sp) + hash(self.tr)
		h = h % 31091
		return h
