#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **Locks**
"""

from multiprocessing import Condition, Lock
from multiprocessing.sharedctypes import RawValue
from server.exceptions import TooManyReadReleases


class ReadWriteLock:
	"""
	A lock object that allows many simultaneous read-locks, but only one
	write-lock; made to work with multiprocessing. A process must wait whenever
	the upper bound on reads is exceeded, i.e., _readers would be equal to
	max_readers. This Lock raises an exception whenever the lower bound on reads
	is exceeded, i.e., there are more release_read() calls than acquire_read().

	Parameters
	----------
	multiple_reads : `int`, optional
		The maximum number of concurrent reads allowed. (the default is 1)

	Raises
	------
	ValueError
		If parameter multiple_reads is not positive.
	"""

	def __init__(self, multiple_reads=1):
		self._read_ready = Condition(Lock())
		self._readers = RawValue('i', 0)
		if multiple_reads <= 0:
			raise ValueError('Variable multiple_reads must be greater or equal to 1 (one).')
		self._max_readers = multiple_reads

	def acquire_read(self):
		""" **Acquires a read lock**

		Blocks only if a process has acquired the write lock.
		"""

		with self._read_ready:
			while self._readers_greater_then(self._max_readers - 1):
				self._read_ready.wait()
			self._readers.value += 1

	def release_read(self):
		""" **Releases a read lock**

		Raises
		------
		TooManyReadReleases
			If the _readers value's decrement results in a negative value.
		"""

		with self._read_ready:
			self._readers.value -= 1
			if self._readers.value < 0:
				self._readers.value = 0
				raise TooManyReadReleases('The _readers value\'s increment resulted in a negative value.')
			if self._readers.value == 0:
				self._read_ready.notify_all()

	def acquire_write(self):
		""" **Acquires a write lock**

		Blocks until there are no acquired read or write locks.
		"""

		self._read_ready.acquire()
		while self._readers_greater_then(0):
			self._read_ready.wait()

	def release_write(self):
		""" **Releases a write lock** """

		self._read_ready.release()

	def _readers_greater_then(self, this):
		""" **Tests if the number of readers is greater then the given parameter**

		Returns
		-------
		bool
			True, if condition holds. False, if not.
		"""

		return self._readers.value > this
