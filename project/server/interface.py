#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **The server http interface is defined in this module.**
"""

import server.pm4pyref  # noqa: F401
import jinja2
import aiohttp_jinja2
import os
import random
import time
from pm4py.objects.log.log import Event
from json.decoder import JSONDecodeError
from aiohttp import web
import asyncio
from concurrent.futures import ProcessPoolExecutor
import atexit
import functools
import statistics
import logging

from server.conformance import prefix_alignment as prefal
from server import constants
from server.objects.alignments import AlignmentData
from server.view_statistics import factory as genstat


class ConfoServer():
	""" **The main class that holds the http interface routes**

	Parameters
	----------
	plot_schedule_on_shutdown : `bool`, optional
		Whether to create a request handling schedule plot when the application
		is terminated.
	"""

	QUEUE_SLEEP_DURATION = 0.01

	def __init__(self, plot_schedule_on_shutdown=False):
		this_dir = os.path.dirname(os.path.abspath(__file__))

		self.executor = ProcessPoolExecutor(max_workers=constants.MAX_NUMBER_OF_WORKERS)
		self.request_counter = 0
		self.response_counter = 0
		self.case_locked = {}
		self.case_queue = {}
		self.min_proc_time = None
		self.max_proc_time = 0
		self.proc_times = list()
		self.loop = asyncio.get_event_loop()

		# Schedule printing
		self.schedule = {}  # Stores arrival time, execution start and finish times of each event
		self.startup = self._get_ms()
		if plot_schedule_on_shutdown:
			atexit.register(self._save_schedule)

		self.app = web.Application()

		aiohttp_jinja2.setup(self.app, loader=jinja2.FileSystemLoader(os.path.join(this_dir, 'templates')))

		self.app.router.add_static('/static', os.path.join(this_dir, 'static'))
		self.app.router.add_post('/check/event', self.check_event, name='check')
		self.app.router.add_get('/statistics/model', self.statistics_model, name='stats')
		self.app.router.add_get('/', self.homepage, name='homepage')

	def run(self):
		logging.info(f' * Asynchronous HTTP server started: PID {os.getpid()}')
		web.run_app(self.app, port=constants.HTTP_PORT)

	def _get_ms(self):
		return int(round(time.time() * 1000))

	def _save_schedule(self):
		logging.info('Shutting down, saving schedule..')
		from server import schedule_plotter
		schedule_plotter.plot_schedule(self.schedule, self.startup)

	def _add_event_to_schedule(self, request_index, case_id):
		self.schedule[request_index] = {'case_id': case_id, 'enqueued': self._get_ms()}

	async def check_event(self, request):
		""" **Checks Conformance of Single Event**

		**POST '/check/event'**

		This function allows to check a single event of a trace for conformance
		to the process model that resides on this server.

		Parameters
		----------
		request : `json`
			Event data as json object with keys:

			- ``request`` : Response object (`dict`) with keys:

				- ``event`` : Event object (`dict`) with keys:

					- ``case_id`` : The case that this event refers to (`str`).
					- ``activity_name`` : The activity name that specifies this event (`str`).

		Returns
		-------
		response : `json`
			Conformance result type returned as json object with keys:

			- ``response`` : Response object (`dict`) with keys:

				- ``type`` : Response type (`str`).

				[type is either 'OK', 'ALERT' or 'REJECT']

				- ``move_on_model`` : True if move on model detected (`bool`).

				[only included if type is ALERT]

		http status code : `int`

		Example
		-------
		Run::

			$ curl -X POST 127.0.0.1:5000/check/event \\
			-d '{"request" :{"event": {"case_id": "123456", "activity_name": "check_thoroughly"}}}' \\
			-H 'Content-Type: application/json'

		"""

		self.request_counter += 1
		request_index = self.request_counter

		# Print connection details
		peername = request.transport.get_extra_info('peername')
		if peername is not None:
			host, port = peername
			logging.info(f'Req{request_index}: Client connection at {host}:{port}.')

		try:
			data = await request.json()
		except JSONDecodeError:
			type = 'REJECT'
			error_id = 1
			error_message = 'JSON invalid. Please provide a legal format.'
			logging.warning(f'Req{request_index}: Syntax Error: {error_message}')
			self.response_counter += 1
			return web.json_response({'response': dict(type=type, error_id=error_id, error_message=error_message)})

		if 'request' in data:
			request_data = data['request']
			if 'event' in request_data:
				event = request_data['event']
				if 'case_id' in event and 'activity_name' in event:
					if event['case_id'] == '' or event['activity_name'] == '':
						type = 'REJECT'
						error_id = 3
						error_message = 'The request body contains empty values.'
						logging.warning(f'Req{request_index}: Syntax Error: {error_message}')
						self.response_counter += 1
						return web.json_response({'response': dict(type=type, error_id=error_id, error_message=error_message)})
					else:
						self._add_event_to_schedule(request_index, event['case_id'])
						if constants.DEBUG_MODE:
							logging.info(f'Req{request_index}: DEBUG_MODE response')
							self.response_counter += 1
							return self._debug_response()
						else:
							event_obj = Event({'concept:name': event['activity_name']})
							logging.info(f'Req{request_index}: case_id = {event["case_id"]} activity = {event["activity_name"]}')
							sync, move_on_model = await self._compute_response(request_index, event['case_id'],
																				event_obj, prefal.apply_incremental)
							# prepare feedback
							if sync:
								return_type = 'OK'
								logging.info(f'Req{request_index}: Event OK')
							else:
								return_type = 'ALERT'
								logging.warning(f'Req{request_index}: Deviation found!')

							logging.info(f'Req{request_index}: Responding to valid request.')
							self.response_counter += 1
							return web.json_response({'response': dict(type=return_type, move_on_model=move_on_model)})
				else:
					return_type = 'REJECT'
					error_id = 2
					error_message = 'The request body does not contain the necessary key(s): (\'case_id\' or \
									\'activity_name\').'
					logging.warning(f'Req{request_index}: Syntax Error: ' + error_message)
					self.response_counter += 1
					return web.json_response({'response': dict(type=return_type, error_id=error_id, error_message=error_message)})
			else:
				return_type = 'REJECT'
				error_id = 2
				error_message = 'The request body does not contain the necessary key(s): (\'event\').'
				logging.warning(f'Req{request_index}: Syntax Error: ' + error_message)
				self.response_counter += 1
				return web.json_response({'response': dict(type=return_type, error_id=error_id, error_message=error_message)})

		else:
			return_type = 'REJECT'
			error_id = 2
			error_message = 'The request body does not contain the necessary key(s): (\'request\').'
			logging.warning(f'Req{request_index}: Syntax Error: ' + error_message)
			self.response_counter += 1
			return web.json_response({'response': dict(type=return_type, error_id=error_id, error_message=error_message)})

	@aiohttp_jinja2.template('deviations.html')
	async def statistics_model(self, request):
		"""	**Returns Deviation Statistics View**

		**GET '/statistics/model'**

		This function allows a human user to view the deviations statistics in
		form of a decorated version process model. This model is served as a png
		file on a static website.

		Returns
		-------
		html document : (`str`)
			With the image file.

		Example
		-------
		Visit https://127.0.0.1:5000/statistics/model in the browser.
		"""

		logging.debug('View of deviation statistics is requested')

		if constants.STATISTIC_STORAGE is not None:
			status, stats = constants.STATISTIC_STORAGE.read_statistic_data_all()
		else:
			logging.warning('Statistic data storage not available.')

		if constants.DEBUG_MODE:
			return {'filename': 'model-with-decorations-sample.png'}
		else:
			statistics_data = dict()
			if status:
				statistics_data['t'] = stats
			else:
				statistics_data = None
			gviz = genstat.apply(
					constants.NET, image_format='png', curr_event_name=None,
					initial_marking=constants.INI, final_marking=constants.FIN,
					is_show_deviation=True, dic_deviation=statistics_data,
					is_show_place=False, variant=genstat.STATISTIC_ONLY)
			dir = os.path.join('static', 'images')
			filename = 'model-with-decorations.png'
			path = os.path.join(dir, filename)
			genstat.store_picture(gviz, path)
			return {'filename': f'{filename}?t={int(time.time()*1000)}'}

	@aiohttp_jinja2.template('index.html')
	async def homepage(self, request):
		"""	**Returns Homepage View**

		**GET '/'**

		This endpoint serves a simple landing page so that a human user can
		easily identify that the server is running.

		Returns
		-------
		html document : (`str`)
			The HTML webpage.

		Example
		-------
		Visit https://127.0.0.1:5000/ in the browser.
		"""

		logging.debug('Homepage is requested')
		interface_stats = f'Resp/Rcvd: {self.response_counter}/{self.request_counter}. '
		median = statistics.median(self.proc_times) if len(self.proc_times) > 0 else '-'
		interface_stats += f'Min/med/max exec: {self.min_proc_time}/{median}/{self.max_proc_time}ms. '

		# Print locked cases
		interface_stats += 'Locked cases: '
		interface_stats += str([case_id for case_id in self.case_locked if self.case_locked[case_id]])
		return {'interface_stats': interface_stats}

	def _debug_response(self):
		# Random OK or ALERT
		outcome = random.randint(0, 1)
		if outcome == 0:
			type = 'OK'
			logging.info('DEBUG Response: Event OK')
			return web.json_response({'response': {'type': type}})
		else:
			deviation = random.randint(0, 1)
			if deviation == 0:
				type = 'ALERT'
				logging.warning('DEBUG Response: move on model')
				return web.json_response({'response': dict(type=type, move_on_model=True)})
			else:
				type = 'ALERT'
				logging.warning('DEBUG Response: move on log')
				return web.json_response({'response': dict(type=type, move_on_model=False)})

	async def _compute_response(self, request_index, case_id, event, conformance_worker_fun):
		if case_id not in self.case_locked:
			self.case_locked[case_id] = False
			self.case_queue[case_id] = []

		self.case_queue[case_id].append(request_index)

		while self.case_locked[case_id] or self.case_queue[case_id][0] != request_index:
			await asyncio.sleep(self.QUEUE_SLEEP_DURATION)

		self.case_locked[case_id] = True
		self.case_queue[case_id].remove(request_index)

		# read old data
		exists, old_alignment_data = constants.ALIGNMENT_STORAGE.read_alignment_data(case_id)
		if not exists:
			old_alignment_data = AlignmentData(case_id)

		sync, move_on_model = None, None

		try:
			# compute new alignment
			part_fun = functools.partial(conformance_worker_fun, constants.NET, constants.INI, constants.FIN,
				old_alignment_data, event, revert_distance=constants.ALGO_WINDOW_SIZE)
			new_alignment_data, sync, move_on_model, statistics, execution_time = await self.loop.run_in_executor(
					self.executor, part_fun)

			self.schedule[request_index]['finished'] = self._get_ms()

			# write new data
			constants.ALIGNMENT_STORAGE.write_alignment_data(new_alignment_data)
			constants.STATISTIC_STORAGE.write_statistic_data(statistics)
			self.schedule[request_index]['executed'] = execution_time

			# keep track of shortest/longest execution times
			proc_time = self.schedule[request_index]['finished'] - self.schedule[request_index]['executed']
			if self.min_proc_time is not None:
				self.min_proc_time = min(self.min_proc_time, proc_time)
			else:
				self.min_proc_time = proc_time
			self.max_proc_time = max(self.max_proc_time, proc_time)
			self.proc_times.append(proc_time)
		except Exception as exc:
			message = f'The coroutine raised an exception: {exc!r} in request {request_index}.'
			logging.warning(message)
		self.case_locked[case_id] = False

		return sync, move_on_model
