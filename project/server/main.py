#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **The main entry point of the application.**

This module is the main entry point for the server application. It takes care
of the start-up process when executed, which includes loading the config,
adjusting constants and starting the interfaces.
"""

import sys
import os
import inspect
import logging

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from server import constants
from server.exceptions import IllegalConfigException


def handler_generate_config(path='', filename='server.ini'):
	""" **Handles the** --generate-config **flag**

	This function generates a sample configuration file at the specified path
	and exits the application with exit code 0.
	"""

	import configparser
	config = configparser.ConfigParser()
	config['config'] = \
		{'max_workers': constants.DEFAULT_MAX_NUMBER_OF_WORKERS, 'path_to_petri_net':
			constants.DEFAULT_NET_PATH, 'revert_window_size': constants.DEFAULT_ALGO_WINDOW_SIZE,
			'port': constants.DEFAULT_HTTP_PORT, 'logfile': constants.DEFAULT_LOG_FILE}
	file = os.path.join(path, filename)
	with open(file, 'w') as configfile:
		config.write(configfile)
	print('Generated sample configuration file: server.ini')
	sys.exit(0)


def handler_no_config():
	""" **Handles the** --no-config **flag**

	This function sets the constant's to their default values.
	"""

	constants.default()
	message = 'Using "--no-config"'
	logging.info(message)
	print(message)


def handler_debug():
	""" **Handles the** --debug **flag**

	This function sets the constant's debug value to true.
	"""

	constants.DEBUG_MODE = True


def parse_flags(args):
	""" **Handles command line args**

	This functions defines behavior when certain command line args are used.
	Undefined input raises an exception and exits the application with exit
	code 2. For certain flags it calls an individual handler function also
	defined in this module.

	Parameters
	----------
	args : `list`
		The list of command line input strings

	Examples
	--------

	Run::

		main.py -h

	to get info on the supported flags and parameters.
	"""

	import argparse
	parser = argparse.ArgumentParser(description='Starts the main conformance checking server.')
	parser.add_argument('--debug', action='count', help='start the server in debug mode (random responses on '
														'conformance checking interface & sample Petri net on '
														'statistics interface)')
	parser.add_argument('--no-config', action='count', help='start server with default values')
	parser.add_argument('--generate-config', action='count', help='generate a sample configuration file and exit')
	parser.add_argument('--path', nargs=1, help='specify a path to the configuration file different than the current '
												'directory')
	parser.add_argument('--filename', nargs=1, help='specify the name of the configuration file different than '
													'server.ini')
	arguments = vars(parser.parse_args(args))
	# print(arguments)
	path = ''
	filename = 'server.ini'
	if arguments['path'] is not None:
		path = arguments['path'][0].strip()
	if arguments['filename'] is not None:
		filename = arguments['filename'][0].strip()

	if arguments['debug'] is not None and arguments['debug'] >= 1:
		handler_debug()

	if arguments['generate_config'] is not None and arguments['generate_config'] >= 1:
		handler_generate_config(path, filename)
	elif arguments['no_config'] is not None and arguments['no_config'] >= 1:
		handler_no_config()
	else:
		load_config_and_define_constants(path, filename)


def load_config_and_define_constants(path='', filename='server.ini'):
	""" **Loads and returns values from the config file**

	The config file for the server-side application by default resides in the
	file **server.ini** and has a section **config** that holds parameters that
	alter the behavior of the server. The path and name of the config file can
	be changed. Subsequently, this funtion updates the constants in
	**constants.py** before starting remaining components of the application.

	Parameters
	----------
	max_workers : `string`
		Specifies the max number of processes by the multiplication of this
		value and the number of cpus on the system; content of **server.ini**.
	path_to_petri_net : `string`
		Specifies the path to the **.pnml** file that holds the Petri net;
		content of **server.ini**.
	revert_window_size : `string`
		Specifies the size of the window used in the prefix alignment algorithm;
		content of **server.ini**.
	port : `int`
		The port that the interface shall respond on; content of **server.ini**.
	logfile : `string`
		The location of the logfile; content of **server.ini**.
	path : `string`, optional
		Specifies a custom path to the configuration file. (the default is '.')
	filename : `string`, optional
		Specifies a custom name of the configuration file. (the default is
		'server.ini')

	Raises
	------
	ValueError
		One of the numeric config values could not be converted to type int.
	IllegalConfigException
		One of the numeric config values was non-positive.
	"""
	file = os.path.join(path, filename)
	import configparser
	config = configparser.ConfigParser()
	config.read(file)
	if 'config' not in config:
		print('No server configuration provided OR\nConfig file does not contain section "config".\n'
				'Aborting start-up ...')
		print('\n')
		print('In case you want to start the application with default values | use "--no-config"')
		print('In case you want to generate a sample config file             | use "--generate-config"')
		sys.exit(1)

	else:

		config_section = config['config']
		tmp = config_section.getint('max_workers')
		if (tmp is None):
			constants.MAX_NUMBER_OF_WORKERS = constants.DEFAULT_MAX_NUMBER_OF_WORKERS
		elif (tmp <= 0):
			raise IllegalConfigException('Value of max_workers: ' + str(tmp), 'One of the config\'s values '
										'is non-positive! Reverting to default values')
		else:
			constants.MAX_NUMBER_OF_WORKERS = tmp

		tmp = config_section.getint('revert_window_size')
		if (tmp is None):
			constants.ALGO_WINDOW_SIZE = constants.DEFAULT_ALGO_WINDOW_SIZE
		elif (tmp <= 0):
			raise IllegalConfigException('Value of algo_window_size: ' + str(tmp), 'One of the config\'s values '
										'is non-positive! Reverting to default values')
		else:
			constants.ALGO_WINDOW_SIZE = tmp

		tmp = config_section.getint('port')
		if tmp is None:
			constants.HTTP_PORT = constants.DEFAULT_HTTP_PORT
		elif tmp <= 0:
			raise IllegalConfigException(f'Value of port: {tmp}', 'The port number must be a positive number.')
		else:
			constants.HTTP_PORT = tmp

		tmp = config_section.get('path_to_petri_net')
		if (tmp is None or tmp == ''):
			constants.NET_PATH = constants.DEFAULT_NET_PATH
		else:
			constants.NET_PATH = tmp

		tmp = config_section.get('logfile')
		if (tmp is None or tmp == ''):
			constants.LOGFILE = constants.LOGFILE
		else:
			constants.LOGFILE = tmp


def petri_net_has_unique_labels(petri_net):
	""" **Checks whether the Petri net has unique labels**

	This checks whether there are two transitions in the given Petri net that have
	identical transition labels.

	Parameters
	----------
	petri_net : `pm4py.objects.petri.petrinet.PetriNet`
		The Petri net.

	Returns
	-------
	bool
		True if there are no two transitions with identical labels, False otherwise.
	"""

	trans = petri_net.transitions
	labels = [t.label for t in trans if t.label is not None]
	return len(set(labels)) == len(labels)


def load_petri_net():  # pragma: no cover
	""" **Loads the Petri net from file** """

	from pm4py.objects.petri.importer import pnml as pnml_importer
	try:
		constants.NET, constants.INI, constants.FIN = pnml_importer.import_net(constants.NET_PATH)
	except Exception as e:
		message = ' * Problems with loading the Petri net from file. The following exception was thrown:'
		logging.info(message)
		print(message)
		logging.info(e)
		sys.exit(1)

	if not petri_net_has_unique_labels(constants.NET):
		message = ' * Problems with loading the Petri net from file. Does not have unique labels.'
		logging.info(message)
		print(message)
		sys.exit(1)

	message = ' * {:30}: {:10}'.format('Petri net from file', 'loaded successfully')
	logging.info(message)
	print(message)
	message = '  ' + '\'' + constants.NET_PATH + '\''
	logging.info(message)
	print(message)


def init_alignment_storage():  # pragma: no cover
	from server.storage.alignments.main import AlignmentDataStorageInterface as ADSI
	from server.storage.physical.pickleharddrive import PickleHardDrive as PHD
	constants.ALIGNMENT_STORAGE = ADSI(handler=PHD('alignment_data', '.ad'))


def init_statistic_storage():  # pragma: no cover
	from server.storage.statistics.main import StatisticDataStorageInterface as SDSI
	from server.storage.physical.pickleharddrive import PickleHardDrive as PHD
	constants.STATISTIC_STORAGE = SDSI(handler=PHD('statistic_data', '.stat'))


def start_web_interfaces():  # pragma: no cover
	""" **Starts the HTTP server** """
	# and dash dashboard

	from server.interface import ConfoServer
	confoserver = ConfoServer(plot_schedule_on_shutdown=True)
	confoserver.run()


def print_copyright_notice():  # pragma: no cover
	name = \
	'   ______                   ____   ____ \n' \
	'  / ____/  ____    ____    / __/  / __ \\\n' \
	' / /      / __ \  / __ \  / /_   / / / /\n' \
	'/ /___   / /_/ / / / / / / __/  / /_/ /\n' \
	'\____/   \____/ /_/ /_/ /_/     \____/  '
	notice = \
	'ConfO - a web service for online conformance checking.\n' \
	'Copyright (C) 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner\n' \
	'This program comes with ABSOLUTELY NO WARRANTY.\n' \
	'This is free software, and you are welcome to redistribute it \n' \
	'under certain conditions. View GNU General Public License \n' \
	'(https://www.gnu.org/licenses/) for details.'

	print(name + '\n')
	print(notice + '\n')


def main():  # pragma: no cover
	""" **Starts the whole server application** """

	print_copyright_notice()

	parse_flags(sys.argv[1:])

	log_format = '%(asctime)s - %(levelname)s - %(message)s'
	log_datefmt = '[%Y-%m-%d %H:%M:%S]'
	if constants.LOGFILE:
		logging.basicConfig(filename=constants.LOGFILE, filemode="a", level=logging.INFO,
			format=log_format, datefmt=log_datefmt)
	else:
		logging.basicConfig(level=logging.INFO, format=log_format, datefmt=log_datefmt)

	message = 'Configured application with parameters'
	logging.info(message)
	print(message)
	message = '--------------------------------------'
	logging.info(message)
	print(message)
	constants.pretty_print()

	message = 'Loading the Petri net'
	logging.info(message)
	print(message)
	message = '--------------------------------------'
	logging.info(message)
	print(message)
	load_petri_net()

	message = 'Initialize alignment storage'
	logging.info(message)
	print(message)
	message = '--------------------------------------'
	logging.info(message)
	print(message)
	init_alignment_storage()

	message = 'Initialize statistic storage'
	logging.info(message)
	print(message)
	message = '--------------------------------------'
	logging.info(message)
	print(message)
	init_statistic_storage()

	message = 'Starting application interfaces'
	logging.info(message)
	print(message)
	message = '--------------------------------------'
	logging.info(message)
	print(message)
	start_web_interfaces()


if __name__ == '__main__':
	main()
