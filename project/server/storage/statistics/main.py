#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module defines and represents all the storage logic for statistics**
"""

import os

from server.storage.storage_interface import DataStorageInterface
from server.view_statistics.factory import N_M_SYNC, N_M_LOG, N_M_MODEL


class StatisticDataStorageInterface(DataStorageInterface):
	""" **A storage interface for statistical data**

	Inherits safety mechanisms from its superclass.

	Parameters
	----------
	handler : `server.storage.physical_interface.DataStorage`
		A data storage handler for the physical storage medium.
	max_concurrent_reads_allowed : `int`, optional
		The number of concurrent reads, that this interface shall allow.
	"""

	def __init__(self, handler, max_concurrent_reads_allowed=1):
		super().__init__(handler, max_concurrent_reads_allowed)

	def write_statistic_data(self, data):
		""" **Safely writes statistical data to the defined storage**

		Calls the superclasses write method.

		Parameters
		---------
		data : `object`
			The data object that is to be written. This object already
			contains the id.

		Raises
		------
		TypeError
			If the data parameter is not of type dictionary.
		"""

		if isinstance(data, dict):
			for identifier in data.keys():
				status, prev_stat = self.read_statistic_data(identifier)
				if status:
					prev_stat[identifier][N_M_SYNC] += data[identifier][N_M_SYNC]
					prev_stat[identifier][N_M_LOG] += data[identifier][N_M_LOG]
					prev_stat[identifier][N_M_MODEL] += data[identifier][N_M_MODEL]
					self.write_data(identifier, prev_stat)
				else:
					stat = dict()
					stat[identifier] = data[identifier]
					self.write_data(identifier, stat)
		else:
			raise TypeError('Parameter data should be of type ExtendedSearchTuple but is of type: ', str(type(data)))

	def read_statistic_data(self, identifier):
		""" **Safely reads statistical data from the defined storage**

		Calls the superclasses read method.

		Parameters
		----------
		identifier : `string`
			The id of the data object to be read.

		Returns
		-------
		bool
			True, if data was successfully read. False, if data does not exists.
		data : `object`
			The read statistics. If does not exist: None.
		"""

		status, data = self.read_data(identifier)
		return status, data

	def read_statistic_data_all(self):
		""" **Safely reads and combines all statistical data from the defined
		storage**

		Calls this classes read method for all files.

		Returns
		-------
		bool
			True, if data was successfully read. False, if data does not exists.
		data : `object`
			The read statistics. If does not exist: None.
		"""

		status = True
		data = dict()
		for filename in os.listdir(self._handler.PATH):
			root, ext = os.path.splitext(filename)
			if ext == self._handler.FILE_EXT:
				identifier = root
				exists, stat = self.read_statistic_data(identifier)
				if not exists:
					data = None
					status = False
					break
				else:
					data = self._merge_dicts(data, stat)
		if len(list(data.keys())) == 0:
			status = False
			data = None
		return status, data

	@staticmethod
	def _merge_dicts(x, y):
		return {**x, **y}
