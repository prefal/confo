#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module defines and represents all the storage logic for the alignment
algorithm.**
"""

from server.storage.storage_interface import DataStorageInterface
from server.objects.alignments import AlignmentData


class AlignmentDataStorageInterface(DataStorageInterface):
	""" **A storage interface for alignment data**

	Inherits safety mechanisms from its superclass.

	Parameters
	----------
	handler : `server.storage.physical_interface.DataStorage`
		A data storage handler for the physical storage medium.
	max_concurrent_reads_allowed : `int`, optional
		The number of concurrent reads, that this interface shall allow.
	"""

	def __init__(self, handler, max_concurrent_reads_allowed=1):
		super().__init__(handler, max_concurrent_reads_allowed=max_concurrent_reads_allowed)

	def write_alignment_data(self, data):
		""" **Safely writes alignment data to the defined storage**

		Calls the superclasses write method.

		Parameters
		---------
		data : `server.objects.alignments.AlignmentData`
			The AlignmentData object that is to be written. This object already
			contains the case id.

		Raises
		------
		TypeError
			If the data parameter is not of type AlignmentData.
		"""

		if isinstance(data, AlignmentData):
			case_id = data.get_case_id()
			self.write_data(case_id, data)

		else:
			raise TypeError('Parameter data should be of type AlignmentData but is of type: ', str(type(data)))

	def read_alignment_data(self, case_id):
		""" **Safely reads alignment data from the defined storage**

		Calls the superclasses read method.

		Parameters
		----------
		case_id : `string`
			The id of the case for which the data should be read.

		Returns
		-------
		bool
			True, if data was successfully read. False, if data does not exists.
		data : `server.objects.alignments.AlignmentData`
			The read AlignmentData object. If does not exist: None.
		"""

		status, data = self.read_data(case_id)
		return status, data
