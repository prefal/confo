#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module defines and represents a general, multiprocessing capable and
conflict-free storage logic.**

In this module you will find an interface is given that provides safe read- /
write-operations to any physical medium in the case of multiprocessing. The
safeness of the operations is provided by instances of the class
server.objects.locks.ReadWriteLock.
"""

from multiprocessing import Lock
from server.objects.locks import ReadWriteLock
from server.storage.physical_interface import DataStorage


class DataStorageInterface:
	""" **A safe wrapper around any physical storage of choice**

	Manages safe read/write operations for sensitive data. This class ensures
	conflict-free r/w operability of such with help of a custom Lock:
	server.objects.locks.ReadWriteLock. It acts as  an interface between the
	application and any custom physical medium:

	Preservers the internal calls to this classes public and safe read/write
	methods, yet allows to exchange the physical storage. Choose any
	implementation inheriting from server.storage.physical_interface.DataStorage
	as a handler for the physical layer.

	As this is an interface to any type of data storage, TypeError checks must
	be included in subclasses of this class.

	Parameters
	----------
	handler : `server.storage.physical_interface.DataStorage`
		A data storage handler for the physical storage medium.
	max_concurrent_reads_allowed : `int`, optional
		The number of concurrent reads, that this interface shall allow.
	"""

	def __init__(self, handler, max_concurrent_reads_allowed=1):
		if not isinstance(handler, DataStorage):
			raise TypeError('Handler must inherit from ', type(DataStorage))
		self._dict_lock = Lock()
		self.control_access = dict()
		self._concurrent_reads_allowed = max_concurrent_reads_allowed if max_concurrent_reads_allowed >= 1 else 1
		self._handler = handler
		self._handler.instantiate_storage()

	def write_data(self, identifier, data):
		""" **Safely writes data to the defined storage**

		Ensures safe write operability, i.e., during a write there are no read
		operations allowed for the data's id. Never shall multiple writes be
		allowed. Should this be the first write operation for the data's id
		then a ReadWriteLock for this id is created first.

		Acts as an interface to the actual data storage: Calls a function of the
		modifiable handler.

		Parameters
		---------
		identifier : `string`
			An unique identifier for the data object.
		data : `object`
			The data object that is to be written. This object already
			contains the case id.
		"""

		if not self._check_if_control_access_exists(identifier):
			self._add_new_control_access_instances(identifier)

		rw_lock = self.control_access[identifier]

		rw_lock.acquire_write()
		self._handler.set(identifier, data)
		rw_lock.release_write()

	def read_data(self, identifier):
		""" **Safely reads data from the defined storage**

		Ensures safe read operability, i.e., during a read there are no write
		operations allowed for data of this id. However, multiple reads are
		allowed.

		Parameters
		----------
		identifier : `string`
			An unique identifier for the data object.

		Returns
		-------
		bool
			True, if data was successfully read. False, if data does not exist.
		data : `object`
			The read data object.
		"""

		status = False
		data = None
		try:
			rw_lock = self.control_access[identifier]
			rw_lock.acquire_read()
			status, data = self._handler.get(identifier)
			rw_lock.release_read()
		except KeyError:
			pass
		return status, data

	def _check_if_control_access_exists(self, identifier):
		""" **Checks if there is a ReadWriteLock instance for the given id**

		For each id, there must be one instance of a ReadWriteLock for safe
		read- and write-operations.

		Parameters
		----------
		identifier : `string`
			The id of the data object for which the existence of control access
			mechanism shall be checked.

		Returns
		-------
		bool
			True, if instance exists. False, if does not exist.
		"""

		with self._dict_lock:
			if identifier in self.control_access.keys():
				return True
			else:
				return False

	def _add_new_control_access_instances(self, identifier):
		""" **Adds new control access instance for a given id**

		Parameters
		----------
		identifier : `string`
			The id of the case for which these instances shall be created.
		"""

		with self._dict_lock:
			self.control_access[identifier] = ReadWriteLock(self._concurrent_reads_allowed)

	def clean_up(self):
		""" **Cleans all data from the physical medium** """

		self._handler.clean_up()
