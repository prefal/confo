#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module defines the interaction with a custom physical storage.**

Interaction with a data storage is to be implemented in a subclass of
server.storage.physical_interface.DataStorage. There is no need to implement any
safety mechanisms, this is handled by server.storage.storage_interface.DataStorageInterface.
"""

from abc import ABC, abstractmethod


class DataStorage(ABC):
	""" **An abstract Superclass for all data storages**

	A class with abstract class methods that must be implemented in a subclass
	of this class. This class serves as an interface for the higher-level data
	storage logic. Subclasses shall implement the low-level functionality to
	interact with the physical data storage. Subclassing allows to specify any
	physical location or data storage mechanism.
	"""

	@classmethod
	@abstractmethod
	def instantiate_storage(cls):
		""" **Instantiates the physical storage upon server start-up** """

	@classmethod
	@abstractmethod
	def _exists(cls, case_id):
		""" **An abstract class method to check existence of data for case_id** """

	@classmethod
	@abstractmethod
	def get(cls, case_id):
		""" **An abstract class method to get data for case_id** """

	@classmethod
	@abstractmethod
	def set(cls, case_id, data):
		""" **An abstract class method to set data object** """

	@classmethod
	@abstractmethod
	def clean_up(cls):
		""" **A static method to clean physical storage** """
