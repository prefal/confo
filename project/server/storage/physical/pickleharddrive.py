#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **This module describes an explicit data storage handler.**
"""

import os
import pickle
import glob
import logging

from server.storage.physical_interface import DataStorage


class PickleHardDrive(DataStorage):

	"""
	This subclass of DataStorage implements the I/O to the harddrive while
	using the pickle library for serialization. By default files are written to
	the directory *'./DIR/'* and files follow the naming convention
	given by identifier and the file extension: ID.FILE_EXT.
	"""

	DIR = ''
	PATH = ''
	FILE_EXT = ''

	def __init__(self, dir, file_extension):
		self.DIR = dir
		self.FILE_EXT = file_extension
		self.PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.DIR)

	def instantiate_storage(self):
		""" **Instantiates the physical storage upon server start-up**

		Creates a directory according to the classes DIR variable. If directory
		already exists, its contents are deleted.

		Is called by DataStorageInterface.
		"""
		if not os.path.isdir(self.PATH):
			os.mkdir(self.PATH)
			message = ' * {:30}: {:10}'.format('Storage', 'Dir created at \'' + self.PATH + '\'')
			logging.info(message)
			print(message)
		else:
			message = ' * {:30}: {:10}'.format('Storage', 'Dir already exists at \'' + self.PATH + '\'')
			logging.info(message)
			print(message)
			self.clean_up()

	def _exists(self, identifier):
		""" **Checks the existence of the file ID.FILE_EXT**

		Checks for this file under the path specified by the classes PATH
		variable.

		Parameters
		----------
		identifier : `string`
			The id of the data file for which the existence shall be checked.

		Returns
		-------
		bool
			True, if data exists. False, if data does not exists.
		"""

		import os
		if os.path.exists(os.path.join(self.PATH, str(identifier) + self.FILE_EXT)):
			return True
		else:
			return False

	def get(self, identifier):
		""" **Retrieves the pickled data object from ID.FILE_EXT**

		Checks if file exists, subsequently reads and un-pickles the data.

		Parameters
		----------
		identifier : `string`
			The id of the data to be read.

		Returns
		-------
		status : `bool`
			True, if data was successfully read. False, if data does not exists.
		data :
			The read data object. If does not exist: None.
		"""

		status = self._exists(identifier)
		if status:
			filename = os.path.join(self.PATH, str(identifier) + self.FILE_EXT)
			with open(filename, 'rb') as file:
				data = pickle.load(file)
			return status, data
		else:
			return status, None

	def set(self, identifier, data):
		""" **Stores the pickled data object to file ID.FILE_EXT**

		Pickles the data and writes it to the file.

		Parameters
		----------
		identifier : `string`
			The id of the data to be stored.
		data
			The data object to be written.
		"""

		filename = os.path.join(self.PATH, str(identifier) + self.FILE_EXT)
		with open(filename, 'wb') as file:
			pickle.dump(data, file)

	def clean_up(self):
		""" **Cleans physical storage**

		Creates a directory according to the classes DIR variable. If directory
		already exists, its contents are kept.
		"""

		files = glob.glob(os.path.join(self.PATH, '*' + self.FILE_EXT))
		for f in files:
			os.remove(f)
