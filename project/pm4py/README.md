# Welcome to Process Mining for Python!

## Remark
All contents of this directory are, with except for this README file, an unchanged copy of the [PM4Py library on
GitHub](https://github.com/pm4py/pm4py-source) of which we respect the license and copyrights.
This copy is a fix version of the library (10. October 2018) provided as-is by one of the library's authors.
