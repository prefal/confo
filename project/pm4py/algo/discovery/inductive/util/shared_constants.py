"""
Shared constants useful for Inductive Miner application
"""
APPLY_REDUCTION_ON_SMALL_LOG = True
MAX_LOG_SIZE_FOR_REDUCTION = 30

LOOP_CONST_1 = 0.2
LOOP_CONST_2 = 0.02
LOOP_CONST_3 = -0.2
LOOP_CONST_4 = -0.7