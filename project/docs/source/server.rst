Server
======

 .. toctree::
   :maxdepth: 1
   :caption: Contents:


Main
----
 .. automodule:: server.main
   :members:
   :show-inheritance:
   :private-members:


Objects
-------

server.objects.alignments
~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.objects.alignments
   :members:
   :show-inheritance:
   :private-members:

server.objects.locks
~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.objects.locks
   :members:
   :show-inheritance:
   :private-members:


Interface
---------

server.interface.ConfoServer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. autoclass:: server.interface.ConfoServer
   :members:
   :show-inheritance:
   :private-members:

server.schedule_plotter
~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.schedule_plotter
   :members:
   :show-inheritance:
   :private-members:


Conformance Checking
--------------------

server.conformance.prefix_alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.conformance.prefix_alignment
   :members:
   :show-inheritance:
   :private-members:

server.conformance.auxiliary
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.conformance.auxiliary
   :members:
   :show-inheritance:
   :private-members:


Data Management
---------------

server.storage.storage_interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.storage.storage_interface
   :members:
   :show-inheritance:
   :private-members:

server.storage.physical_interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.storage.physical_interface
   :members:
   :show-inheritance:
   :private-members:

server.storage.alignments.main
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.storage.alignments.main
   :members:
   :show-inheritance:
   :private-members:

server.storage.statistics.main
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.storage.statistics.main
   :members:
   :show-inheritance:
   :private-members:

server.storage.physical.pickleharddrive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 .. automodule:: server.storage.physical.pickleharddrive
   :members:
   :show-inheritance:
   :private-members:


Visualization
-------------
 .. automodule:: server.view_statistics.versions.generate_statistic_png
   :members:
   :show-inheritance:
   :private-members:

 .. automodule:: server.view_statistics.versions.visualize
   :members:
   :show-inheritance:
   :private-members:


Constants
---------
 .. automodule:: server.constants
   :members:
   :show-inheritance:
   :private-members:


Exceptions
----------
 .. automodule:: server.exceptions
   :members:
   :show-inheritance:
   :private-members:
