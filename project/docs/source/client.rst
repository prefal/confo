Client
======

 .. toctree::
   :maxdepth: 2
   :caption: Contents:

Main
----
 .. automodule:: client.main
   :members:
   :show-inheritance:
   :private-members:

ConfOClient
-----------
 .. automodule:: client.confoclient
   :members:
   :show-inheritance:
   :private-members:

HTTP communication
---------------------------------
 .. automodule:: client.sync_sending
   :members:
   :show-inheritance:
   :private-members:

 .. automodule:: client.async_sending
   :members:
   :show-inheritance:
   :private-members:

Event Board
-----------
 .. automodule:: client.eventboard
   :members:
   :show-inheritance:
   :private-members:
