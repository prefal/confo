.. ConfO documentation master file, created by
   sphinx-quickstart on Wed Nov 14 14:46:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ConfO's documentation!
============================================

ConfO is a **real-time conformance checker** for business processes. It uses a
client-server architecture: the client receives event data from the business
and sends them to the server. The server checks whether the event data conforms
to a defined, fixed process model and returns feedback (e.g. OK, ALERT).
To achieve this, the ConfO server uses a variant of the alignments technique
called prefix-alignments.

`Visit on GitLab! <https://gitlab.com/prefal/confo>`_

.. toctree::
  :maxdepth: 2
  :caption: Contents

  getting_started
  server
  client


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`