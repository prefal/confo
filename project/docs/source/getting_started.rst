Getting Started
===============

Dependencies
------------
- `Pm4Py <https://pm4py.github.io/>`_  (contained in source code)
- Python 3.6
- graphviz
- for other see ``project/requirements.ini``



Quickstart
----------
Run

.. code-block:: BASH

   sudo apt-get install -y --no-install-recommends graphviz && \
   python3.6 install_requirements.py --server && \
   cd server/ && \
   python3.6 main.py --no-config

to install and start the server-side application at http://localhost:5000



Download the code
-----------------
No matter how you will execute the server, you'll need to download the code from the
repository at https://gitlab.com/lab-i9/prefixAlignments. The repository contains the
source code for client and server. If client and server are on separate machines,
please download the source to each machine.

Since the client is an example for an event generating system, we don't provide a Dockerfile.
The client is run directly from the command line, no matter whether you use the Docker
image or not.


Running the server manually
---------------------------
If you want to run the server directly (i.e. without using Docker), follow the steps provided
in this section.

Installing the dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~
After you downloaded the source code, you need to make sure you have all dependencies
installed. To do so, run

.. code-block:: BASH

   python3.6 install_requirements.py --client
   python3.6 install_requirements.py --server

To install further dependencies (such as for testing or generation of this documentation,
run the script with
``--help``).

Starting the server
~~~~~~~~~~~~~~~~~~~
1. Open a terminal window and navigate to ``project/server``.
2. Now, execute the following command to start the server.

   .. code-block:: BASH

      python3.6 main.py

3. Now the server is listening for incoming events.
4. Refer to `Starting the client`_ to learn how to start the client.


Running the application with Docker
-----------------------------------
The repository contains a ``Dockerfile`` which can be used to build a Docker image for
the ConfO server. Furthermore, there is a ``docker-compose.yml`` that uses nginx as the
front-end reverse proxy.

For this section, we assume you have already downloaded the ConfO source code. Further,
be sure to have ``docker`` and ``docker-compose`` installed on your system.

*Note:* depending on your setup, you may need to run the Docker commands as root,
i.e. prefix all docker commands with ``sudo``.

Starting the server
~~~~~~~~~~~~~~~~~~~
To get started, run the following command from the ``project/`` directory:

.. code-block:: BASH

   docker-compose up -d

This will build and start (in detached mode [``-d``])
a Docker container for the server-side aiohttp application (including the application that
sits in the ``project/server/`` directory) and a container running a nginx proxy.
By default, when running with ``docker-compose``, the web interface is available at http://localhost:80.

Stopping the server
~~~~~~~~~~~~~~~~~~~
When working with ``docker-compose``, be sure to run

.. code-block:: BASH

   docker-compose down

to stop and remove all containers created by ``docker-compose up``.

Accessing the log files
~~~~~~~~~~~~~~~~~~~~~~~
If you want to use the built-in logging tools Docker provides, be sure to remove the
``logfile`` option from the server configuration. Absent this option, the server will
log to stdout. The log can be accessed through:

.. code-block:: BASH

   docker-compose logs

It's also possible to connect to the output while it's running:

.. code-block:: BASH

   docker-compose logs -f

Starting the client
-------------------
No matter how the server is running (using Docker or standalone), the steps to run the
client are always the same:

1. Open a new terminal window and navigate to ``project/client``.
2. Execute the following command to start the client:

   .. code-block:: BASH

      python3.6 main.py

If everything was done correctly, the client immediately upon startup begins sending
events to the server. In the client window, each event is logged in a line beginning with
``Sending``. The server outputs a log line as soon as a request has been answered.
The server's response is then reported in the client window in a line starting with ``Response:``.
For help regarding the server or the client, or to see more options, call each with the
``--help`` option.

Please note that after the client has terminated, the server needs to be restarted to erase
the case history before running the client again. Otherwise, the server would simply append
incoming events to the traces from the previous client run and (in this case) only output ALERTs.
This is due to the fact that the client simply replays events from a given log, using the
case identifiers from the log.

Configuration
-------------
The server applications as well as the nginx proxy and the provided client variant allow some
configuration. When making use of ``docker-compose``, find the corresponding Dockerfile in directory
``project/``.

Nginx & Ports
~~~~~~~~~~~~~
The ngnix configuration lies in the file ``project/nginx/nginx.conf``.

There, configure on which port the nginx server
listens and to which port to proxy requests (this is the port that the ``docker-compose.yml`` maps to
the server container's port, which is set in ``Dockerfile``).

If the config file is renamed, make sure to update this in the ``docker-compose.yml``.

Server config
~~~~~~~~~~~~~
The server configuration lies in the file ``project/server/server.ini``.

.. code-block:: none

   [config]
   max_workers = 8
   path_to_petri_net = petri.pnml
   revert_window_size = 10
   port = 5000

   # Use logfile if you want to log into a file instead of stdout
   logfile = server.log


It allows to configure

- the maximum number of spawned asyncio workers,
- the port of the aiohttp server,
- the path to the Petri net used,
- the revert distance for the prefix-alignment algorithm and
- the filename to log to (will log to stdout if not set).

When using Docker, make sure to also update ``Dockerfile`` in the relevant parts, namely

- the port number after ``EXPOSE``, so it matches the port in the ``server.ini`` and

- copy the desired Petri net file (\*.pnml) that the server uses into the correct path in the
  Dockerfile (path given by ``petri_net_path`` in the ``server.ini``).

- remove the ``logfile`` option from ``server.ini`` so that ConfO logs to stdout.

Further, if the port of the application in ``server.ini`` (and in the server's Dockerfile) was changed, also update the
port in the ``docker-compose.yml`` file. These three port definitions (in ``server.ini``,
``Dockerfile`` and ``docker-compose-yml``) must be always be the same.


Client config
~~~~~~~~~~~~~
The client configuration lies in the file ``project/client/client.ini``.

.. code-block:: none

   [config]
   server_base = http://127.0.0.1
   port = 80
   path_to_xes_file = input_data/simple_event_log.xes
   async_delay_ms = 120


It allows to configure

- the server's hostname,
- the server's port,
- the path to the xes log used by the client and
- a delay for sending out requests in milliseconds.

When testing the client on the same system as the server, the ``server_base`` must be
either ``http://127.0.0.1`` or ``http://localhost``.

The port must be the one under which the server is reachable. The default server port is 5000
in standalone mode or 80 if Docker is used. The server port is defined by ``server.ini``
(or ``nginx.conf``, if Docker is used).


Additional Information
----------------------

HTTP communication modes
~~~~~~~~~~~~~~~~~~~~~~~~
The client can send either synchronously or asynchronously. Synchronous sending means that
the client blocks during the transmission of an event and only regains control as soon
as the server's response has been received. This limits the event rate as the round-trip-time
induces delays where the client is blocked.

Asynchronous communication means that the event sending limitation is lifted. The client
is free to send events out without blocking. Theoretically, hundreds or thousands of
requests can be made, up until operating system limitations kick in. In our case, we use
an artificial delay of 120ms in between sending of requests in order to throttle the sending
rate to a hypothetical 30.000 requests/hour.

For the synchronous mode, use the option ``--sync``. For the asynchronous mode, use
``--async``, which is also the default behavior.

Event Board
~~~~~~~~~~~
To have a more concise and informative overview of the events and their conformance
feedback, run the client with the ``--eventboard`` option. This is compatible with either
HTTP communication mode.

Deviations view
~~~~~~~~~~~~~~~
The server applications provides deviation statistics on a graphical model of the process.
To access the view, browse to ``http://{server_base}:{port}/`` and click the link
"Deviations view".
