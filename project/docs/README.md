# Docs

Holds the source code and config for the sphinx documentation generation tool.

Will build code documentation, published to [our gitlab page](https://prefal.gitlab.io/confo).

## Offline doc generation

First, install sphinx (we assume you are able to do this via the official sphinx website).

Then, to generate the docs by yourself, perform the following steps.

	1. Change directory to this folder:
	        cd project/docs/
	
	2. Generate corresponding html files:
	        make html
	
	3. View the docs by opening ./build/html/index.html in your browser
