# Nginx

Holds the configuration file for the nginx Docker container run with docker-compose.

View [our gitlab page](https://prefal.gitlab.io/confo) for details on how to run docker-compose.
