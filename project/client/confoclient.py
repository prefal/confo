#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

import configparser
import os
import sys
import client.pm4pyref  # noqa: F401
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.objects.log import transform

from client.sync_sending import send_events_sync
from client.async_sending import send_events_async


class ConfoClient():
	"""This program reads traces from a hard-coded XES file, concatenates
	them into a single event log and then sends them one by one to the server.

	The client can make either synchronous or asynchronous HTTP POST requests.

	Synchronous requests mean that the client waits for one request to return
	before placing the next call. The sending rate therefore depends greatly
	on the round-trip time, which includes the handling time at the server.

	Asynchronous requests are sent as soon as they are available and do not
	block further requests. This implementation uses an artificial waiting time
	between requests in order to throttle the sending rate.
	"""

	ASYNC_DELAY_MS_DEFAULT = 120

	def _load_config(self, ini_file):
		"""Loads config file and construct API endpoint

		Parameters
		----------
		ini_file : str
			Filename of the config file

		Returns
		-------
		str
			API endpoint

		Raises
		------
		Exception
			If the specified config file does not contain a [config] section
		"""

		config = configparser.ConfigParser()
		config.read(ini_file)

		if 'config' not in config:
			raise Exception('The configuration file does not contain section "config".')

		config_section = config['config']
		server_base = config_section.get('server_base', 'http://localhost')
		port = config_section.get('port', '5000')
		self._check_endpoint = f'{server_base}:{port}/check/event'
		self._path_to_xes_file = config_section.get('path_to_xes_file')
		if self._path_to_xes_file is None:
			raise Exception('The configuration file must contain "path_to_xes_file".')

		self._async_delay_ms = int(config_section.get('async_delay_ms', self.ASYNC_DELAY_MS_DEFAULT))
		if self._async_delay_ms < 0:
			self._async_delay_ms = self.ASYNC_DELAY_MS_DEFAULT

	def _import_event_log(self, xes_file):
		"""Imports a trace log, concatenates events from all traces and sorts event log by timestamp

		Parameters
		----------
		xes_file : str
			Filename of the XES file containing the trace log

		Returns
		-------
		EventLog
			Event log sorted by timestamps
		"""

		trace_log = xes_importer.import_log(xes_file)
		event_log = transform.transform_trace_log_to_event_log(trace_log)
		event_log.sort()
		return event_log

	def _create_request_message(self, case_id, activity_name):
		"""Builds a Request Message to be sent to the server

		Parameters
		----------
		case_id : str
			Identifier for the case the event belongs to
		activity_name : str
			Identifier for the activity the event describes

		Returns
		-------
		dict
			Request Message instance
		"""

		return {
			'request': {
				'event': {
					'case_id': case_id,
					'activity_name': activity_name
				}
			}
		}

	def run(self, async_enabled, before_send_handler, response_handler):  # pragma: no cover
		"""The external interface to run the client operations

		Parameters
		----------
		async : bool
			Determines whether to use synchronous (`False`) or asynchronous sending (`True`).
		before_send_handler : FunctionType
			Callback function that is called before an event is sent.
		response_handler : FunctionType
			Callback function to process the server's responses.

		Returns
		-------
		None
		"""

		try:
			self._load_config('client.ini')
		except Exception as exc:
			print(exc)
			print('Exiting..')
			sys.exit(1)

		self._response_handler = response_handler
		self._on_before_send = before_send_handler

		# Locate and load XES file
		if not os.path.exists(self._path_to_xes_file):
			print(f'XES file {self._path_to_xes_file} does not exist.')
			print('Exiting..')
			sys.exit(2)

		event_log = self._import_event_log(self._path_to_xes_file)

		# print(f'Delay: {self._async_delay_ms}')
		# sys.exit(0)

		if async_enabled:
			send_events_async(self, event_log)
		else:
			send_events_sync(self, event_log)
