#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

from aiohttp import ClientSession
import asyncio
from json import JSONDecodeError

HTTP_STATUS_OK = 200


async def _async_post_event(session, client, id, case_id, activity_name):
	"""Sends a given event as a JSON document to the server endpoint specified
	in the client via HTTP POST.

	Parameters
	----------
	session : aiohttp.ClientSession
		The ClientSession to be used for making the HTTP calls.
	client : ConfoClient
		Reference to the client.
	id : Any
		A unique, artificial identifier for the event.
	case_id : String
		An identifer for the case the event belongs to.
	activity_name : String
		An identifier for the activity that describes the event.

	Returns
	-------
	None
	"""

	json = client._create_request_message(case_id, activity_name)
	client._on_before_send(id, case_id, activity_name)
	async with session.post(client._check_endpoint, json=json) as response:
		if response.status == HTTP_STATUS_OK:
			try:
				return_data = await response.json()
				client._response_handler(id, case_id, activity_name, return_data)
			except JSONDecodeError as e:
				print('The server responded malformed JSON data.', e)
		else:
			raise Exception(f'HTTP error. Expected status code {HTTP_STATUS_OK}, received {response.status}.')


async def _send_events_inner(client, event_log):
	"""Sends events from a given event log as individual messages to the server
	in an asynchronous fashion. This means that the client sends out events
	without waiting for a response for the previous request. In order to throttle
	the sending, the client can wait for a configurable amount of time between
	putting new requests on the	network.

	Parameters
	----------
	client : ConfoClient
		Reference to the client.
	event_log : EventLog
		Event log containing the events to be sent to the server

	Returns
	-------
	None
	"""

	tasks = []
	async with ClientSession() as session:
		for index, event in enumerate(event_log):
			case_id = event['case:concept:name']
			activity_name = event['concept:name']

			coroutine = _async_post_event(session, client, index, case_id, activity_name)
			task = asyncio.ensure_future(coroutine)
			tasks.append(task)

			# Delay the emission of HTTP requests.
			# If we wait 120ms between requests, and assuming that the operations itself don't
			# take time, this will throttle our client to making 30.000 requests per hour.
			await asyncio.sleep(client._async_delay_ms / 1000.0)

		responses = asyncio.gather(*tasks)
		await responses


def send_events_async(client, event_log):
	"""Sends events as individual messages to the server in an asynchronous fashion.

	Parameters
	----------
	client : ConfoClient
		Reference to the client.
	event_log : EventLog
		Event log containing the events to be sent to the server

	Returns
	-------
	None
	"""

	loop = asyncio.get_event_loop()
	future = asyncio.ensure_future(_send_events_inner(client, event_log))
	loop.run_until_complete(future)
