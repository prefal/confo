# Client

The sample client application that sends out an event stream.

View [our gitlab page](https://prefal.gitlab.io/confo) for details on how to run this client application.
