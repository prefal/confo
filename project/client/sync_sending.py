#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

import requests
from json import JSONDecodeError

HTTP_STATUS_OK = 200


def send_events_sync(client, event_log):  # pragma: no cover
	"""Sends events as individual messages to the server in a synchronous fashion.
	There is no delay built in here, because the client always awaits a response to the previous
	message. This slows down the process anyway, and an artificial delay is therefore not needed.

	Parameters
	----------
	client : ConfoClient
		Reference to the client.
	event_log : EventLog
		Event log containing the events to be sent to the server.

	Returns
	-------
	None
	"""

	for index, event in enumerate(event_log):
		case_id = event['case:concept:name']
		activity_name = event['concept:name']

		client._on_before_send(index, case_id, activity_name)

		response = requests.post(client._check_endpoint, json=client._create_request_message(case_id, activity_name))

		if response.status_code == HTTP_STATUS_OK:
			try:
				return_data = response.json()
				client._response_handler(index, case_id, activity_name, return_data)
			except JSONDecodeError as e:
				print('The server responded malformed JSON data.', e)
		else:
			raise Exception(f'HTTP error. Expected status code {HTTP_STATUS_OK}, received {response.status_code}.')
