#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

from collections import defaultdict
import shutil
import sys

_event_data = dict()


def send_handler(id, case_id, activity_name):  # pragma: no cover
	"""Store the event that is about to be transmitted to the server so that
	the event board can be printed.

	Parameters
	----------
	id : Any
		A unique, artificial identifier for the event.
	case_id : String
		An identifer for the case the event belongs to.
	activity_name : String
		An identifier for the activity that describes the event.

	"""

	_event_data[id] = {'case_id': case_id, 'activity_name': activity_name, 'response': '..'}
	print_table(_event_data)


def response_handler(id, case_id, activity_name, message):  # pragma: no cover
	"""Store the response for the corresponding event so that the event board
	can display each event's status.

	Parameters
	----------
	id : Any
		A unique, artificial identifier for the event.
	case_id : String
		An identifer for the case the event belongs to.
	activity_name : String
		An identifier for the activity that describes the event.
	message : dict
		Response Message from the server
	"""

	if 'response' not in message:
		print('Missing "response" key')
	elif 'type' not in message['response']:
		print('Missing "type" key')
	elif message['response']['type'] not in ['OK', 'ALERT', 'REJECT']:
		print('Unknown value for key "type"')
	else:
		_event_data[id]['response'] = message['response']['type']
		print_table(_event_data)


def print_table(event_data):
	"""Display current trace deviation overview. This prints events as part of
	a trace. Each trace is a column, and an event is appended as soon as it is
	sent to the server. As long as the response for an event has not arrived,
	there is a placeholder displayed.

	This function gets into hot water as soon as there are too many cases, i.e.
	columns, to be displayed. We could either dynamically adapt the widths as
	the screen space fills up, or implement some way of scrolling using the
	arrow keys.

	This is a non-essential, experimental feature.

	"""

	ACTIVITY_NAME_MAX_LEN = 20
	SPACING_LEN = 1
	RESPONSE_LEN = 10

	# This has only been tested on Linux and may cause problems on other platforms.
	cols, rows = shutil.get_terminal_size()

	longest_activity_name_len = 0

	# From _event_data, build a new data structure that groups by case_id
	events_by_cases = defaultdict(list)
	for event_id, event in event_data.items():
		case_id = event['case_id']
		activity_name = event['activity_name']
		if len(activity_name) > longest_activity_name_len:
			longest_activity_name_len = len(activity_name)
		events_by_cases[case_id].append({'event_id': event_id, 'activity_name': activity_name, 'response': event['response']})

	activity_len = min(ACTIVITY_NAME_MAX_LEN, longest_activity_name_len)

	# Clear screen, cheap variant
	for i in range(rows):
		print()

	# Print header row (case_id's)
	for case_id in events_by_cases:
		print(str(case_id).ljust(activity_len + SPACING_LEN + RESPONSE_LEN), end='')
	print()

	# Since we're printing to the terminal, all of this has to be done on a per-line basis, not per case.
	line_idx = 0
	longest_trace = max([len(events_by_cases[case_id]) for case_id in events_by_cases])

	while line_idx < longest_trace:
		for case_id, event_list in events_by_cases.items():
			if line_idx >= len(event_list):
				# for this particular case, we are past the listed events, so we just fill up the "cell" with spaces
				print(''.ljust(activity_len + SPACING_LEN + RESPONSE_LEN), end='')
			else:
				# this prints the actual event along with the response
				event = event_list[line_idx]
				print(event['activity_name'][:activity_len].ljust(activity_len), end='')
				print(''.ljust(SPACING_LEN), end='')
				print(event['response'][:RESPONSE_LEN].ljust(RESPONSE_LEN), end='')

		line_idx += 1
		print()

	# Move output to top of screen
	for i in range(rows - longest_trace - 2):
		print()

	sys.stdout.flush()
