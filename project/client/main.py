#!/usr/bin/env python3.6

__copyright__ = """

	ConfO - a web service for online conformance checking.

	Copyright 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner

	This file is part of ConfO.

	ConfO is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ConfO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with ConfO.  If not, see <https://www.gnu.org/licenses/>.

"""
__license__ = "GPL v3.0"

""" **The main entry point of the client application.**

This module is the main entry point for the client application. It deals with the
command-line parameters and starts an instance of ConfoClient accordingly.
"""

import argparse
import sys
import os
import inspect

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import client.pm4pyref  # noqa: F401
from client.confoclient import ConfoClient
from client.eventboard import send_handler as eventboard_send, response_handler as eventboard_receive


def before_send_handler(id, case_id, activity_name):  # pragma: no cover
	"""Handler to be called before an event is sent to the conformance checker.

	Parameters
	----------
	id : Any
		A unique, artificial identifier for the event.
	case_id : String
		An identifer for the case the event belongs to.
	activity_name : String
		An identifier for the activity that describes the event.

	"""

	print(f'Sending event {id}, case {case_id}, activity {activity_name}')


def simple_response_handler(id, case_id, activity_name, message):
	"""Handles the Response Message received from the server

	Parameters
	----------
	id : Any
		A unique, artificial identifier for the event.
	case_id : String
		An identifer for the case the event belongs to.
	activity_name : String
		An identifier for the activity that describes the event.
	message : dict
		Response Message from the server
	"""

	if 'response' not in message:
		print('Missing "response" key')
	elif 'type' not in message['response']:
		print('Missing "type" key')
	elif message['response']['type'] not in ['OK', 'ALERT', 'REJECT']:
		print('Unknown value for key "type"')
	else:
		print(f"Response for event {id}, case {case_id}, activity {activity_name}: {message['response']['type']}")


def print_copyright_notice():  # pragma: no cover
	name = \
	'   ______                   ____   ____ \n' \
	'  / ____/  ____    ____    / __/  / __ \\\n' \
	' / /      / __ \  / __ \  / /_   / / / /\n' \
	'/ /___   / /_/ / / / / / / __/  / /_/ /\n' \
	'\____/   \____/ /_/ /_/ /_/     \____/  '
	notice = \
	'ConfO - a web service for online conformance checking.\n' \
	'Copyright (C) 2018-2019 Benedikt Holmes, Min Li, Valentin Steiner\n' \
	'This program comes with ABSOLUTELY NO WARRANTY.\n' \
	'This is free software, and you are welcome to redistribute it \n' \
	'under certain conditions. View GNU General Public License \n' \
	'(https://www.gnu.org/licenses/) for details.'

	print(name + '\n')
	print(notice + '\n')


if __name__ == "__main__":

	print_copyright_notice()
	
	parser = argparse.ArgumentParser(description='''This starts the ConfoClient. Immediately upon startup, it sends \
		process events to the server.''')
	parser.add_argument('--async', dest='async_enabled', action='store_true', help='Use asynchronous HTTP sending')
	parser.add_argument('--sync', dest='async_enabled', action='store_false', help='Use synchronous HTTP sending')
	parser.add_argument('--eventboard', dest='use_eventboard', action='store_true', help='Display events and responses \
		in a condensed, updated fashion (experimental feature)')
	parser.set_defaults(async_enabled=True, use_eventboard=False)
	args = parser.parse_args()

	client = ConfoClient()

	if args.use_eventboard:
		client.run(args.async_enabled, eventboard_send, eventboard_receive)
	else:
		client.run(args.async_enabled, before_send_handler, simple_response_handler)
