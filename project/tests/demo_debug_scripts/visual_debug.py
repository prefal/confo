#!/usr/bin/env python3.6
import os
import sys

file = os.path.abspath(__file__)
current_dir = os.path.dirname(os.path.dirname(file))
parent_dir = os.path.join(os.path.dirname(current_dir), 'server')
sys.path.insert(0, parent_dir)
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.objects.petri.importer import pnml as pnml_importer

from server.conformance import prefix_alignment as prefal
from server import constants
from server.objects.alignments import AlignmentData
from server.storage.alignments.main import AlignmentDataStorageInterface as ADSI
from server.storage.physical.pickleharddrive import PickleHardDrive as PHD

from tests.demo_debug_scripts.visualizer_alignment.prepare_net import prepare_add_data
from tests.demo_debug_scripts.visualizer_alignment.visualizer_alignment import generate_marking_from_alignment
from tests.demo_debug_scripts.visualizer_alignment.visualizer_alignment import get_path
from pm4py.visualization.petrinet import factory as vis_fac
from server.view_statistics import factory as generate_statistic_image


# from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import random


PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'server_tests', 'test_data')


def prior(filename_log, filename_net):
	constants.ALIGNMENT_STORAGE = ADSI(handler=PHD(PATH, 'storage'))
	log = xes_importer.import_log(os.path.join(PATH, filename_log))
	net, ini, fin = pnml_importer.import_net(os.path.join(PATH, filename_net))
	return log, net, ini, fin


def visual_debug():
	# LOG, NET, INITIAL_MARKING, FINAL_MARKING = prior('simple_event_log.xes', 'simple_petri_net.pnml')
	print('Begin')
	LOG, NET, INITIAL_MARKING, FINAL_MARKING = prior('complex_event_log.xes', 'complex_petri_net.pnml')
	print('\nPre-loading done, starting prefix-alignment computation ...')
	print('_______________________________________________________________')

	trace = LOG[2]
	statistics = dict()
	# transition
	statistics['t'] = dict()
	for t in NET.transitions:
		statistics['t'][t.label] = dict()
		statistics['t'][t.label][generate_statistic_image.N_M_SYNC] = random.randint(0, 1000)
		statistics['t'][t.label][generate_statistic_image.N_M_LOG] = random.randint(0, 100)
		statistics['t'][t.label][generate_statistic_image.N_M_MODEL] = random.randint(0, 100)
	# position
	statistics['p'] = dict()
	for p in NET.places:
		statistics['p'][p.name] = dict()
		statistics['p'][p.name][generate_statistic_image.N_M_SYNC] = random.randint(0, 1000)
		statistics['p'][p.name][generate_statistic_image.N_M_LOG] = random.randint(0, 100)
		statistics['p'][p.name][generate_statistic_image.N_M_MODEL] = random.randint(0, 100)
	# arcs
	statistics['a'] = dict()
	for a in NET.arcs:
		name = '{0} -> {1}'.format(a.source.name, a.target.name)
		statistics['a'][name] = dict()
		statistics['a'][name][generate_statistic_image.ACTIVATED] = random.randint(0, 1000)
	plt.figure(figsize=(20, 14), dpi=300)
	plt.axis('off')
	plt.ion()

	case_id = trace.attributes['concept:name']
	lst_trace_till_now = list()
	lst_transition = list()
	lst_transition_last = list()
	for event in trace:
		lst_trace_till_now.append(event['concept:name'])
		exists, alignment_data = constants.ALIGNMENT_STORAGE.read_alignment_data(case_id)
		if not exists:
			alignment_data = AlignmentData(case_id)

		# compute new alignment
		alignment_data, sync, move_on_model, statistics['t'], _ = prefal.apply_incremental(NET, INITIAL_MARKING,
																					FINAL_MARKING,\
																		alignment_data, event, revert_distance=10)
		# Visualization
		lst_alignment = get_path(alignment_data.get_state())
		dic_places, dic_transition_name, _, _, _ = prepare_add_data(NET)
		lst_transition_last = lst_transition
		lst_marking, lst_transition = generate_marking_from_alignment(NET, INITIAL_MARKING, FINAL_MARKING,
										lst_alignment,
										dic_places, dic_transition_name,
										whole_procedure=True)
		curr_marking = lst_marking[len(lst_marking)-1]
		# gviz = vis_fac.apply(NET, curr_marking , FINAL_MARKING)
		gviz = generate_statistic_image.apply(
				NET, image_format='png',
				curr_event_name=event['concept:name'],
				initial_marking=curr_marking, final_marking=FINAL_MARKING,
				is_show_deviation=True, dic_deviation=statistics,
				variant='statistic_with_alignment',
				debug=False
		)
		vis_fac.save(gviz, './result.png')
		im = mpimg.imread('./result.png')
		plt.title(
				'trace: '+str(lst_trace_till_now) + '\n'
				+ 'transitions last: ' + str(lst_transition_last) + '\n'
				+ 'transitions now: ' + str(lst_transition)
				, fontsize = 2
				, horizontalalignment='left'
				, loc='left'
		)
		plt.axis('off')
		plt.imshow(im, interpolation='blackman')
		plt.pause(0.5)
		print('trace till now: {0:s}'.format(str(lst_trace_till_now)))
		print('SearchTuple print: {0:s}'.format(str(alignment_data.get_state())))
		print(lst_alignment)
		print(lst_transition)
		print('##############################')

		# write new data
		constants.ALIGNMENT_STORAGE.write_alignment_data(alignment_data)

	plt.ioff()
	plt.show()
	post()


def post():
	constants.ALIGNMENT_STORAGE.clean_up()

if __name__ == '__main__':
	print('begin')
	visual_debug()

