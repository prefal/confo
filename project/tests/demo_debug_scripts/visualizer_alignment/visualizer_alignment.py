#!/usr/bin/env python3.6
from pm4py.objects.petri.semantics import is_enabled
from pm4py.objects.petri.semantics import execute

SKIP = '>>'


def generate_marking_from_alignment(net, initial_marking, final_marking,
									lst_alignment,
									dic_places, dic_transition_name,
									whole_procedure=False):
	"""
		Generate lst of marking the petri net with Marking and

		Parameters
		----------
		net : :class:`pm4py.objects.petri.petrinet.PetriNet`
			Imported petri-net as process model
		initial_marking : :class:`pm4py.objects.petri.petrinet.Marking`
			The initial marking of the petri net.
		final_marking : :class:`pm4py.objects.petri.petrinet.Marking`
			The final marking of the petri net.
		lst_alignment : :class:`[{'log': (str, str), 'model': (str, str)}]`
			[{'log': ( t+event order in tracenet, name of event in log),
			'model': (name of transition in petri net, label of transition in petri net)}]
		dic_places : :class:`dictionary`
			the dictionary to look up the place instance according to place name in net
		dic_transition_name : :class:`dictionary`
			the dictionary to look up the transition instance according to transition name in net
		whole_procedure : :class:`boolean`
			whether to reply the whole procedure of alignment.
			If this variable is False, the outputted list will only contain the final marking.
			If this variable is True, the outputted list will contain marking of each alignment steps

		Returns
		-------
		lst_marking : :class:`[pm4py.objects.petri.petrinet.Marking]`
			The list of marking
			If this variable is False, the outputted list will only contain the final marking.
			If this variable is True, the outputted list will contain marking of each alignment steps
	"""
	marking = initial_marking
	lst_marking = list()
	lst_transition = list()
	for dic_alignment in lst_alignment:
		if dic_alignment['model'] is None:
			raise Exception('model transition in Sync net is None.')
		elif dic_alignment['model'][1] == SKIP:
			continue
		else:
			t_name = dic_alignment['model'][0]
			t_instance = dic_transition_name[t_name]
		if is_enabled(t_instance, net, marking):
			marking = execute(t_instance, net, marking)
		else:
			raise Exception('Fire without enough Token')
		if whole_procedure:
			lst_marking.append(marking)
			lst_transition.append(t_instance)
	if not whole_procedure:
		lst_marking.append(marking)
		lst_transition.append(t_instance)
	return lst_marking, lst_transition

def get_path(state):
	"""
	Return
	______
	lst_alignment : :class:`[{'log': (str, str), 'model': (str, str)}]`
		[{'log': ( t+event order in tracenet, name of event in log),
		'model': (name of transition in petri net, label of transition in petri net)}]
	"""
	lst_alignment = []
	if state.p is not None:
		lst_alignment += get_path(state.p)
	if state.t is not None:
		dic_alignment = dict()
		dic_alignment['log'] = (state.t.name[0], state.t.label[0])
		dic_alignment['model'] = (state.t.name[1], state.t.label[1])
		lst_alignment.append(dic_alignment)
	return lst_alignment