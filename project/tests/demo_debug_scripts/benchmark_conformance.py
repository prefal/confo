#!/usr/bin/env python3.6
import os
import sys
import time

file = os.path.abspath(__file__)
current_dir = os.path.dirname(os.path.dirname(file))
parent_dir = os.path.join(os.path.dirname(current_dir), 'server')
sys.path.insert(0, parent_dir)
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.objects.petri.importer import pnml as pnml_importer
from pm4py.visualization.petrinet import factory as petrinet_viz

from server.conformance import prefix_alignment as prefal
from server import constants
from server.objects.alignments import AlignmentData
from server.storage.alignments.main import AlignmentDataStorageInterface as ADSI
from server.storage.physical.pickleharddrive import PickleHardDrive as PHD

PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'server_tests', 'test_data')


def construct_path(tpl):
	ret = []
	if tpl.p is not None:
		ret.extend(construct_path(tpl.p))
	if tpl.t is not None:
		ret.append(tpl.t)
	return ret


def prior(filename_log, filename_net):
	constants.ALIGNMENT_STORAGE = ADSI(handler=PHD('benchmark', '.ad'))
	log = xes_importer.import_log(os.path.join(PATH, filename_log))
	net, ini, fin = pnml_importer.import_net(os.path.join(PATH, filename_net))
	return log, net, ini, fin


def benchmark():
	LOG_NAME = 'complex_event_log.xes'
	NET_NAME = 'complex_petri_net.pnml'
	# LOG_NAME = 'simple_event_log.xes'
	# NET_NAME = 'simple_petri_net.pnml'
	LOG, NET, INITIAL_MARKING, FINAL_MARKING = prior(LOG_NAME, NET_NAME)
	print('Pre-loading done')

	traces = 0
	events = 0
	for trace in LOG:
		traces += 1
		events += len(trace)

	print('\n###############################################################')
	print('Testing on LOG ' + LOG_NAME)
	print('    and on NET ' + NET_NAME)
	print('Log stats:')
	print('  #Traces:   ' + str(traces))
	print('  #Events:   ' + str(events))
	print('###############################################################\n')


	gviz = petrinet_viz.apply(NET, INITIAL_MARKING, FINAL_MARKING)
	petrinet_viz.view(gviz)

	REQ_IN_SIXTY_MINUTES = 30000
	TTB = (events / REQ_IN_SIXTY_MINUTES) * 3600

	print('\n###############################################################')
	print('Benchmark test will output a score based on the throughput requirement')
	print('    {:5.0f}/h '.format(REQ_IN_SIXTY_MINUTES) + ' | Time-to-beat:{:8.2f} [seconds]'.format(TTB))
	print('Score is rate of actual runtime vs time-to-beat in percent, hence:')
	print('    S < 100: bad')
	print('    S \u2248 100: expected minimum')
	print('    S > 100: good')
	print('###############################################################\n')

	input('Press ENTER to start benchmark test ...')

	time_start_log = time.time()
	trace_count = 0
	for trace in LOG:
		case_id = trace.attributes['concept:name']

		print('Trace {:5d}'.format(trace_count) + ' with id: ' + case_id + '  #Events:' + str(len(trace)))
		print('---------------------------------------------------------------')
		time_start_trace = time.time()
		average_event_time = 0
		real_total_trace_time = 0
		event_count = 0
		for event in trace:

			exists, alignment_data = constants.ALIGNMENT_STORAGE.read_alignment_data(case_id)
			if not exists:
				alignment_data = AlignmentData(case_id)

			time_start_event = time.time()
			# compute new alignment
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(NET, INITIAL_MARKING, FINAL_MARKING,
																			alignment_data, event, revert_distance=10)

			# taking time and output
			time_end_event = time.time()
			event_elapsed_time = time_end_event - time_start_event
			time_str = '{:10f} [seconds]'.format(event_elapsed_time)
			state = alignment_data.get_state()
			average_event_time = average_event_time * event_count + event_elapsed_time
			event_count += 1
			average_event_time = average_event_time / event_count
			real_total_trace_time += event_elapsed_time
			print('{:3d} '.format(event_count) + '{:20.15s}'.format(event['concept:name']) + ' |  Elapsed time: ' +
					time_str + '  |  Current Alignment: ' + str(construct_path(state)))

			# write new data
			constants.ALIGNMENT_STORAGE.write_alignment_data(alignment_data)

		trace_count += 1
		time_end_trace = time.time()
		trace_elapsed_time = time_end_trace-time_start_trace
		print('_______________________________________________________________')
		print('Elapsed time (incl. I/O | per trace): {:10f} [seconds]'.format(trace_elapsed_time))
		print('Elapsed time (excl. I/O | per trace): {:10f} [seconds]'.format(real_total_trace_time))
		print('Average time (excl. I/O | per event): {:10f} [seconds]'.format(average_event_time))
		print()
		print()
	time_end_log = time.time()
	print('###############################################################')
	log_elapsed_time = time_end_log-time_start_log
	print('Total elapsed time: {:10f} [seconds]'.format(log_elapsed_time))
	bm = TTB / log_elapsed_time * 100
	print('\nBenchmark Score:  S = {:<8.0f}'.format(bm))
	post()


def post():
	constants.ALIGNMENT_STORAGE.clean_up()


if __name__ == '__main__':
	benchmark()
