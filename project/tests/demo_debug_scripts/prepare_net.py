#!/usr/bin/env python3.6

import server.pm4pyref  # noqa: F401
# unused from pm4py.objects.petri.petrinet import PetriNet
# unused from pm4py.objects.log.log import Trace
# unused from pm4py.objects.petri.petrinet import Marking


def prepare_add_data(net, debug=False):
	"""prepare the data according to the usage of alignment

	Parameters
	----------
	net : :class:'pm4py.objects.petri.petrinet.PetriNet'
		Imported petri-net as process model

	Returns
	-------
	dic_places : :class:'dictionary'
		the dictionary to look up the place instance according to place name of net
	dic_transition : :class:'dictionary'
		the dictionary to look up the transition instance according to
	transition name
	dic_place_name_transition_in : :class:'dictionary'
		the dictionary to look up the names of inlet places of a
	transition
	dic_place_name_transition_out : :class:'dictionary'
		the dictionary to look up the names of outlet places of a transition
	"""
	if debug:
		print('Beginning of method \'prepare_add_data\'')

	set_places = net.places
	set_transition = net.transitions
	dic_places = dict()
	dic_transition = dict()
	dic_place_name_transition_in = dict()
	dic_place_name_transition_out = dict()
	for place in set_places:
		dic_places.update({place.name: place})
	for transition in set_transition:
		dic_transition.update({transition.label: transition})
		set_in_arcs = transition.in_arcs
		set_out_arcs = transition.out_arcs
		place_name_transition_in = dict()
		place_name_transition_out = dict()
		for arcs in set_in_arcs:
			place_name_transition_in.update({arcs.source.name: arcs.source})
		dic_place_name_transition_in.update({transition.label: place_name_transition_in})
		for arcs in set_out_arcs:
			place_name_transition_out.update({arcs.target.name: arcs.target})
		dic_place_name_transition_out.update({transition.label: place_name_transition_out})

	if debug:
		print('End of method \'prepare_add_data\'')
	return dic_places, dic_transition, dic_place_name_transition_in, dic_place_name_transition_out
