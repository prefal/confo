#!/usr/bin/env python3.6
import unittest
import sys
import os
import inspect

if __name__ == '__main__':
	current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
	parent_dir = os.path.join(os.path.dirname(current_dir), 'client')
	sys.path.insert(0, parent_dir)
	parent_dir = os.path.dirname(current_dir)
	sys.path.insert(0, parent_dir)

	from tests.client_tests.client_confoclient_unit_test import ClientTest
	from tests.client_tests.client_main_unit_test import ClientMainTest
	from tests.client_tests.client_confoclient_interface_test import ClientInterfaceTest
	from tests.client_tests.client_eventboard_unit_test import EventBoardTest

	test1_object = ClientTest()
	test2_object = ClientMainTest()
	test3_object = ClientInterfaceTest()
	test4_object = EventBoardTest()

	unittest.main()