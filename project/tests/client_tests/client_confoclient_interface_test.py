#!/usr/bin/env python3
import unittest
import os
import mock
from tempfile import NamedTemporaryFile

# Import python code under test
from client import confoclient
from client import sync_sending

XES_TRACELOG_01 = '''<?xml version='1.0' encoding='UTF-8'?>
<log>
	<trace>
		<string key="concept:name" value="2"/>
		<event>
			<string key="concept:name" value="quotation"/>
			<date key="time:timestamp" value="2018-01-10T19:28:00.000+02:00"/>
		</event>
	</trace>
	<trace>
		<string key="concept:name" value="1"/>
		<event>
			<string key="concept:name" value="register"/>
			<date key="time:timestamp" value="2018-01-01T19:28:00.000+02:00"/>
		</event>
		<event>
			<string key="concept:name" value="shipped"/>
			<date key="time:timestamp" value="2018-01-20T19:28:00.000+02:00"/>
		</event>
	</trace>
</log>
'''

# Let Test Classes end with 'Test'
class ClientInterfaceTest(unittest.TestCase):

	def get_initialized_confo_client_and_event_log(self):
		"""Helper method to create initialized confo client"""
		confo = confoclient.ConfoClient()
		confo._check_endpoint = "http://localhost:5000/check/event"
		confo._on_before_send = lambda a,b,c: None
		confo._response_handler = lambda a,b,c,d: None

		xes_file = NamedTemporaryFile(mode='w', delete=False)
		xes_file.write(XES_TRACELOG_01)
		xes_file.close()

		event_log = confo._import_event_log(xes_file.name)
		os.unlink(xes_file.name)
		return confo, event_log

	def mock_requests_post_successful(*args, **kwargs):
		return MockResponse({'response': {'type': 'OK'}}, 200)

	@mock.patch('sync_sending.requests.post', side_effect=mock_requests_post_successful)
	def test_send_events_1(self, mock_post):
		"""Test sending events where server responds with 200"""
		confo, event_log = self.get_initialized_confo_client_and_event_log()
		sync_sending.send_events_sync(confo, event_log)

	def mock_requests_post_error(*args, **kwargs):
		return MockResponse({}, 500)

	@mock.patch('sync_sending.requests.post', side_effect=mock_requests_post_error)
	def test_send_events_2(self, mock_post):
		"""Test sending events where server responds with 500"""
		confo, event_log = self.get_initialized_confo_client_and_event_log()
		self.assertRaises(Exception, sync_sending.send_events_sync, confo, event_log)

class MockResponse:
	def __init__(self, json_data, status_code):
		self.json_data = json_data
		self.status_code = status_code

	def json(self):
		return self.json_data


if __name__ == '__main__':
	unittest.main()