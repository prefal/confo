#!/usr/bin/env python3.6
import unittest

from client import main

MESSAGE_MISSING_RESPONSE 	= {'RESULT': {'type': 'OK'}}
MESSAGE_MISSING_TYPE 		= {'response': {'STATUS': 'OK'}}
MESSAGE_UNKNOWN_TYPE 		= {'response': {'type': 'ok'}}
MESSAGE_OK 					= {'response': {'type': 'OK'}}
MESSAGE_ALERT 				= {'response': {'type': 'ALERT'}}
MESSAGE_REJECT 				= {'response': {'type': 'REJECT'}}


# Let Test Classes end with 'Test'
class ClientMainTest(unittest.TestCase):

	def _get_print_message(self, message):
		import io
		import sys
		captured_output = io.StringIO()  # Create StringIO object
		sys.stdout = captured_output  # and redirect stdout.
		main.simple_response_handler(None, None, None, message)  # Call unchanged function.
		sys.stdout = sys.__stdout__  # Reset redirect.
		return captured_output.getvalue()


	def test_sample_response_handler_1(self):
		self.assertEqual('Missing "response" key\n', self._get_print_message(MESSAGE_MISSING_RESPONSE))

	def test_sample_response_handler_2(self):
		self.assertEqual('Missing "type" key\n', self._get_print_message(MESSAGE_MISSING_TYPE))

	def test_sample_response_handler_3(self):
		self.assertEqual('Unknown value for key "type"\n', self._get_print_message(MESSAGE_UNKNOWN_TYPE))

	def test_sample_response_handler_4(self):
		self.assertIn('OK\n', self._get_print_message(MESSAGE_OK))

	def test_sample_response_handler_5(self):
		self.assertIn('ALERT\n', self._get_print_message(MESSAGE_ALERT))

	def test_sample_response_handler_6(self):
		self.assertIn('REJECT\n', self._get_print_message(MESSAGE_REJECT))


if __name__ == '__main__':
	unittest.main()
