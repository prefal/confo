#!/usr/bin/env python3.6
import unittest
import os
from tempfile import NamedTemporaryFile
from json import JSONDecodeError

from client import confoclient

XES_TRACELOG_01 = '''<?xml version='1.0' encoding='UTF-8'?>
<log>
	<trace>
		<string key="concept:name" value="2"/>
		<event>
			<string key="concept:name" value="quotation"/>
			<date key="time:timestamp" value="2018-01-10T19:28:00.000+02:00"/>
		</event>
	</trace>
	<trace>
		<string key="concept:name" value="1"/>
		<event>
			<string key="concept:name" value="register"/>
			<date key="time:timestamp" value="2018-01-01T19:28:00.000+02:00"/>
		</event>
		<event>
			<string key="concept:name" value="shipped"/>
			<date key="time:timestamp" value="2018-01-20T19:28:00.000+02:00"/>
		</event>
	</trace>
</log>
'''


class ClientTest(unittest.TestCase):
	def test_load_config(self):
		"""Test normal config file"""
		confo = confoclient.ConfoClient()
		settings_file = NamedTemporaryFile(mode='w', delete=False)
		settings_file.write('[config]\nserver_base = http://www.example.com\nport = 1234\npath_to_xes_file = test.xes\n')
		settings_file.close()
		confo._load_config(settings_file.name)
		os.unlink(settings_file.name)
		self.assertEqual(confo._check_endpoint, 'http://www.example.com:1234/check/event')

	def test_load_config_no_config_section(self):
		"""Test for missing config section with the name 'config'"""
		confo = confoclient.ConfoClient()
		settings_file = NamedTemporaryFile(mode='w', delete=False)
		settings_file.write('[wrongsection]\nserver_base = http://www.example.com\nport = 1234\npath_to_xes_file = test.xes\n')
		settings_file.close()
		self.assertRaises(Exception, confo._load_config, settings_file.name)
		os.unlink(settings_file.name)

	def test_load_config_negative_delay(self):
		"""Test for handling of negative values for 'async_delay_ms'"""
		confo = confoclient.ConfoClient()
		settings_file = NamedTemporaryFile(mode='w', delete=False)
		settings_file.write('[config]\nserver_base = http://www.example.com\nport = 1234\npath_to_xes_file = test.xes\nasync_delay_ms = -15')
		settings_file.close()
		confo._load_config(settings_file.name)
		os.unlink(settings_file.name)
		self.assertTrue(confo._async_delay_ms >= 0)

	def test_load_config_missing_xes_path(self):
		""" Test for missing parameter 'path_to_xes_file'"""
		confo = confoclient.ConfoClient()
		settings_file = NamedTemporaryFile(mode='w', delete=False)
		settings_file.write('[config]\nserver_base = http://www.example.com\nport = 1234\n')
		settings_file.close()
		self.assertRaises(Exception, confo._load_config, settings_file.name)
		os.unlink(settings_file.name)

	def test_import_event_log(self):
		"""Test loading of trace log and preparation into sorted event log"""
		confo = confoclient.ConfoClient()

		xes_file = NamedTemporaryFile(mode='w', delete=False)
		xes_file.write(XES_TRACELOG_01)
		xes_file.close()

		# Do not refactor this, as it's important that '_import_event_log' is called
		# If it was refactored, it may happen that someone no longer makes the call to this method.
		event_log = confo._import_event_log(xes_file.name)
		os.unlink(xes_file.name)

		self.assertEqual(len(event_log), 3)

		self.assertEqual(event_log[0]['concept:name'], "register")
		self.assertEqual(event_log[0]['case:concept:name'], "1")

		self.assertEqual(event_log[1]['concept:name'], "quotation")
		self.assertEqual(event_log[1]['case:concept:name'], "2")

		self.assertEqual(event_log[2]['concept:name'], "shipped")
		self.assertEqual(event_log[2]['case:concept:name'], "1")

	def test_create_request_message(self):
		"""Test for message creation"""
		confo = confoclient.ConfoClient()
		case_id = 'a1234'
		activity_name = 'refresh'
		msg = confo._create_request_message(case_id, activity_name)
		self.assertEqual(msg, {'request': {'event': {'case_id': case_id, 'activity_name': activity_name}}})



if __name__ == '__main__':
	unittest.main()