#!/usr/bin/env python3.6
import unittest
import sys
from io import StringIO
from unittest.mock import patch

from client.eventboard import print_table

EVENT_DATA = {
	0: {'case_id': 'foo', 'activity_name': 'bar', 'response': 'OK'},
	1: {'case_id': 'foo', 'activity_name': 'baz', 'response': 'OK'},
	2: {'case_id': 'XYZ', 'activity_name': 'bar', 'response': 'OK'},
}

class EventBoardTest(unittest.TestCase):

	def test_eventboard(self):
		with patch('sys.stdout', new=StringIO()) as fakeOutput:
			print_table(EVENT_DATA)
			self.assertTrue('bar' in fakeOutput.getvalue().strip())
			self.assertTrue('baz' in fakeOutput.getvalue().strip())
			self.assertTrue('foo' in fakeOutput.getvalue().strip())


if __name__ == '__main__':
	unittest.main()
