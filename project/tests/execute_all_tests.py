#!/usr/bin/env python3.6
import os
import sys
import inspect

'''Use this to run all tests in your Editor, e.g., PyCharm.
   Unit test seperation is for CI pipeline.'''
if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    parent_dir = os.path.dirname(current_dir)
    print('################################################################################')
    print()
    os.system('python3.6 ' + os.path.join(current_dir, 'execute_client_unit_tests.py -v'))
    print()
    print('################################################################################')
    print()
    os.system('python3.6 ' + os.path.join(current_dir, 'execute_server_unit_tests.py -v'))
    print()
    print('################################################################################')
    print()
    os.system('python3.6 ' + os.path.join(current_dir, 'execute_client_interface_tests.py -v'))
    print()
    print('################################################################################')
    print()
    os.system('python3.6 ' + os.path.join(current_dir, 'execute_server_interface_tests.py -v'))
    print()
    print('################################################################################')