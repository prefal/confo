#!/usr/bin/env python3.6
import unittest
import sys
import os
import inspect

if __name__ == '__main__':
	current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
	parent_dir = os.path.join(os.path.dirname(current_dir), 'server')
	sys.path.insert(0, parent_dir)
	parent_dir = os.path.dirname(current_dir)
	sys.path.insert(0, parent_dir)

	from tests.server_tests.server_constants_unit_test import ConstantsTest
	from tests.server_tests.server_main_unit_test import ServerMainTest
	from tests.server_tests.server_storage_unit_test import PickleHarddriveTest, DataStorageInterfaceTest, \
		AlignmentDataStorageInterfaceTest, StatisticDataStorageInterfaceTest
	from tests.server_tests.server_locks_unit_test import ReadWriteLockTest
	from tests.server_tests.server_view_statistic_unit_test import ViewStatisticTest
	from tests.server_tests.server_alignmentdata_unit_test import AlignmentDataTest, ExtendedSearchTupleTest
	from tests.server_tests.server_conformance_unit_test import PrefalAuxiliaryMethodsTest, PrefalSearchEntirelyTest,\
		PrefalSearchIncrementallyTest, PrefalApplySimpleTest, PrefalApplyComplexTest, PrefalApplyMixtureTest
	from tests.server_tests.server_queuing_test import ServerQueuingTest
	from tests.server_tests.server_petri_utils_unit_test import ServerPetriUtilTest

	# this is actually not necessary:
	test_object_1 = ConstantsTest()
	test_object_2 = ServerMainTest()
	test_object_3 = PickleHarddriveTest()
	test_object_4 = DataStorageInterfaceTest()
	test_object_5 = AlignmentDataStorageInterfaceTest()
	test_object_6 = StatisticDataStorageInterfaceTest()
	test_object_7 = ReadWriteLockTest()
	test_object_9 = ViewStatisticTest()
	test_object_10 = AlignmentDataTest()
	test_object_11 = ExtendedSearchTupleTest()
	test_object_12 = PrefalAuxiliaryMethodsTest()
	test_object_13 = PrefalSearchEntirelyTest()
	test_object_14 = PrefalSearchIncrementallyTest()
	test_object_15 = PrefalApplySimpleTest()
	test_object_16 = PrefalApplyComplexTest()
	test_object_17 = PrefalApplyMixtureTest()
	test_object_18 = ServerQueuingTest()
	test_object_19 = ServerPetriUtilTest()

	unittest.main()