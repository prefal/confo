#!/usr/bin/env python3
import unittest
import os

import server.pm4pyref  # noqa: F401
from pm4py.algo.conformance.alignments.utils import SKIP
from pm4py.algo.conformance.alignments.versions import state_equation_a_star as a_star
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.objects.log.log import Trace
from pm4py.objects.petri.importer import pnml as pnml_importer
from pm4py.objects.petri.petrinet import PetriNet
from pm4py.objects import petri

from server.objects.alignments import AlignmentData, ExtendedSearchTuple
from server.conformance import prefix_alignment as prefal
from server.conformance import auxiliary as aux

PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_data')


def load_data_example(filename_log, filename_net):
	log = xes_importer.import_log(os.path.join(PATH, filename_log))
	net, ini, fin = pnml_importer.import_net(os.path.join(PATH, filename_net))
	return log, net, ini, fin


def construct_path(tpl):
	ret = []
	if tpl.p is not None:
		ret.extend(construct_path(tpl.p))
	if tpl.t is not None:
		ret.append(tpl.t)
	return ret


def get_costs(incidence_matrix, sync_initial_marking, sync_final_marking, cost_function):
	return a_star.__vectorize_initial_final_cost(incidence_matrix,
													sync_initial_marking,
													sync_final_marking,
													cost_function)


class PrefalAuxiliaryMethodsTest(unittest.TestCase):
	""" Tests auxiliary methods supporting the prefix-alignment algorithm. """

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')
		self.tpl0 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 0, 0, list())
		self.tpl1 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 1, 0, list())
		self.tpl2 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 2, 0, list())
		self.tpl3 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 3, 0, list())
		self.tpl4 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, False, None, 7, 0, list('1'))
		self.tpl5 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 5, 0, list())
		self.tpl6 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 1, 0, list())
		self.tpl7 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 6, 0, list())
		self.tpl8 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, False, None, 7, 0, list())
		self.tpl9 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 9, 0, list())

	def test_is_sync_move_true(self):
		transition = PetriNet.Transition('name', ['string', 'string'])
		self.assertTrue(aux.is_sync_move(transition, SKIP))

	def test_is_sync_move_false_1(self):
		transition = PetriNet.Transition('name', [SKIP, 'string'])
		self.assertFalse(aux.is_sync_move(transition, SKIP))

	def test_is_sync_move_false_2(self):
		transition = PetriNet.Transition('name', ['string', SKIP])
		self.assertFalse(aux.is_sync_move(transition, SKIP))

	def test_is_sync_move_false_3(self):
		transition = PetriNet.Transition('name', [SKIP, SKIP])
		self.assertFalse(aux.is_sync_move(transition, SKIP))

	def test_is_log_move(self):
		transition = PetriNet.Transition('name', ['string', SKIP])
		self.assertTrue(aux.is_log_move(transition, SKIP))
		self.assertFalse(aux.is_model_move(transition, SKIP))

	def test_is_model_move(self):
		transition = PetriNet.Transition('name', [SKIP, 'string'])
		self.assertTrue(aux.is_model_move(transition, SKIP))
		self.assertFalse(aux.is_log_move(transition, SKIP))

	def test_is_tau_move_true(self):
		transition = PetriNet.Transition('name', [SKIP, None])
		self.assertTrue(aux.is_tau_move(transition, SKIP))

	def test_is_tau_move_false_1(self):
		transition = PetriNet.Transition('name', ['string', None])
		self.assertFalse(aux.is_tau_move(transition, SKIP))

	def test_is_tau_move_false_2(self):
		transition = PetriNet.Transition('name', [SKIP, 'string'])
		self.assertFalse(aux.is_tau_move(transition, SKIP))

	def test_is_tau_move_false_4(self):
		transition = PetriNet.Transition('name', [None, SKIP])
		self.assertFalse(aux.is_tau_move(transition, SKIP))

	def test_trace_net_moves(self):
		trace = self.log[0]
		trace_net, t_im, t_fm, _, _, _, _ = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		tnm = t_im
		tnm = aux.do_step_in_trace_net('a', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('b', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('c', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('d', trace_net, tnm)
		self.assertEqual(t_fm, tnm)

	def test_trace_net_moves_wrong_label(self):
		trace = self.log[0]
		trace_net, t_im, t_fm, _, _, _, _ = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		tnm = t_im
		tnm = aux.do_step_in_trace_net('a', trace_net, tnm)
		tnm_b = aux.do_step_in_trace_net('b', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('x', trace_net, tnm_b)
		tnm = aux.do_step_in_trace_net('d', trace_net, tnm)
		self.assertEqual(tnm_b, tnm)

	def test_is_enabled(self):
		trace = self.log[0]
		trace_net, t_im, t_fm, _, _, _, _ = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		for t in trace_net.transitions:
			if t.label == 'a':
				self.assertTrue(aux.is_enabled(t, trace_net, t_im))
			else:
				self.assertFalse(aux.is_enabled(t, trace_net, t_im))

	def test_trace_net_done(self):
		trace = self.log[0]
		trace_net, t_im, t_fm, _, _, _, _ = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		tnm = t_im
		tnm = aux.do_step_in_trace_net('a', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('b', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('c', trace_net, tnm)
		tnm = aux.do_step_in_trace_net('d', trace_net, tnm)
		self.assertTrue(aux.net_done(t_fm, tnm))

	def test_provide_feedback_sync_1(self):
		tran1 = PetriNet.Transition('trans', ('sync', 'sync'))
		tran2 = PetriNet.Transition('trans', (SKIP, None))
		tran3 = PetriNet.Transition('trans', ('sync', 'sync'))
		new_tpl = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 3, 0, [tran1, tran2, tran3])
		sync, move_on_model = aux.provide_feedback(new_tpl, SKIP)
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_provide_feedback_sync_2(self):
		tran1 = PetriNet.Transition('trans', (SKIP, None))
		tran2 = PetriNet.Transition('trans', (SKIP, None))
		new_tpl = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 3, 0, [tran1, tran2])
		sync, move_on_model = aux.provide_feedback(new_tpl, SKIP)
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_provide_feedback_model_move(self):
		tran1 = PetriNet.Transition('trans', ('sync', SKIP))
		tran2 = PetriNet.Transition('trans', ('sync', 'sync'))
		tran3 = PetriNet.Transition('trans', (SKIP, None))
		tran4 = PetriNet.Transition('trans', (SKIP, 'sync'))
		new_tpl = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 3, 0, [tran1, tran2, tran3, tran4])
		sync, move_on_model = aux.provide_feedback(new_tpl, SKIP)
		self.assertFalse(sync)
		self.assertTrue(move_on_model)

	def test_provide_feedback_log_move(self):
		tran1 = PetriNet.Transition('trans', (SKIP, 'sync'))
		tran2 = PetriNet.Transition('trans', (SKIP, None))
		tran3 = PetriNet.Transition('trans', ('sync', 'sync'))
		tran4 = PetriNet.Transition('trans', ('sync', SKIP))
		new_tpl = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 3, 0, [tran1, tran2, tran3, tran4])
		sync, move_on_model = aux.provide_feedback(new_tpl, SKIP)
		self.assertFalse(sync)
		self.assertFalse(move_on_model)

	def test_do_trace_back(self):
		tpl0 = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 10, 0, None)
		tpl1 = ExtendedSearchTuple(0, 0, 0, None, tpl0, None, None, True, None, 11, 0, None)
		tpl2 = ExtendedSearchTuple(0, 0, 0, None, tpl1, None, None, True, None, 12, 0, None)
		tpl3 = ExtendedSearchTuple(0, 0, 0, None, tpl2, None, None, True, None, 13, 0, None)
		tpl4 = ExtendedSearchTuple(0, 0, 0, None, tpl3, None, None, True, None, 14, 0, None)
		tpl5 = ExtendedSearchTuple(0, 0, 0, None, tpl4, None, None, True, None, 15, 0, None)
		tpl6 = ExtendedSearchTuple(0, 0, 0, None, tpl5, None, None, True, None, 16, 0, None)
		tpl7 = ExtendedSearchTuple(0, 0, 0, None, tpl6, None, None, True, None, 17, 0, None)
		tpl8 = ExtendedSearchTuple(0, 0, 0, None, tpl7, None, None, True, None, 18, 0, None)
		tpl9 = ExtendedSearchTuple(0, 0, 0, None, tpl8, None, None, True, None, 19, 0, None)

		self.assertEqual(tpl0.sp, aux.do_trace_back(tpl9, 9).sp)
		self.assertEqual(tpl0.sp, aux.do_trace_back(tpl8, 8).sp)
		self.assertEqual(tpl0.sp, aux.do_trace_back(tpl7, 7).sp)
		self.assertEqual(tpl0.sp, aux.do_trace_back(tpl6, 6).sp)

		self.assertEqual(tpl2.sp, aux.do_trace_back(tpl5, 3).sp)
		self.assertEqual(tpl2.sp, aux.do_trace_back(tpl4, 2).sp)
		self.assertEqual(tpl2.sp, aux.do_trace_back(tpl3, 1).sp)

		self.assertEqual(tpl1.sp, aux.do_trace_back(tpl1, 0).sp)
		self.assertEqual(tpl3.sp, aux.do_trace_back(tpl3, -5).sp)
		self.assertEqual(tpl0.sp, aux.do_trace_back(tpl7, 20).sp)

	def test_cost_upper_bound(self):
		trace = self.log[7]
		_, _, _, sync_net, _, _, cost_func = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)

		for e in trace:
			mock_costs = hash(e['time:timestamp']) % 876
			log_costs = 0
			for t in sync_net.transitions:
				if t.label[0] == e['concept:name'] and aux.is_log_move(t, SKIP):
					log_costs = cost_func[t]
			expected_costs = mock_costs + log_costs
			tpl = ExtendedSearchTuple(0, mock_costs, 0, None, None, None, None, True, None, 10, 0, None)
			cost_upper_bound = aux.cost_upper_bound(tpl, sync_net, e, cost_func, SKIP)
			self.assertEqual(expected_costs, cost_upper_bound)

	def tearDown(self):
		pass


class PrefalSearchEntirelyTest(unittest.TestCase):
	""" Tests search algorithm for prefix-alignments on a simple log and net.

	For each case of the log an alignment search is tested for the entire case
	in one go, computing the alignment from scratch.

	It is shown hereby that the search algorithm is downward compatible: It is
	possible to search for an (approximately) optimal (prefix-)alignment of a
	series of events at once, just as in classic alignments computation.
	"""

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')

	def test_search_throws_error(self):
		trace = self.log[0]
		trace_net, t_im, t_fm, sync, ini, fin, cost_func = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		with self.assertRaises(TypeError):
			return prefal._search(trace_net, t_im, t_fm, sync, ini, fin, cost_func, SKIP, old_search_tuple=set())

	def test_search_entirely_complete_trace_1(self):
		""" Trace 1 is 'abcd'."""
		trace = self.log[0]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for p in path:
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_entirely_complete_trace_2(self):
		""" Trace 2 is 'acbd'."""
		trace = self.log[1]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for p in path:
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_entirely_incomplete_trace_3(self):
		""" Trace 3 is 'ad'."""
		trace = self.log[2]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for i in range(len(path)):
			p = path[i]
			if i == 0 or i == 3:
				self.assertTrue(aux.is_sync_move(p, SKIP))
			else:
				self.assertFalse(aux.is_sync_move(p, SKIP))
				self.assertTrue(aux.is_model_move(p, SKIP))

	def test_search_entirely_incomplete_trace_4(self):
		""" Trace 4 is 'ab'."""
		trace = self.log[3]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for p in path:
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_entirely_incomplete_trace_5(self):
		""" Trace 5 is 'ac'."""
		trace = self.log[4]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for p in path:
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_entirely_incomplete_trace_6(self):
		""" Trace 6 is 'cd'."""
		trace = self.log[5]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		expr = aux.is_sync_move(path[1], SKIP) and aux.is_sync_move(path[3], SKIP) or \
				aux.is_sync_move(path[2], SKIP) and aux.is_sync_move(path[3], SKIP)
		self.assertTrue(expr)
		expr = not aux.is_sync_move(path[0], SKIP) and not aux.is_sync_move(path[1], SKIP) and \
				aux.is_model_move(path[0], SKIP) and aux.is_model_move(path[1], SKIP) or \
				not aux.is_sync_move(path[0], SKIP) and not aux.is_sync_move(path[2], SKIP) and \
				aux.is_model_move(path[0], SKIP) and aux.is_model_move(path[2], SKIP)
		self.assertTrue(expr)

	def test_search_entirely_incomplete_trace_7(self):
		""" Trace 7 is 'x'."""
		trace = self.log[6]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		for p in path:
			self.assertTrue(aux.is_log_move(p, SKIP))

	def test_search_entirely_incomplete_trace_8(self):
		""" Trace 8 is 'xa'."""
		trace = self.log[7]
		result = self.__search_entirely(trace)
		path = construct_path(result)
		print('    Result is: ', path)
		print()
		self.assertTrue(aux.is_log_move(path[0], SKIP))
		self.assertTrue(aux.is_sync_move(path[1], SKIP))

	def __search_entirely(self, trace):
		""" Help method to call prefix-alignment search for non-trivial traces,
		meaning for all events of the trace at once, starting from the sync
		net's initial marking.
		"""

		trace_net, t_im, t_fm, sync_net, ini, fin, cost_func = prefal._get_sync_trace_net_and_cost_function(
				trace, self.net,
				self.initial_marking,
				self.final_marking, SKIP)
		print('(Search-Entirely)   Trace is: ', [event['concept:name'] for event in trace])
		return prefal._search(trace_net, t_im, t_fm, sync_net, ini, fin, cost_func, SKIP)

	def tearDown(self):
		pass


class PrefalSearchIncrementallyTest(unittest.TestCase):
	""" Tests search algorithm for prefix-alignments on a simple log and net.

	For each case of the log an alignment search is tested for each event of the
	case separately, computing the alignment incrementally on partial traces.

	It is shown hereby that the alignment search is resumeable: It is possible
	to continue the search for an already existing (approximately) optimal
	(prefix-)alignment of a case whenever a new event is introduced and appended
	to the case.
	"""

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')

	def test_search_incrementally_trace_1(self):
		""" Trace 1 is 'abcd'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[0])
		# checks here
		for p in construct_path((tpls_dict['a'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['b'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['c'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['d'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_incrementally_trace_2(self):
		""" Trace 2 is 'acbd'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[1])
		# checks here
		for p in construct_path((tpls_dict['a'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['c'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['b'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['d'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_incrementally_trace_3(self):
		""" Trace 3 is 'ad'.

		Note
		----
		Due to the cost upper bound used in the incremental setup the best
		alignment for this case is [(a,a),(d,>>)]
		"""

		tpls_dict = self.__search_incrementally(self.log[2])
		for p in construct_path((tpls_dict['a'])):
			aux.is_sync_move(p, SKIP)
		path = construct_path((tpls_dict['d']))
		for i in range(len(path)):
			p = path[i]
			if i == 0 or i==3:
				self.assertTrue(aux.is_sync_move(p, SKIP))
			else:
				self.assertFalse(aux.is_sync_move(p, SKIP))
				self.assertTrue(aux.is_model_move(p, SKIP))

	def test_search_incrementally_trace_4(self):
		""" Trace 4 is 'ab'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[3])
		# checks here
		for p in construct_path((tpls_dict['a'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['b'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_incrementally_trace_5(self):
		""" Trace 5 is 'ac'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[4])
		# checks here
		for p in construct_path((tpls_dict['a'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))
		for p in construct_path((tpls_dict['c'])):
			self.assertTrue(aux.is_sync_move(p, SKIP))

	def test_search_incrementally_trace_6(self):
		""" Trace 6 is 'cd'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[5])
		# checks here
		p = construct_path((tpls_dict['c']))
		for i in range(len(p)):
			if i == 0:
				self.assertTrue(aux.is_model_move(p[i], SKIP))
			else:
				self.assertTrue(aux.is_sync_move(p[i], SKIP))
		p = construct_path((tpls_dict['d']))
		expr = aux.is_sync_move(p[1], SKIP) and aux.is_sync_move(p[3], SKIP) or \
				aux.is_sync_move(p[2], SKIP) and aux.is_sync_move(p[3], SKIP)
		self.assertTrue(expr)
		expr = not aux.is_sync_move(p[0], SKIP) and not aux.is_sync_move(p[1], SKIP) and \
				aux.is_model_move(p[0], SKIP) and aux.is_model_move(p[1], SKIP) or \
				not aux.is_sync_move(p[0], SKIP) and not aux.is_sync_move(p[2], SKIP) and \
				aux.is_model_move(p[0], SKIP) and aux.is_model_move(p[2], SKIP)
		self.assertTrue(expr)

	def test_search_incrementally_trace_7(self):
		""" Trace 7 is 'x'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[6])
		# checks here
		for p in construct_path((tpls_dict['x'])):
			self.assertFalse(aux.is_sync_move(p, SKIP))

	def test_search_incrementally_trace_8(self):
		""" Trace 8 is 'xa'."""
		# run conf-checker incrementally for trace
		tpls_dict = self.__search_incrementally(self.log[7])
		# checks here
		for p in construct_path((tpls_dict['x'])):
			self.assertFalse(aux.is_sync_move(p, SKIP))
		p = construct_path((tpls_dict['a']))
		for i in range(len(p)):
			if i == 0:
				self.assertTrue(aux.is_log_move(p[i], SKIP))
			else:
				self.assertTrue(aux.is_sync_move(p[i], SKIP))

	def __search_incrementally(self, trace):
		""" Help method to call prefix-alignment search for non-trivial traces
		incrementally, meaning for each event of a trace, starting from
		markings in the sync net discovered in previous iterations.
		"""

		tpls_dict = dict()
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		trace_len = len(trace)

		for i in range(trace_len):
			event = trace[i]
			alignment_data.append_event(event)
			partial_trace = alignment_data.get_current_trace()
			trace_net, t_im, t_fm, sync_net, ini, fin, cost_func = prefal._get_sync_trace_net_and_cost_function(
					partial_trace, self.net,
					self.initial_marking,
					self.final_marking, SKIP)
			print('(Search-Incrementally)   Partial trace is: ', [event['concept:name'] for event in partial_trace])

			# compute alignment here
			if len(partial_trace) == 1:
				result = prefal._search(trace_net, t_im, t_fm, sync_net, ini, fin, cost_func, SKIP)
			else:
				old_tpl = alignment_data.get_state()
				# cost_upper_bound = aux.cost_upper_bound(old_tpl, sync_net, event, cost_func, SKIP)
				result = prefal._search(trace_net, t_im, t_fm, sync_net, ini, fin, cost_func, SKIP, old_tpl)
										# cost_upper_bound)

			path = construct_path(result)
			print('    Result is: ', path)
			print()

			tpls_dict[event['concept:name']] = result
			alignment_data.set_state(result)
		return tpls_dict

	def tearDown(self):
		pass


class PrefalSearchSyncMoveTest(unittest.TestCase):

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'complex_event_log.xes', 'complex_petri_net.pnml')
		self.trace = Trace(list(), attributes={'concept:name': 'TEST_ID', 'creator': 'ConfOSystem'})

	def __prepare(self):
		self.trace_net, self.trace_initial_marking, self.trace_final_marking, self.sync_net, \
			self.sync_initial_marking, self.sync_final_marking, self.cost_function	= \
				prefal._get_sync_trace_net_and_cost_function(
					self.trace,
					self.net,
					self.initial_marking,
					self.final_marking,
					SKIP)

		self.incidence_matrix = petri.incidence_matrix.construct(self.sync_net)
		self.ini_vec, self.fin_vec, self.cost_vec = get_costs(self.incidence_matrix,
																self.sync_initial_marking,
																self.sync_final_marking,
																self.cost_function)

	def test_search_sync_moves_1(self):

		# Prep
		for event in self.log[0][0:2]:
			self.trace.append(event)
		self.__prepare()

		# debug
		# from pm4py.visualization.petrinet import factory as petri_viz
		# gviz = petri_viz.apply(self.sync_net, self.sync_initial_marking, self.sync_final_marking)
		# petri_viz.view(gviz)
		# gviz = petri_viz.apply(self.net, self.initial_marking, self.final_marking)
		# petri_viz.view(gviz)
		# debug

		marking = self.sync_initial_marking
		trans = None
		tn_marking = self.trace_initial_marking
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'ER Registration' and t.label[1] == 'ER Registration':
				marking = petri.semantics.execute(t, self.sync_net, self.sync_initial_marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		tpl = ExtendedSearchTuple(0, 0, 0, marking, None, trans, 0, False, tn_marking, 0, 0, list())
		event = self.trace[1]

		# Exec
		sync_move_found, result = self.__search_sync_moves(event, tpl)

		# Check
		print('Started from search tuple:')
		print(construct_path(tpl))
		print('Shortest path to subsequent sync move is:')
		print(result.np)
		for i in range(len(result.np)):
			t = result.np[i]
			if i == len(result.np)-1:
				self.assertTrue(aux.is_sync_move(t, SKIP))
			else:
				self.assertTrue(aux.is_tau_move(t, SKIP))

	def test_search_sync_moves_2(self):

		# Prep
		for event in self.log[1][0:2]:
			self.trace.append(event)
		self.__prepare()

		# debug
		# from pm4py.visualization.petrinet import factory as petri_viz
		# gviz = petri_viz.apply(self.sync_net, self.sync_initial_marking, self.sync_final_marking)
		# petri_viz.view(gviz)
		# gviz = petri_viz.apply(self.net, self.initial_marking, self.final_marking)
		# petri_viz.view(gviz)
		# debug

		marking = self.sync_initial_marking
		trans = None
		tn_marking = self.trace_initial_marking
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'ER Registration' and t.label[1] == 'ER Registration':
				marking = petri.semantics.execute(t, self.sync_net, self.sync_initial_marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		tpl = ExtendedSearchTuple(0, 0, 0, marking, None, trans, 0, False, tn_marking, 0, 0, list())
		event = self.trace[1]

		# Exec
		sync_move_found, result = self.__search_sync_moves(event, tpl)

		# Check
		print('Started from search tuple:')
		print(construct_path(tpl))
		print('Shortest path to subsequent sync move is:')
		print(result.np)
		for i in range(len(result.np)):
			t = result.np[i]
			if i == len(result.np)-1:
				self.assertTrue(aux.is_sync_move(t, SKIP))
			else:
				self.assertTrue(aux.is_tau_move(t, SKIP))

	def test_search_sync_moves_3(self):

		# Prep
		for event in self.log[14][0:5]:
			self.trace.append(event)
		self.__prepare()

		# debug
		from pm4py.visualization.petrinet import factory as petri_viz
		# gviz = petri_viz.apply(self.sync_net, self.sync_initial_marking, self.sync_final_marking)
		# petri_viz.view(gviz)
		# gviz = petri_viz.apply(self.net, self.initial_marking, self.final_marking)
		# petri_viz.view(gviz)
		# debug

		marking = self.sync_initial_marking
		trans = None
		tn_marking = self.trace_initial_marking
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'ER Registration' and t.label[1] == 'ER Registration':
				marking = petri.semantics.execute(t, self.sync_net, self.sync_initial_marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if aux.is_tau_move(t, SKIP):
				marking = petri.semantics.execute(t, self.sync_net, marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if aux.is_tau_move(t, SKIP):
				marking = petri.semantics.execute(t, self.sync_net, marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'ER Triage' and t.label[1] == 'ER Triage':
				marking = petri.semantics.execute(t, self.sync_net, marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break

		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'ER Sepsis Triage' and t.label[1] == 'ER Sepsis Triage':
				marking = petri.semantics.execute(t, self.sync_net, marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break
		for t in petri.semantics.enabled_transitions(self.sync_net, marking):
			if t.label[0] == 'IV Liquid' and t.label[1] == 'IV Liquid':
				marking = petri.semantics.execute(t, self.sync_net, marking)
				tn_marking = aux.do_step_in_trace_net(t.label[0], self.trace_net, tn_marking)
				trans = t
				break

		tpl = ExtendedSearchTuple(0, 0, 0, marking, None, trans, 0, False, tn_marking, 0, 0, list())
		event = self.trace[4]

		# Exec
		sync_move_found, result = self.__search_sync_moves(event, tpl)

		# Check
		print('Started from search tuple:')
		print(construct_path(tpl))
		print('Shortest path to subsequent sync move is:')
		print(result.np)
		for i in range(len(result.np)):
			t = result.np[i]
			if i == len(result.np)-1:
				self.assertTrue(aux.is_sync_move(t, SKIP))
			else:
				self.assertTrue(aux.is_tau_move(t, SKIP))

	def __search_sync_moves(self, event, tpl):
		return prefal._search_sync_move(self.trace_net, self.sync_net, self.cost_function,
											self.incidence_matrix, self.cost_vec, self.fin_vec,
											event, SKIP, to_travers=[tpl])

	def tearDown(self):
		pass


class PrefalApplySimpleTest(unittest.TestCase):

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')

	def test_apply_throws_error_1(self):
		""" Throw error on trace 1 is 'abcd' when giving no event as input."""
		trace = self.log[0]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		with self.assertRaises(ValueError):
			prefal.apply(self.net, self.initial_marking, self.final_marking, alignment_data)

	def test_apply_throws_error_2(self):
		""" Throw error on trace 1 is 'abcd' when giving no event as input."""
		trace = self.log[0]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		with self.assertRaises(ValueError):
			prefal.apply(self.net, self.initial_marking, self.final_marking, alignment_data, *[])

	def test_apply_incremental_trace_1(self):
		""" Trace 1 is 'abcd'."""
		trace = self.log[0]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertTrue(sync)
			self.assertFalse(move_on_model)

	def test_apply_incremental_trace_2(self):
		""" Trace 2 is 'acbd'."""
		trace = self.log[1]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertTrue(sync)
			self.assertFalse(move_on_model)

	def test_apply_incremental_trace_3(self):
		""" Trace 3 is 'ad'.

		Note
		----
		Due to the cost upper bound used in the incremental setup the best
		alignment for this case is [(a,a),(d,>>)]
		"""

		trace = self.log[2]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			if e['concept:name'] == 'a':
				self.assertTrue(sync)
				self.assertFalse(move_on_model)
			else:
				self.assertFalse(sync)
				self.assertTrue(move_on_model)

	def test_apply_incremental_trace_4(self):
		""" Trace 4 is 'ab'."""
		trace = self.log[3]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertTrue(sync)
			self.assertFalse(move_on_model)

	def test_apply_incremental_trace_5(self):
		""" Trace 5 is 'ac'."""
		trace = self.log[4]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertTrue(sync)
			self.assertFalse(move_on_model)

	def test_apply_incremental_trace_6(self):
		""" Trace 6 is 'cd'."""
		trace = self.log[5]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertFalse(sync)
			self.assertTrue(move_on_model)

	def test_apply_incremental_trace_7(self):
		""" Trace 7 is 'x'."""
		trace = self.log[6]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			self.assertFalse(sync)
			self.assertFalse(move_on_model)

	def test_apply_incremental_trace_8(self):
		""" Trace 8 is 'xa'."""
		trace = self.log[7]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)
		for e in trace:
			alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																			self.final_marking, alignment_data, e)
			state = alignment_data.get_state()
			print(e['concept:name'] + ': ' + str(construct_path(state)) + ' |  NEW: ' + str(state.np))
			print('  Total SearchTuple:\n  ', repr(state).strip())
			if e['concept:name'] == 'x':
				self.assertFalse(sync)
				self.assertFalse(move_on_model)
			else:
				self.assertTrue(sync)
				self.assertFalse(move_on_model)

	def test_apply_trace_1(self):
		""" Trace 1 is 'abcd'."""
		trace = self.log[0]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_apply_trace_2(self):
		""" Trace 2 is 'acbd'."""
		trace = self.log[1]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_apply_trace_3(self):
		""" Trace 3 is 'ad'."""
		trace = self.log[2]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertFalse(sync)
		self.assertTrue(move_on_model)

	def test_apply_trace_4(self):
		""" Trace 4 is 'ab'."""
		trace = self.log[3]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_apply_trace_5(self):
		""" Trace 5 is 'ac'."""
		trace = self.log[4]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertTrue(sync)
		self.assertFalse(move_on_model)

	def test_apply_trace_6(self):
		""" Trace 6 is 'cd'."""
		trace = self.log[5]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertFalse(sync)
		self.assertTrue(move_on_model)

	def test_apply_trace_7(self):
		""" Trace 7 is 'x'."""
		trace = self.log[6]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertFalse(sync)
		self.assertFalse(move_on_model)

	def test_apply_trace_8(self):
		""" Trace 8 is 'xa'."""
		trace = self.log[7]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
															alignment_data, *trace)
		print(alignment_data.get_state())
		self.assertFalse(sync)
		self.assertFalse(move_on_model)

	def tearDown(self):
		pass


class PrefalApplyMixtureTest(unittest.TestCase):

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')

	def test_apply_mixture_trace_1(self):
		""" Tests a mixture of calls to apply and apply_incremental

		1) one single event with apply_incremental,
		2) then two events at once with apply,
		3) and the last event again with apply_incremental

		Note:
		In Step 1) we find some search tuples.
		Apply call in step 2) neglects the search tuples found in 1).
		Step 3) works on the search tuples found in 2).
		"""

		trace = self.log[0]
		case_id = trace.attributes['concept:name']
		alignment_data = AlignmentData(case_id)

		# 1)
		print('Apply Incremental (based on last result)')
		e = trace[0]
		alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																		self.final_marking,
																		alignment_data, e)
		print(e['concept:name'] + ': ' + str(construct_path(alignment_data.get_state())))
		self.assertTrue(sync)
		self.assertFalse(move_on_model)
		print()

		# 2)
		print('Apply (from scratch)')
		e = [trace[1], trace[2]]
		alignment_data, sync, move_on_model = prefal.apply(self.net, self.initial_marking, self.final_marking,
																		alignment_data, *e)
		print(e[len(e) - 1]['concept:name'] + ': ' + str(construct_path(alignment_data.get_state())))
		self.assertTrue(sync)
		self.assertFalse(move_on_model)
		print()

		# 3)
		print('Apply Incremental (based on last result)')
		e = trace[3]
		alignment_data, sync, move_on_model, _, _ = prefal.apply_incremental(self.net, self.initial_marking,
																		self.final_marking,
																		alignment_data, e)
		print(e['concept:name'] + ': ' + str(construct_path(alignment_data.get_state())))
		self.assertTrue(sync)
		self.assertFalse(move_on_model)
		print()

	def tearDown(self):
		pass


class PrefalApplyComplexTest(unittest.TestCase):

	def setUp(self):
		self.log, self.net, self.initial_marking, self.final_marking = load_data_example(
				'simple_event_log.xes', 'simple_petri_net.pnml')

	def test_xxx(self):
		self.assertTrue(True)

	def tearDown(self):
		pass


if __name__ == '__main__':
	unittest.main()
