#!/usr/bin/env python3
import unittest

# Import python code under test
#import server. ...

# Let Test Classes end with 'Test'
class ClassXXXTest(unittest.TestCase):

    '''
    Use this to instantiate repetitive set-up code.
    Is called once prior to each test case.
    '''
    def setUp(self):
        print('SETUP')
        pass

    '''
    Unittest fuctions have to start with lowercase 'test_'.
    Use asserts to test a certain function(ality) of code under test.
    '''
    def test_xxx(self):
        self.assertTrue(True)

    '''
    Use this to delete what was created by setUp().
    Is called once after each test case.
    '''
    def tearDown(self):
        print('TEARDOWN')
        pass

if __name__ == '__main__':
    unittest.main()