#!/usr/bin/env python3.6

import unittest
import io
import sys

# Import python code under test
from server import constants

# Let Test Classes end with 'Test'
class ConstantsTest(unittest.TestCase):


	def test_default(self):
		constants.default()
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, constants.DEFAULT_MAX_NUMBER_OF_WORKERS)
		self.assertEqual(constants.NET_PATH, constants.DEFAULT_NET_PATH)
		self.assertEqual(constants.ALGO_WINDOW_SIZE, constants.DEFAULT_ALGO_WINDOW_SIZE)
		self.assertEqual(constants.HTTP_PORT, constants.DEFAULT_HTTP_PORT)

	def test_pretty_print(self):
		captured_output = io.StringIO()  # Create StringIO object
		sys.stdout = captured_output  # and redirect stdout.
		constants.pretty_print()  # Call unchanged function.
		sys.stdout = sys.__stdout__  # Reset redirect.
		# self.assert ...
		print('\nCaptured:\n',captured_output.getvalue())


if __name__ == '__main__':
	unittest.main()