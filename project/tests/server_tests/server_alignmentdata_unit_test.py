#!/usr/bin/env python3
import unittest

# Import python code under test
from server.objects.alignments import AlignmentData, ExtendedSearchTuple

from pm4py.objects.log.log import Event


class AlignmentDataTest(unittest.TestCase):

	def setUp(self):
		self.ID = '123abc'
		self.alignment_data = AlignmentData(self.ID)
		self.tpl = ExtendedSearchTuple(0, 0, 0, None, None, None, None, True, None, 0, 0, None)
		pass

	def test_get_case_id(self):
		self.assertEqual(self.ID, self.alignment_data.get_case_id())

	def test_repr(self):
		representation = repr(self.alignment_data)
		self.assertTrue(isinstance(representation, str))

	def test_append_event(self):
		name = 'example_activity_A'
		self.alignment_data.append_event(Event({'concept:name':name}))
		self.assertEqual(name, self.alignment_data.get_current_trace()[0]['concept:name'])

	def test_append_event_throws_exception(self):
		with self.assertRaises(TypeError):
			self.alignment_data.append_event(set())

	def test_get_current_trace(self):
		trace = [{'concept:name': 'a'}, {'concept:name':  'xyz'}, {'concept:name': 'vdA'}, {'concept:name': 'string'}]
		self.alignment_data.append_event(Event(trace[0]))
		self.alignment_data.append_event(Event(trace[1]))
		self.alignment_data.append_event(Event(trace[2]))
		self.alignment_data.append_event(Event(trace[3]))
		other_trace = self.alignment_data.get_current_trace()
		self.assertEqual(trace, other_trace._list)

	def test_set_search_tuples(self):
		self.alignment_data.set_state(self.tpl)
		self.assertEqual(self.tpl, self.alignment_data.get_state())

	def test_set_search_tuples_existing_entry(self):
		self.assertTrue(isinstance(self.alignment_data.get_state(), ExtendedSearchTuple))
		self.alignment_data.set_state(self.tpl)
		self.assertEqual(self.tpl, self.alignment_data.get_state())
		# second set call
		self.alignment_data.set_state(self.tpl)
		self.assertEqual(self.tpl, self.alignment_data.get_state())

	def test_set_search_tuples_throws_exception_1(self):
		with self.assertRaises(TypeError):
			self.alignment_data.set_state(list('string'))

	def test_set_search_tuples_throws_exception_2(self):
		with self.assertRaises(TypeError):
			self.alignment_data.set_state(set())

	def test_get_search_tuples_empty(self):
		self.assertTrue(isinstance(self.alignment_data.get_state(), ExtendedSearchTuple))

	def test_get_search_tuples_not_empty(self):
		self.alignment_data.set_state(self.tpl)
		self.assertEqual(self.tpl, self.alignment_data.get_state())

	def tearDown(self):
		pass


class ExtendedSearchTupleTest(unittest.TestCase):

	def setUp(self):
		self.tpl0 = ExtendedSearchTuple(10, 0, 0, None, None, None, None, True, None, 0, 0, list())
		self.tpl1 = ExtendedSearchTuple(20, 0, 0, None, None, None, None, True, None, 0, 0, list())
		self.tpl2 = ExtendedSearchTuple(10, 0, 0, None, None, None, None, True, None, 10, 0, list())
		self.tpl3 = ExtendedSearchTuple(10, 0, 0, None, None, None, None, False, None, 0, 0, list())
		self.tpl4 = ExtendedSearchTuple(10, 0, 10, None, None, None, None, True, None, 0, 0, list())
		self.tpl5 = ExtendedSearchTuple(10, 0, 0, None, None, None, None, True, None, 0, 10, list())

	def test_extended_search_tuple_lt_f_1(self):
		self.assertLess(self.tpl0, self.tpl1)

	def test_extended_search_tuple_lt_f_2(self):
		self.assertGreater(self.tpl1, self.tpl0)

	def test_extended_search_tuple_lt_sp_1(self):
		self.assertLess(self.tpl0, self.tpl2)

	def test_extended_search_tuple_lt_sp_2(self):
		self.assertGreater(self.tpl2, self.tpl0)

	def test_extended_search_tuple_lt_trust_1(self):
		self.assertTrue(self.tpl0 < self.tpl3)

	def test_extended_search_tuple_lt_trust_2(self):
		self.assertFalse(self.tpl3 < self.tpl0)

	def test_extended_search_tuple_lt_h_1(self):
		self.assertLess(self.tpl0, self.tpl4)

	def test_extended_search_tuple_lt_h_2(self):
		self.assertGreater(self.tpl4, self.tpl0)

	def test_extended_search_tuple_lt_tr_1(self):
		self.assertGreater(self.tpl0, self.tpl5)

	def test_extended_search_tuple_lt_tr_2(self):
		self.assertLess(self.tpl5, self.tpl0)

	def test_repr(self):
		strings = repr(self.tpl0).strip().split('  ')
		parameters = [string.split('=')[0] for string in strings]
		self.assertEqual(['m', 'tm', 'sp', 'tr', 'f', 'g', 'h', 'path', 'np'], parameters)

	def tearDown(self):
		pass


if __name__ == '__main__':
	unittest.main()
