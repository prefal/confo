#!/usr/bin/env python3
import unittest
import os
import server.pm4pyref  # noqa: F401
from pm4py.objects.petri.importer import pnml as pnml_importer
from server.view_statistics import factory


FILENAME_NET = 'simple_petri_net.pnml'

# Constants
N_M_MODEL = factory.N_M_MODEL
N_M_LOG = factory.N_M_LOG
N_M_SYNC = factory.N_M_SYNC
ACTIVATED = factory.ACTIVATED


def load_petri_net():
	path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'test_data', FILENAME_NET)
	net, initial_marking, final_marking = pnml_importer.import_net(path)
	return net, initial_marking, final_marking


class ViewStatisticTest(unittest.TestCase):

	def setUp(self):
		print('SETUP')
		pass

	def test_generate_png(self):
		net, initial_marking, final_marking = load_petri_net()
		# Prepare net

		# generate random number of statistics
		dic_deviation = dict()
		# transition
		dic_deviation['t'] = dict()
		for t in net.transitions:
			dic_deviation['t'][t.label] = dict()
			dic_deviation['t'][t.label][N_M_SYNC] = 1
			dic_deviation['t'][t.label][N_M_LOG] = 0
			dic_deviation['t'][t.label][N_M_MODEL] = 0
		gviz_1 = factory.apply(
				net, image_format='png',
				curr_event_name='register request',
				initial_marking=initial_marking, final_marking=final_marking,
				is_show_deviation=True, dic_deviation=dic_deviation,
				variant='statistic_with_alignment',
				is_show_place=True,
				debug=False
		)
		gviz_2 = factory.apply(
				net, image_format='png',
				curr_event_name='register request',
				initial_marking=initial_marking, final_marking=final_marking,
				is_show_deviation=True, dic_deviation=dic_deviation,
				variant='statistic_only',
				is_show_place=False,
				debug=False
		)

		self.assertTrue(gviz_1 is not None)
		self.assertTrue(gviz_2 is not None)

	def tearDown(self):
		print('TEARDOWN')
		pass


if __name__ == '__main__':
	unittest.main()
