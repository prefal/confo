#!/usr/bin/env python3
import unittest

# Import python code under test
from server.objects.locks import ReadWriteLock
from server.exceptions import TooManyReadReleases


class ReadWriteLockTest(unittest.TestCase):
	def setUp(self):
		self.CONCURRENT_READS_TEST = 5
		self.rw_lock_1 = ReadWriteLock()
		self.rw_lock_X = ReadWriteLock(self.CONCURRENT_READS_TEST)

	def test_init_correct_max_readers(self):
		self.assertEqual(self.rw_lock_1._max_readers, 1)
		self.assertEqual(self.rw_lock_X._max_readers, self.CONCURRENT_READS_TEST)

	def test_init_throws_value_error(self):
		with self.assertRaises(ValueError):
			ReadWriteLock(-1)

	def test_acquire_read(self):
		self.rw_lock_1.acquire_read()

	def test_acquire_read_X(self):
		self.rw_lock_X.acquire_read()
		self.rw_lock_X.acquire_read()
		self.rw_lock_X.acquire_read()
		self.rw_lock_X.acquire_read()
		self.rw_lock_X.acquire_read()

	def test_release_read_throws_exception(self):
		with self.assertRaises(TooManyReadReleases):
			self.rw_lock_1.release_read()
		with self.assertRaises(TooManyReadReleases):
			self.rw_lock_X.release_read()

	def test_acquire_write(self):
		self.rw_lock_1.acquire_write()

	def test_acquire_write_X(self):
		self.rw_lock_X.acquire_write()

	def test_release_write(self):
		self.rw_lock_1.acquire_write()
		self.rw_lock_1.release_write()

	def test_release_write_X(self):
		self.rw_lock_X.acquire_write()
		self.rw_lock_X.release_write()

	def test_concurrent_reads(self):
		import random
		from multiprocessing import Process, Value
		x = Value('i', 0, lock=False)
		processes = list()
		for i in range(20):
			rest_time = random.randint(0, 1)
			p = Process(target=self._process_mock_read, args=(self.rw_lock_X, x, rest_time, i))
			processes.append(p)
			p.start()
		for p in processes:
			p.join()
		self.assertEqual(x.value, 0)

	def test_sequential_writes(self):
		from multiprocessing import Process, Value
		x = Value('i', 0, lock=False)
		processes = list()
		for i in range(10):
			p = Process(target=self._process_mock_write, args=(self.rw_lock_X, x, 0.2))
			processes.append(p)
			p.start()
		for p in processes:
			p.join()
		self.assertEqual(x.value, 10)

	def test_concurrent_reads_and_writes(self):
		from multiprocessing import Process, Value
		x = Value('i', 0, lock=False)
		processes = list()
		for i in range(5):
			p1 = Process(target=self._process_mock_read, args=(self.rw_lock_X, x, 0.2, i))
			p2 = Process(target=self._process_mock_write, args=(self.rw_lock_X, x, 0.2))
			processes.append(p1)
			processes.append(p2)
			p1.start()
			p2.start()
		for p in processes:
			p.join()
		self.assertEqual(x.value, 5)

	def test_force_read_wait(self):
		from multiprocessing import Process, Value
		x = Value('i', 0, lock=False)
		processes = list()
		for i in range(2):
			p = Process(target=self._process_mock_read, args=(self.rw_lock_1, x, 1, i))
			processes.append(p)
			p.start()
		for p in processes:
			p.join()
		self.assertEqual(x.value, 0)

	def test_force_write_wait(self):
		from multiprocessing import Process, Value
		x = Value('i', 0, lock=False)
		processes = list()
		for i in range(2):
			p = Process(target=self._process_mock_write, args=(self.rw_lock_1, x, 1))
			processes.append(p)
			p.start()
		for p in processes:
			p.join()
		self.assertEqual(x.value, 2)

	@staticmethod
	def _process_mock_read(lock, x, rest_time, number):
		from time import sleep
		lock.acquire_read()
		sleep(rest_time)
		print('Proc:' + str(number) + ', Value of x: ' + str(x.value) + ', READERS:' + str(lock._readers.value) + '/' +
			str(lock._max_readers))
		sleep(rest_time)
		lock.release_read()

	@staticmethod
	def _process_mock_write(lock, x, rest_time):
		from time import sleep
		lock.acquire_write()
		sleep(rest_time)
		x.value += 1
		sleep(rest_time)
		lock.release_write()

	def tearDown(self):
		pass


if __name__ == '__main__':
	unittest.main()
