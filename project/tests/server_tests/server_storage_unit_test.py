#!/usr/bin/env python3.6
import unittest
import os

# Import python code under test
from server.objects.alignments import AlignmentData
from server.objects.locks import ReadWriteLock
from server.storage.alignments.main import AlignmentDataStorageInterface
from server.storage.statistics.main import StatisticDataStorageInterface
from server.storage.physical.pickleharddrive import PickleHardDrive
from server.storage.storage_interface import DataStorageInterface
from server.view_statistics.factory import N_M_SYNC, N_M_LOG, N_M_MODEL
from pm4py.objects.log.log import Event

sample_data = [{'one': dict(), 'two': list(), 'three': 'string'},
				dict(),
				list(),
				'string']


class PickleHarddriveTest(unittest.TestCase):

	def setUp(self):
		self.FILE_EXT = '.test'
		self.pds = PickleHardDrive('test', '.test')
		self.pds.instantiate_storage()
		self.pds.clean_up()
		self.assertTrue(os.path.isdir(self.pds.PATH))

	def test_init_create_new_dir(self):
		self.pds.clean_up()
		os.removedirs(self.pds.PATH)
		self.assertFalse(os.path.isdir(self.pds.PATH))
		self.pds.instantiate_storage()
		self.assertTrue(os.path.isdir(self.pds.PATH))

	def test_init_dir_exists(self):
		for i in range(len(sample_data)):
			item = sample_data[i]
			filename = str(i) + self.FILE_EXT
			self.pds.set(str(i), item)
			self.assertTrue(os.path.isfile(os.path.join(self.pds.PATH, filename)))

		self.pds.instantiate_storage()  # Call function to change state.

		for i in range(len(sample_data)):
			filename = str(i) + self.FILE_EXT
			self.assertFalse(os.path.isfile(os.path.join(self.pds.PATH, filename)))

	def test_write_successful(self):
		for i in range(len(sample_data)):
			item = sample_data[i]
			filename = str(i) + self.FILE_EXT
			self.pds.set(str(i), item)
			self.assertTrue(os.path.isfile(os.path.join(self.pds.PATH, filename)))

	def test_write_read_successful(self):
		# write test files first
		for i in range(len(sample_data)):
			item = sample_data[i]
			filename = str(i) + self.FILE_EXT
			self.pds.set(str(i), item)
			self.assertTrue(os.path.isfile(os.path.join(self.pds.PATH, filename)))
		# test read
		for i in range(len(sample_data)):
			status, item = self.pds.get(str(i))
			self.assertTrue(status)
			self.assertIsNotNone(item)
			self.assertEqual(item, sample_data[i])

	def test_read_unsuccessful(self):
		status, item = self.pds.get('bonkers')
		self.assertFalse(status)
		self.assertIsNone(item)

	def test_overwrite(self):
		self.pds.set(str(1), sample_data[1])
		new_dict = sample_data[1]
		new_dict['four'] = 'added'
		self.pds.set(str(1), new_dict)
		status, item = self.pds.get(str(1))
		self.assertTrue(status)
		self.assertIsNotNone(item)
		self.assertEqual(new_dict.keys(), item.keys())

	def tearDown(self):
		self.pds.clean_up()


class DataStorageInterfaceTest(unittest.TestCase):

	def setUp(self):
		# PickleHardDrive is default test handler because one handler is used
		# by the interface. Specify, if necessary, a different one here.
		self.handler = PickleHardDrive('test', '.test')
		self.data_interface = DataStorageInterface(self.handler, max_concurrent_reads_allowed=1)
		self.ID = '123abc'

	def test_init_parameter_not_a_handler(self):
		with self.assertRaises(TypeError):
			AlignmentDataStorageInterface(self.ID, max_concurrent_reads_allowed=1)

	def test_control_mechanism_is_correctly_added(self):
		self.data_interface._add_new_control_access_instances(self.ID)
		self.assertTrue(self.data_interface._check_if_control_access_exists(self.ID))

	def test_control_mechanism_does_not_exists(self):
		self.assertFalse(self.data_interface._check_if_control_access_exists(self.ID))

	def test_add_new_control_mechanism_instances(self):
		self.data_interface._add_new_control_access_instances(self.ID)
		self.assertTrue(isinstance(self.data_interface.control_access[self.ID], ReadWriteLock))

	def tearDown(self):
		self.data_interface.clean_up()


class AlignmentDataStorageInterfaceTest(unittest.TestCase):

	def setUp(self):
		# PickleHardDrive is default test handler because one handler is used
		# by the interface. Specify, if necessary, a different one here.
		self.handler = PickleHardDrive('test', '.ad')

		self.data_interface = AlignmentDataStorageInterface(self.handler, max_concurrent_reads_allowed=1)
		self.ID = '123abc'
		self.alignment_data = AlignmentData(self.ID)

	def test_write_successful_single(self):
		self.data_interface.write_alignment_data(self.alignment_data)

	def test_write_throws_exception(self):
		with self.assertRaises(TypeError):
			self.data_interface.write_alignment_data(Exception)
		with self.assertRaises(TypeError):
			self.data_interface.write_alignment_data('string')
		with self.assertRaises(TypeError):
			self.data_interface.write_alignment_data(123456789)

	def test_write_twice(self):
		self.data_interface.write_alignment_data(self.alignment_data)
		self.data_interface.write_alignment_data(self.alignment_data)

	def test_write_read_successful_single(self):
		self.data_interface.write_alignment_data(self.alignment_data)
		status, data = self.data_interface.read_alignment_data(self.ID)
		self.assertTrue(status)
		self.assertTrue(isinstance(data, AlignmentData))
		self.assertEqual(self.alignment_data.get_case_id(), data.get_case_id())

	def test_read_None(self):
		status, data = self.data_interface.read_alignment_data(self.ID)
		self.assertFalse(status)
		self.assertIsNone(data)

	def test_write_read_successful_rich_object(self):
		self.alignment_data.append_event(Event({'concept:name': 'a'}))
		self.alignment_data.append_event(Event({'concept:name': 'b'}))
		self.alignment_data.append_event(Event({'concept:name': 'c'}))
		self.data_interface.write_alignment_data(self.alignment_data)
		status, data = self.data_interface.read_alignment_data(self.ID)
		self.assertTrue(status)
		self.assertTrue(isinstance(data, AlignmentData))
		self.assertEqual(self.alignment_data.get_case_id(), data.get_case_id())
		self.assertEqual(repr(self.alignment_data.get_current_trace()), repr(data.get_current_trace()))

	def tearDown(self):
		self.data_interface.clean_up()


class StatisticDataStorageInterfaceTest(unittest.TestCase):

	def setUp(self):
		# PickleHardDrive is default test handler because one handler is used
		# by the interface. Specify, if necessary, a different one here.
		self.FILE_EXT = '.stat'
		self.handler = PickleHardDrive('test', self.FILE_EXT)

		self.data_interface = StatisticDataStorageInterface(self.handler, max_concurrent_reads_allowed=1)
		self.statistics_1 = dict()
		self.statistics_1['123'] = dict()
		self.statistics_1['123'][N_M_SYNC] = 10
		self.statistics_1['123'][N_M_LOG] = 20
		self.statistics_1['123'][N_M_MODEL] = 30
		self.statistics_2 = dict()
		self.statistics_2['abc'] = dict()
		self.statistics_2['abc'][N_M_SYNC] = 40
		self.statistics_2['abc'][N_M_LOG] = 50
		self.statistics_2['abc'][N_M_MODEL] = 60

	def test_write_successful_single_1(self):
		ID = list(self.statistics_1.keys())[0]
		path = self.data_interface._handler.PATH
		self.data_interface.write_statistic_data(self.statistics_1)
		file = os.path.join(path, ID + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file))

	def test_write_successful_single_2(self):
		ID = list(self.statistics_2.keys())[0]
		path = self.data_interface._handler.PATH
		self.data_interface.write_statistic_data(self.statistics_2)
		file = os.path.join(path, ID + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file))

	def test_write_throws_exception(self):
		with self.assertRaises(TypeError):
			self.data_interface.write_statistic_data(Exception)
		with self.assertRaises(TypeError):
			self.data_interface.write_statistic_data('string')
		with self.assertRaises(TypeError):
			self.data_interface.write_statistic_data(123456789)

	def test_merge_dicts_1(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		comb_stats = self.data_interface._merge_dicts(self.statistics_1, self.statistics_2)
		self.assertTrue(ID_1 in comb_stats.keys())
		self.assertTrue(ID_2 in comb_stats.keys())

	def test_merge_dicts_2(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		comb_stats = self.data_interface._merge_dicts(self.statistics_1, self.statistics_1)
		self.assertTrue(ID_1 in comb_stats.keys())
		self.assertTrue(ID_2 not in comb_stats.keys())

	def test_merge_dicts_3(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		comb_stats = self.data_interface._merge_dicts(self.statistics_2, self.statistics_2)
		self.assertTrue(ID_1 not in comb_stats.keys())
		self.assertTrue(ID_2 in comb_stats.keys())

	def test_merge_dicts_4(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		comb_stats = dict()
		comb_stats = self.data_interface._merge_dicts(self.statistics_1, comb_stats)
		self.assertTrue(ID_1 in comb_stats.keys())
		self.assertTrue(ID_2 not in comb_stats.keys())
		comb_stats = self.data_interface._merge_dicts(self.statistics_2, comb_stats)
		self.assertTrue(ID_1 in comb_stats.keys())
		self.assertTrue(ID_2 in comb_stats.keys())

	def test_write_separately(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		path = self.data_interface._handler.PATH
		self.data_interface.write_statistic_data(self.statistics_1)
		self.data_interface.write_statistic_data(self.statistics_2)
		file_1 = os.path.join(path, ID_1 + self.FILE_EXT)
		file_2 = os.path.join(path, ID_2 + self.FILE_EXT)
		print(file_1)
		self.assertTrue(os.path.isfile(file_1))
		self.assertTrue(os.path.isfile(file_2))

	def test_write_together(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		path = self.data_interface._handler.PATH
		comb_stats = self.data_interface._merge_dicts(self.statistics_1, self.statistics_2)
		self.data_interface.write_statistic_data(comb_stats)
		file_1 = os.path.join(path, ID_1 + self.FILE_EXT)
		file_2 = os.path.join(path, ID_2 + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file_1))
		self.assertTrue(os.path.isfile(file_2))

	def test_write_read_successful_single(self):
		ID = list(self.statistics_1.keys())[0]
		self.data_interface.write_statistic_data(self.statistics_1)
		status, data = self.data_interface.read_statistic_data(ID)
		self.assertTrue(status)
		self.assertTrue(isinstance(data, dict))
		self.assertEqual(ID, list(data.keys())[0])

	def test_write_twice_successful_update(self):
		ID = list(self.statistics_1.keys())[0]
		path = self.data_interface._handler.PATH
		self.data_interface.write_statistic_data(self.statistics_1)

		status, data = self.data_interface.read_statistic_data(ID)
		self.assertTrue(status)
		self.assertTrue(isinstance(data, dict))
		self.assertEqual(ID, list(data.keys())[0])
		file = os.path.join(path, ID + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file))
		self.assertEqual(self.statistics_1[ID][N_M_SYNC], data[ID][N_M_SYNC])
		self.assertEqual(self.statistics_1[ID][N_M_LOG], data[ID][N_M_LOG])
		self.assertEqual(self.statistics_1[ID][N_M_MODEL], data[ID][N_M_MODEL])

		self.data_interface.write_statistic_data(self.statistics_1)

		status, data = self.data_interface.read_statistic_data(ID)
		self.assertTrue(status)
		self.assertTrue(isinstance(data, dict))
		self.assertEqual(ID, list(data.keys())[0])
		file = os.path.join(path, ID + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file))
		self.assertEqual(2 * self.statistics_1[ID][N_M_SYNC], data[ID][N_M_SYNC])
		self.assertEqual(2 * self.statistics_1[ID][N_M_LOG], data[ID][N_M_LOG])
		self.assertEqual(2 * self.statistics_1[ID][N_M_MODEL], data[ID][N_M_MODEL])

	def test_read_None(self):
		ID = list(self.statistics_1.keys())[0]
		status, data = self.data_interface.read_statistic_data(ID)
		self.assertFalse(status)
		self.assertIsNone(data)

	def test_read_all(self):
		ID_1 = list(self.statistics_1.keys())[0]
		ID_2 = list(self.statistics_2.keys())[0]
		path = self.data_interface._handler.PATH
		comb_stats = self.data_interface._merge_dicts(self.statistics_1, self.statistics_2)
		self.data_interface.write_statistic_data(comb_stats)

		file_1 = os.path.join(path, ID_1 + self.FILE_EXT)
		file_2 = os.path.join(path, ID_2 + self.FILE_EXT)
		self.assertTrue(os.path.isfile(file_1))
		self.assertTrue(os.path.isfile(file_2))

		status, data = self.data_interface.read_statistic_data_all()

		self.assertTrue(status)
		self.assertTrue(isinstance(data, dict))
		self.assertTrue(ID_1 in data.keys())
		self.assertTrue(ID_2 in data.keys())

	def tearDown(self):
		self.data_interface.clean_up()


if __name__ == '__main__':
	unittest.main()
