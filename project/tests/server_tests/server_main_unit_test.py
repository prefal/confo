#!/usr/bin/env python3.6

import unittest
import os
import configparser
from server.exceptions import IllegalConfigException

# Import python code under test
from server import main

# Let Test Classes end with 'Test'
class ServerMainTest(unittest.TestCase):

	tmp_config = configparser.ConfigParser()
	config_available = False
	PATH = 'project/server/'
	FILE_NAME = 'server.ini'
	CONFIG_FILE = os.path.join(PATH, FILE_NAME)


	def setUp(self):
		""" **Saves current config, if exists** """

		if os.path.exists(self.CONFIG_FILE):
			self.tmp_config.read(self.CONFIG_FILE)
			self.config_available = True
		else:
			self.config_available = False


	def test_no_config_available(self):
		""" **Tests if start.up is aborted if config file misses**

		Without the --no-config flag the application should start from the
		default configuratin file **server.ini**.
		"""

		if os.path.exists(self.CONFIG_FILE):
			os.remove(self.CONFIG_FILE)
		with self.assertRaises(SystemExit) as cm:
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		self.assertEqual(cm.exception.code, 1)


	def test_load_config_1(self):
		""" **Tests if arbitrary legal values inside a config are accepted** """

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='5', path_to_petri_net='net_petri_test.pnml', revert_window_size='1120', port='1234')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists('project/server/server.ini'):
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, 5)
		self.assertEqual(constants.NET_PATH, 'net_petri_test.pnml')
		self.assertEqual(constants.ALGO_WINDOW_SIZE, 1120)
		self.assertEqual(constants.HTTP_PORT, 1234)


	def test_load_config_2(self):
		""" **Tests if arbitrary legal values inside a config are accepted**

		max_workers is missing.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(path_to_petri_net='net_petri_test.pnml', revert_window_size='1120')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)
		import multiprocessing as mp
		cpus = mp.cpu_count()
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, constants.DEFAULT_MAX_NUMBER_OF_WORKERS)
		self.assertEqual(constants.NET_PATH, 'net_petri_test.pnml')
		self.assertEqual(constants.ALGO_WINDOW_SIZE, 1120)
		self.assertEqual(constants.HTTP_PORT, constants.DEFAULT_HTTP_PORT)


	def test_load_config_3(self):
		""" **Tests if arbitrary legal values inside a config are accepted**

		path_to_petri_net is missing.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='5', revert_window_size='1120')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, 5)
		self.assertEqual(constants.NET_PATH, constants.DEFAULT_NET_PATH)
		self.assertEqual(constants.ALGO_WINDOW_SIZE, 1120)


	def test_load_config_4(self):
		""" **Tests if arbitrary legal values inside a config are accepted**

		revert_window_size is missing.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='5', path_to_petri_net='net_petri_test.pnml')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, 5)
		self.assertEqual(constants.NET_PATH, 'net_petri_test.pnml')
		self.assertEqual(constants.ALGO_WINDOW_SIZE, constants.DEFAULT_ALGO_WINDOW_SIZE)


	def test_load_config_5(self):
		""" **Tests if illegal values inside a config are rejected**

		max_workers is zero and default value should be used.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='0', path_to_petri_net='net_petri_test.pnml', revert_window_size='1120')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			with self.assertRaises(IllegalConfigException):
				main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)


	def test_load_config_6(self):
		""" **Tests if illegal values inside a config are rejected**

		path_to_petri_net is emtpy and default value should be used.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='5', path_to_petri_net='', revert_window_size='1120')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, 5)
		self.assertEqual(constants.NET_PATH, constants.DEFAULT_NET_PATH)
		self.assertEqual(constants.ALGO_WINDOW_SIZE, 1120)


	def test_load_config_7(self):
		""" **Tests if illegal values inside a config are rejected**

		revert_window_size is zero and default value should be used.
		"""

		config = configparser.ConfigParser()
		config['config'] = \
			dict(max_workers='5', path_to_petri_net='net_petri_test.pnml', revert_window_size='0')
		with open(self.CONFIG_FILE, 'w') as configfile:
			config.write(configfile)
		if os.path.exists(self.CONFIG_FILE):
			with self.assertRaises(IllegalConfigException):
				main.load_config_and_define_constants(path=self.PATH, filename=self.FILE_NAME)
		else:
			print('Test failed')
			exit(1)

	def test_parse_debug_flag(self):
		""" **Tests start-up with the --debug flag** """

		main.parse_flags(['--debug', '--no-config'])
		from server import constants
		self.assertTrue(constants.DEBUG_MODE)

	def test_parse_no_config_flag(self):
		""" **Tests start-up with the --no-config flag** """

		if os.path.exists(self.CONFIG_FILE):
			os.remove(self.CONFIG_FILE)
		main.parse_flags(['--no-config'])
		from server import constants
		self.assertEqual(constants.MAX_NUMBER_OF_WORKERS, constants.DEFAULT_MAX_NUMBER_OF_WORKERS)
		self.assertEqual(constants.NET_PATH, constants.DEFAULT_NET_PATH)
		self.assertEqual(constants.ALGO_WINDOW_SIZE, constants.DEFAULT_ALGO_WINDOW_SIZE)


	def test_parse_generate_config_flag(self):
		""" **Tests the --generate-config flag** """

		if os.path.exists(self.CONFIG_FILE):
			os.remove(self.CONFIG_FILE)
		with self.assertRaises(SystemExit) as cm:
			main.parse_flags(['--generate-config', '--path', self.PATH, '--filename', self.FILE_NAME])
		self.assertEqual(cm.exception.code, 0)
		self.assertTrue(os.path.exists(self.CONFIG_FILE))


	def test_parse_arbitrary_input_flag(self):
		""" **Tests arbitrary input parameter**

		Module argparse should throw exception and exit with code 2.
		"""

		with self.assertRaises(SystemExit) as cm:
			main.parse_flags(['test_flag_to_raise_a_system_exit'])
		self.assertEqual(cm.exception.code, 2)


	def test_parse_only_path_file_flags(self):
		""" **Test start-up with path and file flags only** """

		main.parse_flags(['--path', self.PATH, '--filename', self.FILE_NAME])


	def tearDown(self):
		""" **Restores current config file if exists** """

		if self.config_available:
			with open(self.CONFIG_FILE, 'w') as configfile:
				self.tmp_config.write(configfile)


if __name__ == '__main__':
	unittest.main()