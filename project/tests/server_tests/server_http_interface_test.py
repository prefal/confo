#!/usr/bin/env python3

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
import json

from server.interface import ConfoServer
from server import constants


class ServerHTTPInterfaceTest(AioHTTPTestCase):

	async def get_application(self):
		constants.DEBUG_MODE = True
		constants.MAX_NUMBER_OF_WORKERS = 4
		confoserver = ConfoServer()
		return confoserver.app

	@unittest_run_loop
	async def test_homepage_interface(self):
		"""Test if interface responds as expected."""
		response = await self.client.get('/')
		self.assertEqual(response.status, 200)

	@unittest_run_loop
	async def test_post_on_homepage_interface(self):
		"""Test if interface responds as expected."""
		response = await self.client.post('/')
		self.assertEqual(response.status, 405)

	@unittest_run_loop
	async def test_statistics_model_interface(self):
		"""Test if interface responds as expected."""
		response = await self.client.get('/statistics/model')
		self.assertEqual(response.status, 200)

	@unittest_run_loop
	async def test_post_on_statistics_model_interface(self):
		"""Test if interface responds as expected."""
		response = await self.client.post('/statistics/model')
		self.assertEqual(response.status, 405)

	@unittest_run_loop
	async def test_check_event_interface_NORMAL(self):
		"""Test if interface handles parameters as expected."""
		data = {'request': {'event': {'case_id': '12345', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})

		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertTrue(json_response['response']['type'] == 'OK' or json_response['response']['type'] == 'ALERT')

	@unittest_run_loop
	async def test_check_event_interface_INVALID_JSON(self):
		"""Test if interface handles parameters as expected."""
		broken_json = "{'request': {'event': {'case_id': '12345', 'activity_name': 'abcde'"
		response = await self.client.post('/check/event', data=broken_json, headers={'content-type': 'application/json'})
		self.assertEqual(response.status, 200)

		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 1)

	@unittest_run_loop
	async def test_check_event_interface_REQUEST(self):
		"""Test if interface handles parameters as expected."""
		data = {'REQUEST': {'event': {'case_id': '12345', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})
		self.assertEqual(response.status, 200)

		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 2)

	@unittest_run_loop
	async def test_check_event_interface_EVENT(self):
		"""Test if interface handles parameters as expected."""
		data = {'request': {'EVENT': {'case_id': '12345', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})
		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 2)

	@unittest_run_loop
	async def test_check_event_interface_CASE_ID(self):
		"""Test if interface handles parameters as expected."""
		data = {'request': {'event': {'CASE_ID': '12345', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})
		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 2)

	@unittest_run_loop
	async def test_check_event_interface_ACTIVITY_NAME(self):
		"""Test if interface handles parameters as expected."""
		data = {'request': {'event': {'case_id': '12345', 'ACTIVITY_NAME': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})
		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 2)

	@unittest_run_loop
	async def test_check_event_interface_EMPTY_VALUES(self):
		"""Test if interface handles parameters as expected."""
		data = {'request': {'event': {'case_id': '', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data), headers={'content-type': 'application/json'})

		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertEqual(json_response['response']['type'], 'REJECT')
		self.assertEqual(json_response['response']['error_id'], 3)

	@unittest_run_loop
	async def test_get_on_check_event_interface(self):
		"""Test if interface responds as expected."""
		response = await self.client.get('/check/event')
		self.assertEqual(response.status, 405)

	@unittest_run_loop
	async def test_statistics_after_check_event(self):
		"""Test if interface responds as expected."""
		data = {'request': {'event': {'case_id': '12345', 'activity_name': 'abcde'}}}
		response = await self.client.post('/check/event', data=json.dumps(data),
										  headers={'content-type': 'application/json'})

		self.assertEqual(response.status, 200)
		json_response = await response.json()
		self.assertTrue(json_response['response']['type'] == 'OK' or json_response['response']['type'] == 'ALERT')
		response = await self.client.get('/statistics/model')
		self.assertEqual(response.status, 200)

if __name__ == '__main__':
	unittest.main()
