#!/usr/bin/env python3
import unittest
import time
import asyncio

# Import python code under test
from pm4py.objects.log.log import Event
from server.interface import ConfoServer
from server import constants
from server.objects.alignments import AlignmentData
from server.storage.alignments.main import AlignmentDataStorageInterface as ADSI
from server.storage.physical.pickleharddrive import PickleHardDrive as PHD
from server.storage.statistics.main import StatisticDataStorageInterface as SDSI


class ServerQueuingTest(unittest.TestCase):

	def setUp(self):
		constants.ALIGNMENT_STORAGE = ADSI(handler=PHD('alignment_data', '.ad'))
		constants.STATISTIC_STORAGE = SDSI(handler=PHD('statistic_data', '.stat'))
		constants.MAX_NUMBER_OF_WORKERS = 4

		# Schedule can be turned on, too, to have visual results of the tests
		self.confoserver = ConfoServer(plot_schedule_on_shutdown=False)
		self.loop = asyncio.get_event_loop()

	def test_compute_response(self):
		"""Show that events belonging to a single case are processed in (the right) sequence"""
		NO_OF_EVENTS = 8

		async def run_test():
			request_index = 0
			event_stream = [('A', 'foo')] * NO_OF_EVENTS
			for case_id, activity in event_stream:
				request_index += 1
				asyncio.ensure_future(register_event(request_index, case_id, activity))

		async def register_event(request_index, case_id, activity):
			self.confoserver._add_event_to_schedule(request_index, case_id)
			event = Event({'concept:name': activity})
			_, _ = await self.confoserver._compute_response(request_index, case_id, event, dummy_worker)

		future = asyncio.ensure_future(run_test())
		# Dirty hack to have computation finish..
		self.loop.run_until_complete(asyncio.sleep(2))

		for index in range(NO_OF_EVENTS - 1):
			self.assertTrue(self.confoserver.schedule[index+2]['executed'] > self.confoserver.schedule[index+1]['finished'])


def dummy_worker(petri_net, initial_marking, final_marking, alignment_data, event, revert_distance=10):
	start_time = int(round(time.time() * 1000))
	time.sleep(0.002)
	case_id = 'x'
	alignment_data, sync, move_on_model, statistics = AlignmentData(case_id), True, False, dict()
	return alignment_data, sync, move_on_model, statistics, start_time

if __name__ == '__main__':
	unittest.main()
