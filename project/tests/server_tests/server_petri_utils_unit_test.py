#!/usr/bin/env python3.6

import unittest

import server.pm4pyref  # noqa: F401
from pm4py.objects import petri

from server.main import petri_net_has_unique_labels


class ServerPetriUtilTest(unittest.TestCase):

	def create_simple_petri_net(self, transition_names, transition_labels):
		"""Create a simple Petri net that only contains some transitions.."""
		assert(len(transition_names) == len(transition_labels))
		net = petri.petrinet.PetriNet('foo')
		for i in range(len(transition_names)):
			transition = petri.petrinet.PetriNet.Transition(name=transition_names[i], label=transition_labels[i])
			net.transitions.add(transition)
		return net

	def test_unique_labels_1(self):
		"""Check that unique labels are ok"""

		names = labels = ['a', 'b', 'c']
		net = self.create_simple_petri_net(names, labels)
		self.assertTrue(petri_net_has_unique_labels(net))

	def test_unique_labels_2(self):
		"""Check that duplicate labels are detected"""

		names = labels = ['a', 'b', 'b']
		net = self.create_simple_petri_net(names, labels)
		self.assertFalse(petri_net_has_unique_labels(net))

	def test_unique_labels_3(self):
		"""Check that unique labels are ok even when names are duplicated"""

		net = self.create_simple_petri_net(['a', 'a'], ['a', 'b'])
		self.assertTrue(petri_net_has_unique_labels(net))

	def test_unique_labels_4(self):
		"""Check that 'None' labels are not detected as duplicates"""

		net = self.create_simple_petri_net(['t_1', 't_2', 't_3'], ['a', None, None])
		self.assertTrue(petri_net_has_unique_labels(net))


if __name__ == '__main__':
	unittest.main()