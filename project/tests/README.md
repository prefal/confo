# Tests

Holds the test files of the client and server-side tests for this application. The specific testcases are to be found
 in the corresponding subdirectories.

----

To execute all unittests ``run _execute_all_unit_tests.py``.

_Use this for convenient testing in your IDE_

----

If only a specific component _X_ is to be tested run any of ``execute_X_unit_tests.py``.

_This is used by the CI pipeline to perform parallel testing_