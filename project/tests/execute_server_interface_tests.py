#!/usr/bin/env python3.6
import unittest
import sys
import os
import inspect

if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    parent_dir = os.path.join(os.path.dirname(current_dir), 'server')
    sys.path.insert(0, parent_dir)
    parent_dir = os.path.dirname(current_dir)
    sys.path.insert(0, parent_dir)

    from tests.server_tests.server_http_interface_test import ServerHTTPInterfaceTest

    test1_object = ServerHTTPInterfaceTest()

    unittest.main()
