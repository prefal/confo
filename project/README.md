# Project

Holds the source code for the 
- **client/server construct**,
- **tests** for the above,
- **docs** source code for generation docs with sphinx and
- a current copy of the **PM4Py library**


