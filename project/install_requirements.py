#!/usr/bin/env python3.6

""" A script to parse and install requirements from requirements.ini

For pm4py, just the requirements from pm4py/requirements.txt are installed.
"""

import argparse
import configparser
import sys
import os

sections = []
pip_parameters = {'pip3': False, 'user': False}
pm4py_used = False


def get_abs_path(filename):
	return os.path.join(os.path.dirname(os.path.abspath(__file__)), filename)


def main(args):
	if args == []:
		print('No arguments given!')
		sys.exit(0)
	parser = argparse.ArgumentParser(description='Install requirements for this project.')
	for sec in sections:
		parser.add_argument('--' + sec, action='count', help='Install ' + sec + ' requirements from requirements.ini.')
	parser.add_argument('--pip3', action='count', help='Use pip3 instead of pip.')
	parser.add_argument('--user', action='count', help='Passing the --user flag to pip.')
	arguments = vars(parser.parse_args(args))

	for prm in pip_parameters:
		if arguments[prm] is not None and arguments[prm] >= 1:
			pip_parameters[prm] = True

	for sec in sections:
		if arguments[sec] is not None and arguments[sec] >= 1:
			parse(sec, pip_parameters)
	if pm4py_used:
		print()
		print('Installing requirements for pm4py')
		print('--------------------------------------')
		install(['-r', get_abs_path('pm4py/requirements.txt')], pip_parameters)


def parse(section, pip_parameters):
	req = configparser.ConfigParser()
	req.read(get_abs_path('requirements.ini'))
	dep_list = []
	if section not in req:
		print('There is no section ' + section + ' in requirements-ini!')
		sys.exit(0)
	else:
		req_section = req[section]
		for dep in req_section:
			if dep == 'pm4py':
				global pm4py_used
				pm4py_used = True
				break
			version = req_section.get(dep)
			if version == 'latest':
				dep_list.append(dep)
			else:
				dep_list.append(dep + '==' + str(version))
	print()
	print('Installing requirements for ' + section)
	print('--------------------------------------')
	install(dep_list, pip_parameters)


def install(args_list, pip_parameters):
	import subprocess
	if pip_parameters['user']:
		args_list.insert(0, '--user')
	args_list.insert(0, '-U')
	args_list.insert(0, 'install')
	if pip_parameters['pip3']:
		args_list.insert(0, 'pip3')
	else:
		args_list.insert(0, 'pip')
	subprocess.call(args_list)


if __name__ == '__main__':
	print('Installing requirements from ' + get_abs_path('requirements.ini'))
	print()
	req = configparser.ConfigParser()
	req.read(get_abs_path('requirements.ini'))
	for key in req.keys():
		if not key == 'DEFAULT':
			sections.append(key)
	main(sys.argv[1:])
