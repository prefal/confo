#!/usr/bin/env python3.6

import os
import subprocess
import sys


if __name__ == '__main__':
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        'public', 'badges', '')
    flake8_cmd = ['flake8', 'project/']
    if not os.path.isdir(path):
        os.makedirs(path)
        print('Created path: ', path)
    else:
        print('Path: ', path, ' exists already.')
    badge_cmd = ['anybadge', '--label=PEP8-Violations', '--value',
             '--file=' + os.path.join(path, 'flake8.svg'),
             '--overwrite', '10=green', '20=orange', '10000000=red']
    #flake8
    print('Running flake8... (', flake8_cmd, ')')
    p = subprocess.Popen(flake8_cmd, universal_newlines=True, stdout=subprocess.PIPE)
    (output, err) = p.communicate()
    print(output)
    violations = len(output.splitlines())
    print('Violations: ', violations)
    exit_code = p.wait()
    print('flake8: DONE')
    
    #badge
    print('Creating badge... (', badge_cmd, ')')
    badge_cmd.insert(3, str(violations))
    subprocess.call(badge_cmd)
    print('Badges creation: DONE')
    sys.exit(exit_code)
