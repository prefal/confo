# Welcome to ConfO!
 [![pipeline status](https://gitlab.com/prefal/confo/badges/master/pipeline.svg?branch=master)](https://gitlab.com/prefal/confo/commits/master)
 [![python_version](https://img.shields.io/badge/python-3.6-blue.svg)](https://img.shields.io/badge/python-3.6-blue.svg)
 [![pep8](https://prefal.gitlab.io/confo/badges/flake8.svg)](.coveragerc)

----

| Unittesting | Client | Server |
| :---- | :----: | :----: |
| with combined statement <br> and branch coverage | [![coverage report](https://gitlab.com/prefal/confo/badges/master/coverage.svg?job=unit_test_client)](https://gitlab.com/prefal/confo/commits/master) | [![coverage report](https://gitlab.com/prefal/confo/badges/master/coverage.svg?job=unit_test_server)](https://gitlab.com/prefal/confo/commits/master) |

----

**Table of Contents**
- [Introduction](#introduction)
- [Repo Structure](#repo-structure)
- [License](#license)
- [Contributors](#contributors)
- [References](#references)

----

## Introduction

ConfO is web service that provides a special service in the process mining context. At its core it focusses solely on computing alignments of event data, but in an online setting. To compute online conformance checking functionality the product relies on the alignments over prefixes approach, or prefix-alignments<sup>[[1]](#references)</sup> respectively. Due to developing in the process mining domain, this project makes use of the existing [PM4PY library][2]. Ultimately, ConfO is able to compute alignments for prefixes of unfinished process executions and provide feedback on deviations immediately.

### Getting Started and Code Documentation

ConfO consists of a web service application and a sample client application. In brief, the web service receives an event stream over a dedicated HTTP/POST interface (actually: one event per HTTP request). For details on the HTTP interface(s) view the corresponding part in the code documentation.

The code documentation is available at [our gitlab page](https://prefal.gitlab.io/confo).

Please also refer to this documentation for installation and deployment guides.

## Repo Structure

The application resides in the project directory. The structure of this directory is the following:

- [**project/server**](project/server) contains the source code for the server application.
- [**project/client**](project/client) contains the source code of an exemplary client application.
- [**project/docs**](project/docs) contains the source code for generating the docs published at [our gitlab page](https://prefal.gitlab.io/confo).
- [**project/tests**](project/tests) contains all source code for unit and interface tests for both the client and the server application.

Check these folders for more detail!

## License

ConfO is released under the GNU General Public License v3.0. The [used library][2] has its own license file in [**project/pm4py**](project/pm4py).

### Copyright Disclaimer

_'The Chair of Process and Data Science at RWTH Aachen University hereby disclaims all copyright interest in the program “ConfO” written by Benedikt Holmes, Min Li and Valentin Steiner.'_ (see [signed Copyright Disclaimer](copyright_disclaimer.pdf))

## Contributors
This contents of this repository are a release of work previously conducted in a private repository by the authors. These contents are the result of a practical lab at the [*Chair of Process and Data Science*](http://www.pads.rwth-aachen.de/go/id/pnbx/?lidx=1) at the RWTH Aachen University. These results only contain the application- and documentation-relevant artifacts of this practical lab. The corresponding authors are:

- [Benedikt Holmes](https://gitlab.com/benhol)
- [Min Li](https://gitlab.com/min.li.RWTH)
- [Valentin Steiner](https://gitlab.com/valentin.steiner)

## References

[1] van Zelst, Sebastiaan J., "Online conformance checking: relating event streams to process models using prefix-alignments", International Journal of Data Science and Analytics, 2017.

[2]: https://pm4py.github.io/